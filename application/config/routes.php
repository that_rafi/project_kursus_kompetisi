<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/

$adminModule = 'Admin';
$adminServices = array(
	'login' => $adminModule.'/Login_admin_controller',
	'logout' => $adminModule.'/Dashboard_admin_controller/logout',
	'dashboard' => $adminModule.'/Dashboard_admin_controller',
	'pengguna' => $adminModule.'/Pengguna_admin_controller',
	'pengguna_page'=>$adminModule.'/Pengguna_admin_controller/index/$1',
	'pengguna_data'=>$adminModule.'/Pengguna_admin_controller/readData',
	'pengguna_detail'=>$adminModule.'/Pengguna_admin_controller/detail',
	'pengguna_delete' =>$adminModule.'/Pengguna_admin_controller/delete',
	'artikel' => $adminModule.'/Artikel_admin_controller',
	'artikel_page'=>$adminModule.'/Artikel_admin_controller/index/$1',
	'artikel_data'=>$adminModule.'/Artikel_admin_controller/readData',
	'artikel_detail'=>$adminModule.'/Artikel_admin_controller/detail',
	'artikel_upload'=>$adminModule.'/Artikel_admin_controller/upload',
	'artikel_uploadRes'=>$adminModule.'/Artikel_admin_controller/upload_single',
	'artikel_delete'=>$adminModule.'/Artikel_admin_controller/delete/$1',
	'artikel_deleteRes'=>$adminModule.'/Artikel_admin_controller/delete_res',
	'kursus' => $adminModule.'/Kursus_admin_controller',
	'kursus_page'=>$adminModule.'/Kursus_admin_controller/index/$1',
	'kursus_data'=>$adminModule.'/Kursus_admin_controller/readData',
	'kursus_detail'=>$adminModule.'/Kursus_admin_controller/detail',
	'kursus_delete'=>$adminModule.'/Kursus_admin_controller/delete/$1',
	'kelas' => $adminModule.'/Kelas_admin_controller',
	'kelas_page'=>$adminModule.'/Kelas_admin_controller/index/$1',
	'kelas_data'=>$adminModule.'/Kelas_admin_controller/readData',
	'kelas_detail'=>$adminModule.'/Kelas_admin_controller/detail',
	'kelas_delete'=>$adminModule.'/Kelas_admin_controller/delete/$1',
	'tempat' => $adminModule.'/Tempat_admin_controller',
	'tempat_page'=>$adminModule.'/Tempat_admin_controller/index/$1',
	'tempat_data'=>$adminModule.'/Tempat_admin_controller/readData',
	'tempat_detail'=>$adminModule.'/Tempat_admin_controller/detail',
	'tempat_delete'=>$adminModule.'/Tempat_admin_controller/delete/$1',
	'pengajar' => $adminModule.'/Pengajar_admin_controller',
	'pengajar_page'=>$adminModule.'/Pengajar_admin_controller/index/$1',
	'pengajar_data'=>$adminModule.'/Pengajar_admin_controller/readData',
	'pengajar_detail'=>$adminModule.'/Pengajar_admin_controller/detail',
	'pengajar_delete'=>$adminModule.'/Pengajar_admin_controller/delete/$1',
	'setting' => $adminModule.'/Setting_admin_controller',
);


$route['admin/login'] = $adminServices['login'];
$route['admin/logout'] = $adminServices['logout'];
$route['admin'] = $adminServices['dashboard'];
// data pengguna
$route['admin/pengguna'] = $adminServices['pengguna'];
$route['admin/pengguna/(:num)'] = $adminServices['pengguna_page'];
$route['admin/pengguna/table'] = $adminServices['pengguna_data'];
$route['admin/pengguna/detail'] = $adminServices['pengguna_detail'];
$route['admin/pengguna/detele'] = $adminServices['pengguna_delete'];
// artikel
$route['admin/artikel'] = $adminServices['artikel'];
$route['admin/artikel/(:num)'] = $adminServices['artikel_page'];
$route['admin/artikel/table'] = $adminServices['artikel_data'];
$route['admin/artikel/detail/(:any)'] = $adminServices['artikel_detail'];
$route['admin/artikel/upload'] = $adminServices['artikel_upload'];
$route['admin/artikel/uploadRes'] = $adminServices['artikel_uploadRes'];
$route['admin/artikel/delete/(:any)'] = $adminServices['artikel_delete'];
$route['admin/artikel/deleteRes'] = $adminServices['artikel_deleteRes'];
// kursus
$route['admin/kursus'] = $adminServices['kursus'];
$route['admin/kursus/(:num)'] = $adminServices['kursus_page'];
$route['admin/kursus/table'] = $adminServices['kursus_data'];
$route['admin/kursus/detail/(:any)'] = $adminServices['kursus_detail'];
$route['admin/kursus/delete/(:any)'] = $adminServices['kursus_delete'];
// kelas
$route['admin/kelas'] = $adminServices['kelas'];
$route['admin/kelas/(:num)'] = $adminServices['kelas_page'];
$route['admin/kelas/table'] = $adminServices['kelas_data'];
$route['admin/kelas/detail/(:any)'] = $adminServices['kelas_detail'];
$route['admin/kelas/delete/(:any)'] = $adminServices['kelas_delete'];
// tempat
$route['admin/tempat'] = $adminServices['tempat'];
$route['admin/tempat/(:num)'] = $adminServices['tempat_page'];
$route['admin/tempat/table'] = $adminServices['tempat_data'];
$route['admin/tempat/detail/(:any)'] = $adminServices['tempat_detail'];
$route['admin/tempat/delete/(:any)'] = $adminServices['tempat_delete'];
// pengajar
$route['admin/pengajar'] = $adminServices['pengajar'];
$route['admin/pengajar/(:num)'] = $adminServices['pengajar_page'];
$route['admin/pengajar/table'] = $adminServices['pengajar_data'];
$route['admin/pengajar/detail/(:any)'] = $adminServices['pengajar_detail'];
$route['admin/pengajar/delete/(:any)'] = $adminServices['pengajar_delete'];
// setting
$route['admin/setting'] = $adminServices['setting'];

$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;
// customer
$customerServices = array(
	'tempat' => 'Tempat_controller',
	'tempat_page'=>'Tempat_controller/index/$1',
	'tempat_detail'=>'Tempat_controller/detail',
	'tempat_tambah'=>'Tempat_controller/tambah',
	'tempat_tambah_upload' => 'Tempat_controller/add',
	'tempat_update' => 'Tempat_controller/manage/$1',
	'tempat_update_uploadRes' => 'Tempat_controller/upload_single',
	'tempat_update_deleteRes' => 'Tempat_controller/delete_res',
	'tempat_hapus' => 'Tempat_controller/hapus/$1',
	'artikel' => 'Artikel_controller',
	'artikel_page'=>'Artikel_controller/index/$1',
	'artikel_detail'=>'Artikel_controller/detail',
	'kelas' => 'Kelas_controller',
	'kelas_page'=>'Kelas_controller/index/$1',
	'kelas_detail'=>'Kelas_controller/detail',
	'kelas_tambah'=>'Kelas_controller/tambah',
	'kelas_tambah_upload' => 'Kelas_controller/add',
	'kelas_update' => 'Kelas_controller/manage/$1',
	'kelas_update_uploadRes' => 'Kelas_controller/upload_single',
	'kelas_update_deleteRes' => 'Kelas_controller/delete_res',
	'kelas_hapus' => 'Kelas_controller/hapus/$1',
	'kursus_tambah'=>'Kursus_controller',
	'kursus_tambah_upload' => 'Kursus_controller/add',
	'kursus_update' => 'Kursus_controller/detail',
	'kursus_update_uploadRes' => 'Kursus_controller/upload_single',
	'kursus_update_deleteRes' => 'Kursus_controller/delete_res',
	'kursus_hapus' => 'Kursus_controller/hapus',
	'search' => 'Search_controller',
	'search_page' => 'Search_controller/index/$1',
	'about' => 'About_controller',
	'dashboard' => 'Dashboard_controller',
	'profile' => 'Profile_controller',
	'login' => 'Login_controller',
	'logout' => 'Login_controller/logout'
);
$route['tempat'] = $customerServices['tempat'];
$route['tempat/(:num)'] = $customerServices['tempat_page'];
$route['tempat/detail/(:any)'] = $customerServices['tempat_detail'];
// dashboard add tempat
$route['tempat/tambah'] = $customerServices['tempat_tambah'];
$route['tempat/tambah/upload'] = $customerServices['tempat_tambah_upload'];
$route['tempat/manage/(:any)'] = $customerServices['tempat_update'];
$route['tempat/manage/(:any)/uploadRes'] = $customerServices['tempat_update_uploadRes'];
$route['tempat/manage/(:any)/deleteRes'] = $customerServices['tempat_update_deleteRes'];
$route['tempat/hapus/(:any)'] = $customerServices['tempat_hapus'];
$route['artikel'] = $customerServices['artikel'];
$route['artikel/(:num)'] = $customerServices['artikel_page'];
$route['artikel/detail/(:any)'] = $customerServices['artikel_detail'];
$route['kursus/tambah'] = $customerServices['kursus_tambah'];
$route['kursus/tambah/upload'] = $customerServices['kursus_tambah_upload'];
$route['kursus/detail'] = $customerServices['kursus_update'];
$route['kursus/detail/uploadRes'] = $customerServices['kursus_update_uploadRes'];
$route['kursus/detail/deleteRes'] = $customerServices['kursus_update_deleteRes'];
$route['kursus/hapus'] = $customerServices['kursus_hapus'];
$route['kelas'] = $customerServices['kelas'];
$route['kelas/(:num)'] = $customerServices['kelas_page'];
$route['kelas/detail/(:any)'] = $customerServices['kelas_detail'];
// dashboard add kelas
$route['kelas/tambah'] = $customerServices['kelas_tambah'];
$route['kelas/tambah/upload'] = $customerServices['kelas_tambah_upload'];
$route['kelas/manage/(:any)'] = $customerServices['kelas_update'];
$route['kelas/manage/(:any)/uploadRes'] = $customerServices['kelas_update_uploadRes'];
$route['kelas/manage/(:any)/deleteRes'] = $customerServices['kelas_update_deleteRes'];
$route['search'] = $customerServices['search'];
$route['search/(:num)'] = $customerServices['search_page'];
$route['kelas/hapus/(:any)'] = $customerServices['kelas_hapus'];
$route['profile'] = $customerServices['profile'];
$route['dashboard'] = $customerServices['dashboard'];
$route['about'] = $customerServices['about'];
$route['login'] = $customerServices['login'];
$route['logout'] = $customerServices['logout'];
$route['default_controller'] = 'Home';
