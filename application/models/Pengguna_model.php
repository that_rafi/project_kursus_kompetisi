<?php
class pengguna_model extends CI_model
 {
     // CRUD
     // READ
     function getID(){
      $this->db->select('id_pengguna');
      $this->db->from('pengguna');
      $this->db->limit(1);
      $this->db->order_by('id_pengguna', 'DESC');
      $data = $this->db->get()->row_array();
      return $data['id_pengguna'];
    }
   function getAllpengguna(){
     $user = $this->db->get('pengguna');
     return $user;
   }
   function getPenggunaByKeyword($limit,$start,$key){
     $this->db->select('p.*');
      $this->db->from('pengguna p');
      $this->db->like('p.nama', $key);
      $this->db->or_like('p.email', $key);
      $this->db->limit($limit,$start);
      $data = $this->db->get();
      return $data;
    }
   function getpenggunaById($authkey){
    $this->db->select('*');
    $this->db->from('pengguna');
    $this->db->where('id_auth_key',$authkey);
    $data = $this->db->get();
    return $data;
  }

  function getPenggunaByIdPengguna($id){
   $this->db->select('*');
   $this->db->from('pengguna');
   $this->db->where('id_pengguna',$id);
   $data = $this->db->get();
   return $data;
 }

  //insert
  function insertQuery($id_auth,$nama,$email,$tgl_lahir,$alamat,$t_badan,$b_badan,$no,$prestasi,$foto){

    $data = [
        'nama' => $nama,
        'email'  => $email,
        'tgl_lahir'  => $tgl_lahir,
        'alamat'  => $alamat,
        't_badan'  => $t_badan,
        'b_badan'  => $b_badan,
        'no_telp'  => $no,
        'prestasi'  => $prestasi,
        'foto' => $foto,
        'id_auth_key'  => $id_auth,
        'createdOn'  => date('y-m-d'),
        'updatedOn' => date('y-m-d')
    ];
    $builder=$this->db->insert('pengguna',$data);
    if($builder){
        return true;
    }else{
        return false;
    }
}

function updateQuery($id_auth,$nama,$email,$tgl_lahir,$alamat,$t_badan,$b_badan,$no,$prestasi,$foto){
    $data = [
        'nama' => $nama,
        'email'  => $email,
        'tgl_lahir'  => $tgl_lahir,
        'alamat'  => $alamat,
        't_badan'  => $t_badan,
        'b_badan'  => $b_badan,
        'no_telp'  => $no,
        'prestasi'  => $prestasi,
        'foto' => $foto,
        'updatedOn' => date('y-m-d')
    ];

$this->db->where('id_auth_key', $id_auth);

$builder=$this->db->update('pengguna',$data);
if($builder){
    return true;
}else{
    return false;
}
}

function updateQueryById($id,$nama,$email,$tgl_lahir,$alamat,$t_badan,$b_badan,$no,$prestasi,$foto){
    $data = [
        'nama' => $nama,
        'email'  => $email,
        'tgl_lahir'  => $tgl_lahir,
        'alamat'  => $alamat,
        't_badan'  => $t_badan,
        'b_badan'  => $b_badan,
        'no_telp'  => $no,
        'prestasi'  => $prestasi,
        'foto' => $foto,
        'updatedOn' => date('y-m-d')
    ];

$this->db->where('id_pengguna', $id);

$builder=$this->db->update('pengguna',$data);
if($builder){
    return true;
}else{
    return false;
}
}

function deleteQuery($id){
    $this->db->where('id_pengguna', $id);
    $builder=$this->db->delete('pengguna');
    if($builder){
        return true;
    }else{
        return false;
}
}
  // untuk cek pengguna
  function isUserExist($id_auth){
    $user = $this->db->get_where('pengguna',['id_auth_key'=> $id_auth]);
    if($user->num_rows() > 0 ){
        return true;
    }else{
        return false;
    }

  }
  // untuk cocokan email , pass
  function isVerify($uname,$pass){
    $user = $this->db->get_where('pengguna',['username'=> $uname,'password'=>$pass]);
    return $user;
  }
 }

 ?>
