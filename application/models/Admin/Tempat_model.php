<?php
class Tempat_model extends CI_model
 {
     // CRUD
     // READ
    function getID(){
        $this->db->select('id_tempat');
        $this->db->from('tempat_or');
        $this->db->limit(1);
        $this->db->order_by('id_tempat', 'DESC');
        $data = $this->db->get()->row_array();
        return $data['id_tempat'];

    }
    function getIDRes($id){
        $this->db->select('id_resource');
        $this->db->from('resource_tempat_or');
        $this->db->limit(1);
        $this->db->where('id_tempat',$id);
        $this->db->order_by('id_resource', 'DESC');
        $data = $this->db->get()->row_array();
        return $data['id_resource'];
    }
   function getAllTempat(){
     $this->db->select('tp.*,c.*,p.*');
     $this->db->from('tempat_or tp');
     $this->db->join('pengguna p', 'p.id_pengguna = tp.createdBy');
     $this->db->join('cabang_or c', 'c.id_cabang = tp.id_cabang');
     $this->db->order_by('tp.id_tempat', 'DESC');
     $user = $this->db->get();
     return $user;
   }
   function getAllApprovedTempat(){
     $this->db->select('tp.*,c.*,p.*');
     $this->db->from('tempat_or tp');
     $this->db->join('pengguna p', 'p.id_pengguna = tp.createdBy');
     $this->db->join('cabang_or c', 'c.id_cabang = tp.id_cabang');
     $this->db->where('tp.status','disetujui');
     $this->db->order_by('tp.id_tempat', 'DESC');
     $user = $this->db->get();
     return $user;
   }
   function getTempatById($id){
     $this->db->select('tp.*,c.*,p.*');
     $this->db->from('tempat_or tp');
     $this->db->join('pengguna p', 'p.id_pengguna = tp.createdBy');
     $this->db->join('cabang_or c', 'c.id_cabang = tp.id_cabang');
     $this->db->where('tp.id_tempat',$id);
     $this->db->order_by('tp.id_tempat', 'DESC');
     $data = $this->db->get();
     return $data;
  }
  function getTempatByIdPengguna($id){
    $this->db->select('tp.*,c.*,p.*');
    $this->db->from('tempat_or tp');
    $this->db->join('pengguna p', 'p.id_pengguna = tp.createdBy');
    $this->db->join('cabang_or c', 'c.id_cabang = tp.id_cabang');
    $this->db->where('tp.createdBy',$id);
    $this->db->order_by('tp.id_tempat', 'DESC');
    $data = $this->db->get();
    return $data;
 }

 function getTempatByIdAndPengguna($idpengguna,$id){
   $this->db->select('tp.*,c.*,p.*');
   $this->db->from('tempat_or tp');
   $this->db->join('pengguna p', 'p.id_pengguna = tp.createdBy');
   $this->db->join('cabang_or c', 'c.id_cabang = tp.id_cabang');
   $this->db->where('tp.createdBy',$idpengguna);
   $this->db->where('tp.id_tempat',$id);
   $this->db->order_by('tp.id_tempat', 'DESC');
   $data = $this->db->get();
   return $data;
}

  function getTempatByKeyword($limit,$start,$key){
    $this->db->select('tp.*,c.*,p.*');
    $this->db->from('tempat_or tp');
    $this->db->join('pengguna p', 'p.id_pengguna = tp.createdBy');
    $this->db->join('cabang_or c', 'c.id_cabang = tp.id_cabang');
    $this->db->like('tp.nama_tempat', $key);
    $this->db->or_like('tp.lokasi', $key);
    $this->db->or_like('tp.status', $key);
    $this->db->order_by('tp.id_tempat', 'DESC');
    $this->db->limit($limit,$start);
    $data = $this->db->get();
    return $data;
  }
  function getApprovedTempatByKeyword($limit,$start,$key,$tag,$sort){
    $this->db->select('tp.*,c.*,p.*');
    $this->db->from('tempat_or tp');
    $this->db->join('pengguna p', 'p.id_pengguna = tp.createdBy');
    $this->db->join('cabang_or c', 'c.id_cabang = tp.id_cabang');
    $this->db->where('tp.status', 'disetujui');
    if($tag!=""){
    $this->db->where('c.nama_cabang', $tag);
    }
    $this->db->group_start();
    $this->db->like('tp.nama_tempat', $key);
    $this->db->or_like('tp.lokasi', $key);
    $this->db->or_like('tp.status', $key);
    $this->db->group_end();
    if($sort!=""){
      $this->db->order_by('tp.id_tempat', $sort);
    }else {
      $this->db->order_by('tp.id_tempat', 'DESC');
    }
    $this->db->limit($limit,$start);
    $data = $this->db->get();
    return $data;
  }
  function getSearchApprovedTempat($key,$tag,$sort){
    $this->db->select('tp.*,c.*,p.*');
    $this->db->from('tempat_or tp');
    $this->db->join('pengguna p', 'p.id_pengguna = tp.createdBy');
    $this->db->join('cabang_or c', 'c.id_cabang = tp.id_cabang');
    $this->db->where('tp.status', 'disetujui');
    if($tag!=""){
    $this->db->where('c.nama_cabang', $tag);
    }
    $this->db->group_start();
    $this->db->like('tp.nama_tempat', $key);
    $this->db->or_like('tp.lokasi', $key);
    $this->db->or_like('tp.status', $key);
    $this->db->group_end();
    if($sort!=""){
      $this->db->order_by('tp.id_tempat', $sort);
    }else {
      $this->db->order_by('tp.id_tempat', 'DESC');
    }
    $data = $this->db->get();
    return $data;
  }
  function getResourceById($id){
    $this->db->select('*');
    $this->db->from('resource_tempat_or');
    $this->db->where('id_resource',$id);
    $data = $this->db->get();
    return $data;
  }
  function getResourceByIdTempat($id){
    $this->db->select('*');
    $this->db->from('resource_tempat_or');
    $this->db->where('id_tempat',$id);
    $data = $this->db->get();
    return $data;
  }
  function getAllResource(){
    $this->db->select('*');
    $this->db->from('resource_tempat_or');
    $data = $this->db->get();
    return $data;
  }

  function getAllTempatWithResource(){
    $this->db->select('tp.*,c.*');
    $this->db->from('tempat_or tp');
    $this->db->join('cabang_or c', 'c.id_cabang = tp.id_cabang');
    $this->db->group_by('tp.id_tempat');
    $this->db->order_by('tp.createdOn','DESC');
    $data = $this->db->get();
    return $data;
  }

  function getPreviewTempatWithResource(){
    $this->db->select('tp.*,c.*');
    $this->db->from('tempat_or tp');
    $this->db->join('cabang_or c', 'c.id_cabang = tp.id_cabang');
    $this->db->group_by('tp.id_tempat');
    $this->db->order_by('tp.createdOn','DESC');
    $this->db->limit(3,0);
    $data = $this->db->get();
    return $data;
  }

  function insertImg($id,$nama,$keterangan){
    $data = [
        'id_tempat' => $id,
        'filename'  => $nama,
        'keterangan' => $keterangan,
        'uploadedOn'  => date('Y-m-d')
    ];
    $builder=$this->db->insert('resource_tempat_or',$data);
    if($builder){
        return true;
    }else{
        return false;
    }
  }
  function deleteIMGQuery($id){
    $this->db->where('id_resource', $id);
    $builder=$this->db->delete('resource_tempat_or');
    if($builder){
        return true;
    }else{
        return false;
  }
  }

  //insert
  function insertQuery(){
    $data = [
        'createdBy' => $this->input->post('createdBy'),
        'nama_tempat'  => $this->input->post('nama_tempat'),
        'deskripsi'  => $this->input->post('deskripsi'),
        'id_cabang'  => $this->input->post('id_cabang'),
        'biaya_sewa'  => $this->input->post('biaya_sewa'),
        'jam'  => $this->input->post('jam'),
        'lokasi'  => $this->input->post('lokasi'),
        'kontak'  => $this->input->post('kontak'),
        'status'  => 'pending',
        'createdOn'  => date('Y-m-d')
    ];
    $builder=$this->db->insert('tempat_or',$data);
    if($builder){
        return true;
    }else{
        return false;
    }
  }

    function updateQuery($id){
      $data = [
          'createdBy' => $this->input->post('createdBy'),
          'nama_tempat'  => $this->input->post('nama_tempat'),
          'deskripsi'  => $this->input->post('deskripsi'),
          'id_cabang'  => $this->input->post('id_cabang'),
          'biaya_sewa'  => $this->input->post('biaya_sewa'),
          'jam'  => $this->input->post('jam'),
          'lokasi'  => $this->input->post('lokasi'),
          'kontak'  => $this->input->post('kontak'),
          'status'  => $this->input->post('status'),
          'updatedOn'  => date('Y-m-d')
      ];

    $this->db->where('id_tempat', $id);

    $builder=$this->db->update('tempat_or',$data);
    if($builder){
        return true;
    }else{
        return false;
    }
    }

    function deleteQuery($id){
        $this->db->where('id_tempat', $id);
        $builder=$this->db->delete('tempat_or');
        if($builder){
            return true;
        }else{
            return false;
    }
    }
  }

 ?>
