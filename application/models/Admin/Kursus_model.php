<?php
class Kursus_model extends CI_model
 {
     // CRUD
     // READ
    function getID(){
        $this->db->select('id_kursus');
        $this->db->from('kursus_p');
        $this->db->limit(1);
        $this->db->order_by('id_kursus', 'DESC');
        $data = $this->db->get()->row_array();
        return $data['id_kursus'];

    }
    function getIDRes($id){
        $this->db->select('id_resource');
        $this->db->from('resource_kursus');
        $this->db->limit(1);
        $this->db->where('id_kursus_p',$id);
        $this->db->order_by('id_resource', 'DESC');
        $data = $this->db->get()->row_array();
        return $data['id_resource'];
    }
   function getAllKursus(){
     $this->db->select('kp.*,c.*,p.*');
     $this->db->from('kursus_p kp');
     $this->db->join('cabang_or c', 'c.id_cabang = kp.cabang_olahraga');
     $this->db->join('pengguna p', 'p.id_pengguna = kp.createdBy');
     $this->db->order_by('kp.id_kursus', 'DESC');
     $user = $this->db->get();
     return $user;
   }
   function getKursusById($id){
     $this->db->select('kp.*,c.*,p.*');
     $this->db->from('kursus_p kp');
     $this->db->join('cabang_or c', 'c.id_cabang = kp.cabang_olahraga');
     $this->db->join('pengguna p', 'p.id_pengguna = kp.createdBy');
     $this->db->where('kp.id_kursus',$id);
     $data = $this->db->get();
     return $data;
  }

  function getKursusByIdPengguna($id){
    $this->db->select('kp.*,c.*,p.*');
    $this->db->from('kursus_p kp');
    $this->db->join('cabang_or c', 'c.id_cabang = kp.cabang_olahraga');
    $this->db->join('pengguna p', 'p.id_pengguna = kp.createdBy');
    $this->db->where('kp.createdBy',$id);
    $data = $this->db->get();
    return $data;
 }

 function getApprovedKursusByIdPengguna($id){
   $this->db->select('kp.*,c.*,p.*');
   $this->db->from('kursus_p kp');
   $this->db->join('cabang_or c', 'c.id_cabang = kp.cabang_olahraga');
   $this->db->join('pengguna p', 'p.id_pengguna = kp.createdBy');
   $this->db->where('kp.createdBy',$id);
   $this->db->where('kp.status','disetujui');
   $this->db->order_by('kp.id_kursus','DESC');
   $data = $this->db->get();
   return $data;
}

  function getKursusByKeyword($limit,$start,$key){
    $this->db->select('kp.*,c.*,p.*');
    $this->db->from('kursus_p kp');
    $this->db->join('cabang_or c', 'c.id_cabang = kp.cabang_olahraga');
    $this->db->join('pengguna p', 'p.id_pengguna = kp.createdBy');
    $this->db->like('kp.nama_kursus', $key);
    $this->db->or_like('kp.lokasi', $key);
    $this->db->or_like('kp.status', $key);
    $this->db->limit($limit,$start);
    $this->db->order_by('kp.id_kursus', 'DESC');
    $data = $this->db->get();
    return $data;
  }
  function getResourceByIdRes($id){
    $this->db->select('*');
    $this->db->from('resource_kursus');
    $this->db->where('id_resource',$id);
    $data = $this->db->get();
    return $data;
  }
  function getResourceByIdKursus($id){
    $this->db->select('*');
    $this->db->from('resource_kursus');
    $this->db->where('id_kursus_p',$id);
    $data = $this->db->get();
    return $data;
  }
  function getResourceBatchById($id){
    $this->db->select('*');
    $this->db->from('resource_kursus');
    $this->db->where_in('id_kursus',$id);
    $data = $this->db->get();
    return $data;
  }
  function getAllResource(){
    $this->db->select('*');
    $this->db->from('resource_kursus');
    $data = $this->db->get();
    return $data;
  }

  function getAllKursusWithResource(){
    $this->db->select('kp.*,c.*');
    $this->db->from('kursus_p kp');
    $this->db->join('cabang_or c', 'c.id_cabang = kp.cabang_olahraga');
    $this->db->group_by('kp.id_kursus');
    $this->db->order_by('kp.createdOn','DESC');
    $data = $this->db->get();
    return $data;
  }

  function getPreviewKursusWithResource(){
    $this->db->select('kp.*,c.*');
    $this->db->from('kursus_p kp');
    $this->db->join('cabang_or c', 'c.id_cabang = kp.cabang_olahraga');
    $this->db->group_by('kp.id_kursus');
    $this->db->order_by('kp.createdOn','DESC');
    $this->db->limit(3,0);
    $data = $this->db->get();
    return $data;
  }

  function insertImg($id,$nama,$keterangan){
    $data = [
        'id_kursus_p' => $id,
        'filename'  => $nama,
        'keterangan' => $keterangan,
        'uploadedOn'  => date('Y-m-d')
    ];
    $builder=$this->db->insert('resource_kursus',$data);
    if($builder){
        return true;
    }else{
        return false;
    }
  }
  function deleteIMGQuery($id){
    $this->db->where('id_resource', $id);
    $builder=$this->db->delete('resource_kursus');
    if($builder){
        return true;
    }else{
        return false;
  }
  }

  //insert
  function insertQuery($logoName){
    $data = [
        'createdBy' => $this->input->post('createdBy'),
        'logo'  => $logoName,
        'nama_kursus'  => $this->input->post('nama_kursus'),
        'cabang_olahraga'  => $this->input->post('cabang_olahraga'),
        'lokasi'  => $this->input->post('lokasi'),
        'deskripsi'  => $this->input->post('deskripsi'),
        'kontak_kursus' => $this->input->post('kontak_kursus'),
        'status'  => 'pending',
        'createdOn'  => date('Y-m-d')
    ];
    $builder=$this->db->insert('kursus_p',$data);
    if($builder){
        return true;
    }else{
        return false;
    }
  }

    function updateQuery($logo,$status){
      $data = [
          'logo'  => $logo,
          'nama_kursus'  => $this->input->post('nama_kursus'),
          'cabang_olahraga'  => $this->input->post('cabang_olahraga'),
          'lokasi'  => $this->input->post('lokasi'),
          'deskripsi'  => $this->input->post('deskripsi'),
          'kontak_kursus' => $this->input->post('kontak_kursus'),
          'status'  => $status,
          'createdOn'  => date('Y-m-d')
      ];

    $this->db->where('id_kursus', $this->input->post('id_kursus'));

    $builder=$this->db->update('kursus_p',$data);
    if($builder){
        return true;
    }else{
        return false;
    }
    }

    function deleteQuery($id){
        $this->db->where('id_kursus', $id);
        $builder=$this->db->delete('kursus_p');
        if($builder){
            return true;
        }else{
            return false;
    }
    }
  }

 ?>
