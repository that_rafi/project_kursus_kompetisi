<?php
class Cabang_model extends CI_model
 {
     // CRUD
     // READ
    function getID(){
        $this->db->select('id_cabang');
        $this->db->from('cabang_or');
        $this->db->limit(1);
        $this->db->order_by('id_cabang', 'DESC');
        $data = $this->db->get()->row_array();
        return $data['id_cabang'];

    }
    function getAllCabang(){
      $this->db->order_by('id_cabang', 'DESC');
      $user = $this->db->get('cabang_or');
      return $user;
    }

   function getCabangById($id){
    $user = $this->db->get_where('cabang_or',['id_cabang'=> $id]);
    return $user;
  }

  //insert
  function insertQuery($nama){
    $data = [
        'nama_cabang' => $nama,
    ];
    $builder=$this->db->insert('cabang_or',$data);
    if($builder){
        return true;
    }else{
        return false;
    }
}

    function updateQuery($nama){
      $data = [
          'nama_cabang' => $nama,
      ];

    $this->db->where('id_cabang', $id);

    $builder=$this->db->update('cabang_or',$data);
    if($builder){
        return true;
    }else{
        return false;
    }
    }

    function deleteQuery($id){
        $this->db->where('id_cabang', $id);
        $builder=$this->db->delete('cabang_or');
        if($builder){
            return true;
        }else{
            return false;
    }
    }
  }

 ?>
