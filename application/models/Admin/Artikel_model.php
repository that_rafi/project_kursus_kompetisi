<?php
class Artikel_model extends CI_model
 {
     // CRUD
     // READ
    function getID(){
        $this->db->select('id_artikel');
        $this->db->from('artikel');
        $this->db->limit(1);
        $this->db->order_by('id_artikel', 'DESC');
        $data = $this->db->get()->row_array();
        return $data['id_artikel'];

    }
    function getIDRes($id){
        $this->db->select('id_resource');
        $this->db->from('resource_artikel');
        $this->db->limit(1);
        $this->db->where('id_artikel',$id);
        $this->db->order_by('id_resource', 'DESC');
        $data = $this->db->get()->row_array();
        return $data['id_resource'];

    }
   function getAllArtikel(){
     $this->db->order_by('id_artikel', 'DESC');
     $user = $this->db->get('artikel');
     return $user;
   }
   function getArtikelById($id){
    $user = $this->db->get_where('artikel',['id_artikel'=> $id]);
    return $user;
  }
  function getArtikelByKeyword($limit,$start,$key){
    // ? ketika ada artikel yang gak punya data
    $this->db->select('a.*');
    $this->db->from('artikel a');
    $this->db->like('a.judul', $key);
    $this->db->or_like('a.isi', $key);
    $this->db->or_like('a.createdOn', $key);
    $this->db->order_by('a.id_artikel', 'DESC');
    $this->db->limit($limit,$start);
    $data = $this->db->get();
    return $data;
  }
  function getResourceById($id){
    $this->db->select('*');
    $this->db->from('resource_artikel');
    $this->db->where('id_artikel',$id);
    $data = $this->db->get();
    return $data;
  }
  function getResourceByIdRes($id){
    $this->db->select('*');
    $this->db->from('resource_artikel');
    $this->db->where('id_resource',$id);
    $data = $this->db->get();
    return $data;
  }
  function getAllResource(){
    $this->db->select('*');
    $this->db->from('resource_artikel');
    $data = $this->db->get();
    return $data;
  }

  function getAllArtikelWithResource(){
    $this->db->select('a.*,ra.*');
    $this->db->from('artikel a');
    $this->db->join('resource_artikel ra', 'ra.id_artikel = a.id_artikel');
    $this->db->group_by('a.id_artikel');
    $this->db->order_by('a.date','DESC');
    $this->db->order_by('a.id_artikel', 'DESC');
    $data = $this->db->get();
    return $data;
  }

  function getPreviewArtikelWithResource(){
    $this->db->select('a.*,ra.*');
    $this->db->from('artikel a');
    $this->db->join('resource_artikel ra', 'ra.id_resource = a.id_artikel');
    $this->db->group_by('a.id_artikel');
    $this->db->order_by('a.createdOn','DESC');
    $this->db->limit(3,0);
    $data = $this->db->get();
    return $data;
  }

  function insertImg($id,$nama,$ket){
    $data = [
        'id_artikel' => $id,
        'filename'  => $nama,
        'keterangan' => $ket,
        'uploadedOn'  => date('Y-m-d')
    ];
    $builder=$this->db->insert('resource_artikel',$data);
    if($builder){
        return true;
    }else{
        return false;
    }
}
function deleteIMGQuery($id){
    $this->db->where('id_resource', $id);
    $builder=$this->db->delete('resource_artikel');
    if($builder){
        return true;
    }else{
        return false;
}
}

  //insert
  function insertQuery(){
    $data = [
        'judul' => $this->input->post('judul'),
        'isi'  => $this->input->post('isi'),
        'createdOn'  => $this->input->post('createdOn')
    ];
    $builder=$this->db->insert('artikel',$data);
    if($builder){
        return true;
    }else{
        return false;
    }
}

    function updateQuery($id){
      $data = [
          'judul' => $this->input->post('judul'),
          'isi'  => $this->input->post('isi'),
          'createdOn'  => $this->input->post('createdOn')
      ];

    $this->db->where('id_artikel', $id);

    $builder=$this->db->update('artikel',$data);
    if($builder){
        return true;
    }else{
        return false;
    }
    }

    function deleteQuery($id){
        $this->db->where('id_artikel', $id);
        $builder=$this->db->delete('artikel');
        if($builder){
            return true;
        }else{
            return false;
    }
    }
  }

 ?>
