<?php
class Kelas_model extends CI_model
 {
     // CRUD
     // READ
    function getID(){
        $this->db->select('id_kelas');
        $this->db->from('daftar_kelas');
        $this->db->limit(1);
        $this->db->order_by('id_kelas', 'DESC');
        $data = $this->db->get()->row_array();
        return $data['id_kelas'];

    }
    function getIDRes($id){
        $this->db->select('id_resource');
        $this->db->from('resource_kelas');
        $this->db->limit(1);
        $this->db->where('id_kelas',$id);
        $this->db->order_by('id_resource', 'DESC');
        $data = $this->db->get()->row_array();
        return $data['id_resource'];
    }
   function getAllKelas(){
     $this->db->select('dk.*,kp.*,p.*,pg.*,co.*');
     $this->db->from('daftar_kelas dk');
     $this->db->join('kursus_p kp', 'kp.id_kursus = dk.id_kursus_p');
     $this->db->join('cabang_or co', 'co.id_cabang = kp.cabang_olahraga');
     $this->db->join('pengajar p', 'p.id_pengajar = dk.id_pengajar');
     $this->db->join('pengguna pg', 'pg.id_pengguna = kp.createdBy');
     $this->db->order_by('dk.id_kelas', 'DESC');
     $user = $this->db->get();
     return $user;
   }
   function getAllApprovedKelas(){
     $this->db->select('dk.*,kp.*,p.*,pg.*');
     $this->db->from('daftar_kelas dk');
     $this->db->join('kursus_p kp', 'kp.id_kursus = dk.id_kursus_p');
     $this->db->join('pengajar p', 'p.id_pengajar = dk.id_pengajar');
     $this->db->join('pengguna pg', 'pg.id_pengguna = kp.createdBy');
     $this->db->order_by('dk.id_kelas', 'DESC');
     $this->db->where('dk.status_kelas','disetujui');
     $user = $this->db->get();
     return $user;
   }

   function getKelasById($id){
     $this->db->select('dk.*,kp.*,p.*,c.*,pg.*');
     $this->db->from('daftar_kelas dk');
     $this->db->join('kursus_p kp', 'kp.id_kursus = dk.id_kursus_p');
     $this->db->join('cabang_or c', 'c.id_cabang = kp.cabang_olahraga');
     $this->db->join('pengajar p', 'p.id_pengajar = dk.id_pengajar');
     $this->db->join('pengguna pg', 'pg.id_pengguna = kp.createdBy');
     $this->db->order_by('dk.id_kelas', 'DESC');
     $this->db->where('dk.id_kelas',$id);
     $data = $this->db->get();
     return $data;
  }

  function getKelasByIdKursus($id){
    $this->db->select('dk.*,kp.*,p.*,c.*,pg.*');
    $this->db->from('daftar_kelas dk');
    $this->db->join('kursus_p kp', 'kp.id_kursus = dk.id_kursus_p');
    $this->db->join('cabang_or c', 'c.id_cabang = kp.cabang_olahraga');
    $this->db->join('pengajar p', 'p.id_pengajar = dk.id_pengajar');
    $this->db->join('pengguna pg', 'pg.id_pengguna = kp.createdBy');
    $this->db->order_by('dk.id_kelas', 'DESC');
    $this->db->where('dk.id_kursus_p',$id);
    $data = $this->db->get();
    return $data;
 }

  function getKelasByIdPengguna($id){
    $this->db->select('dk.*,kp.*,p.*,c.*,pg.*');
    $this->db->from('daftar_kelas dk');
    $this->db->join('kursus_p kp', 'kp.id_kursus = dk.id_kursus_p');
    $this->db->join('cabang_or c', 'c.id_cabang = kp.cabang_olahraga');
    $this->db->join('pengajar p', 'p.id_pengajar = dk.id_pengajar');
    $this->db->join('pengguna pg', 'pg.id_pengguna = kp.createdBy');
    $this->db->order_by('dk.id_kelas', 'DESC');
    $this->db->where('kp.createdBy',$id);
    $data = $this->db->get();
    return $data;
 }

 function getKelasByIdAndPengguna($idpengguna,$id){
   $this->db->select('dk.*,kp.*,p.*,c.*,pg.*');
   $this->db->from('daftar_kelas dk');
   $this->db->join('kursus_p kp', 'kp.id_kursus = dk.id_kursus_p');
   $this->db->join('cabang_or c', 'c.id_cabang = kp.cabang_olahraga');
   $this->db->join('pengajar p', 'p.id_pengajar = dk.id_pengajar');
   $this->db->join('pengguna pg', 'pg.id_pengguna = kp.createdBy');
   $this->db->order_by('dk.id_kelas', 'DESC');
   $this->db->where('dk.id_kelas',$id);
   $this->db->where('kp.createdBy',$idpengguna);
   $data = $this->db->get();
   return $data;
}
  function getKelasByKeyword($limit,$start,$key){
    $this->db->select('dk.*,kp.*,p.*,c.*,pg.*');
    $this->db->from('daftar_kelas dk');
    $this->db->join('kursus_p kp', 'kp.id_kursus = dk.id_kursus_p');
    $this->db->join('cabang_or c', 'c.id_cabang = kp.cabang_olahraga');
    $this->db->join('pengajar p', 'p.id_pengajar = dk.id_pengajar');
    $this->db->join('pengguna pg', 'pg.id_pengguna = kp.createdBy');
    $this->db->like('dk.nama_kelas', $key);
    $this->db->or_like('kp.nama_kursus', $key);
    $this->db->or_like('dk.id_kursus_p', $key);
    $this->db->order_by('dk.id_kelas', 'DESC');
    $this->db->limit($limit,$start);
    $data = $this->db->get();
    return $data;
  }
  function getApprovedKelasByKeyword($limit,$start,$key,$tag,$sort){
    $this->db->select('dk.*,kp.*,p.*,c.*,pg.*');
    $this->db->from('daftar_kelas dk');
    $this->db->join('kursus_p kp', 'kp.id_kursus = dk.id_kursus_p');
    $this->db->join('cabang_or c', 'c.id_cabang = kp.cabang_olahraga');
    $this->db->join('pengajar p', 'p.id_pengajar = dk.id_pengajar');
    $this->db->join('pengguna pg', 'pg.id_pengguna = kp.createdBy');
    $this->db->where('dk.status_kelas','disetujui');
    if($tag!=""){
    $this->db->where('c.nama_cabang', $tag);
    }
    $this->db->group_start();
    $this->db->like('dk.nama_kelas', $key);
    $this->db->or_like('kp.nama_kursus', $key);
    $this->db->or_like('dk.id_kursus_p', $key);
    $this->db->group_end();
    if($sort!=""){
      $this->db->order_by('dk.id_kelas', $sort);
    }else {
      $this->db->order_by('dk.id_kelas', 'DESC');
    }
    $this->db->limit($limit,$start);
    $data = $this->db->get();
    return $data;
  }
  function getSearchApprovedKelas($key,$tag,$sort){
    $this->db->select('dk.*,kp.*,p.*,c.*,pg.*');
    $this->db->from('daftar_kelas dk');
    $this->db->join('kursus_p kp', 'kp.id_kursus = dk.id_kursus_p');
    $this->db->join('cabang_or c', 'c.id_cabang = kp.cabang_olahraga');
    $this->db->join('pengajar p', 'p.id_pengajar = dk.id_pengajar');
    $this->db->join('pengguna pg', 'pg.id_pengguna = kp.createdBy');
    $this->db->where('dk.status_kelas','disetujui');
    if($tag!=""){
    $this->db->where('c.nama_cabang', $tag);
    }
    $this->db->group_start();
    $this->db->like('dk.nama_kelas', $key);
    $this->db->or_like('kp.nama_kursus', $key);
    $this->db->or_like('dk.id_kursus_p', $key);
    $this->db->group_end();
    if($sort!=""){
      $this->db->order_by('dk.id_kelas', $sort);
    }else {
      $this->db->order_by('dk.id_kelas', 'DESC');
    }
    $data = $this->db->get();
    return $data;
  }
  function getResourceById($id){
    $this->db->select('*');
    $this->db->from('resource_kelas');
    $this->db->where('id_resource',$id);
    $data = $this->db->get();
    return $data;
  }

  function getResourceByIdKelas($id){
    $this->db->select('*');
    $this->db->from('resource_kelas');
    $this->db->where('id_kelas',$id);
    $data = $this->db->get();
    return $data;
  }

  function getResourceBatchById($id){
    $this->db->select('*');
    $this->db->from('resource_kelas');
    $this->db->where_in('id_kelas',$id);
    $data = $this->db->get();
    return $data;
  }
  function getAllResource(){
    $this->db->select('*');
    $this->db->from('resource_kelas');
    $data = $this->db->get();
    return $data;
  }

  function getAllKelasWithResource(){
    $this->db->select('dk.*,rk.*');
    $this->db->from('daftar_kelas dk');
    $this->db->join('resource_kelas rk', 'rk.id_kelas = dk.id_kelas');
    $this->db->group_by('dk.id_kelas');
    $this->db->order_by('dk.createdOn','DESC');
    $data = $this->db->get();
    return $data;
  }

  function getPreviewKelasWithResource(){
    $this->db->select('dk.*,rk.*');
    $this->db->from('daftar_kelas dk');
    $this->db->join('resource_kelas rk', 'rk.id_kelas = dk.id_kelas');
    $this->db->group_by('dk.id_kelas');
    $this->db->order_by('dk.createdOn','DESC');
    $this->db->limit(3,0);
    $data = $this->db->get();
    return $data;
  }

  function insertImg($id,$nama,$keterangan){
    $data = [
        'id_kelas' => $id,
        'filename'  => $nama,
        'keterangan' => $keterangan,
        'uploadedOn'  => date('Y-m-d')
    ];
    $builder=$this->db->insert('resource_kelas',$data);
    if($builder){
        return true;
    }else{
        return false;
    }
  }
  function deleteIMGQuery($id){
    $this->db->where('id_resource', $id);
    $builder=$this->db->delete('resource_kelas');
    if($builder){
        return true;
    }else{
        return false;
  }
  }

  //insert
  function insertQuery($idpengajar){
    $data = [
        'id_kursus_p' => $this->input->post('id_kursus_p'),
        'nama_kelas'  => $this->input->post('nama_kelas'),
        'deskripsi_kelas'  => $this->input->post('deskripsi_kelas'),
        'id_pengajar'  => $idpengajar,
        'biaya'  => $this->input->post('biaya'),
        'jadwal_latihan'  => $this->input->post('jadwal_latihan'),
        'jangka_wkt'  => $this->input->post('jangka_wkt'),
        'link'  => $this->input->post('link'),
        'kontak_kelas'  => $this->input->post('kontak_kelas'),
        'status_kelas'  => 'pending',
        'createdOn'  => date('Y-m-d')
    ];
    $builder=$this->db->insert('daftar_kelas',$data);
    if($builder){
        return true;
    }else{
        return false;
    }
  }

    function updateQuery($id){
      $data = [
          'id_kursus_p' => $this->input->post('id_kursus_p'),
          'nama_kelas'  => $this->input->post('nama_kelas'),
          'deskripsi_kelas'  => $this->input->post('deskripsi_kelas'),
          'id_pengajar'  => $this->input->post('id_pengajar'),
          'biaya'  => $this->input->post('biaya'),
          'jadwal_latihan'  => $this->input->post('jadwal_latihan'),
          'jangka_wkt'  => $this->input->post('jangka_wkt'),
          'link'  => $this->input->post('link'),
          'kontak_kelas'  => $this->input->post('kontak_kelas'),
          'status_kelas'  => $this->input->post('status_kelas'),
          'updatedOn'  => date('Y-m-d')
      ];

    $this->db->where('id_kelas', $id);

    $builder=$this->db->update('daftar_kelas',$data);
    if($builder){
        return true;
    }else{
        return false;
    }
    }

    function deleteQuery($id){
        $this->db->where('id_kelas', $id);
        $builder=$this->db->delete('daftar_kelas');
        if($builder){
            return true;
        }else{
            return false;
    }
    }
  }

 ?>
