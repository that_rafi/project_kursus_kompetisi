<?php 
class Admin_model extends CI_model
 {
     // CRUD
     // READ
     function getID(){
      $this->db->select('id_admin');
      $this->db->from('admin');
      $this->db->limit(1);
      $this->db->order_by('id_admin', 'DESC');
      $data = $this->db->get()->row_array();
      return $data['id_admin'];
    }
   function getAllAdmin($id){
     $user = $this->db->get('admin');
     return $user;
   }
   function getAdminById($id){
    $user = $this->db->get('admin',['id_admin'=> $id])->row_array();
    return $user;
  }
  // untuk cek email
  function isUnameExist($uname){
    $user = $this->db->get_where('admin',['username'=> $uname]);
    return $user; 
  }
  // untuk cocokan email , pass
  function isVerify($uname,$pass){
    $user = $this->db->get_where('admin',['username'=> $uname,'password'=>$pass]);
    return $user;
  }
 }

 ?>