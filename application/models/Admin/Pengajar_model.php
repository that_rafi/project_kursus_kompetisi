<?php
class Pengajar_model extends CI_model
 {
     // CRUD
     // READ
    function getID(){
        $this->db->select('id_pengajar');
        $this->db->from('pengajar');
        $this->db->limit(1);
        $this->db->order_by('id_pengajar', 'DESC');
        $data = $this->db->get()->row_array();
        return $data['id_pengajar'];

    }
   function getAllPengajar(){
     $user = $this->db->get('pengajar');
     return $user;
   }
   function getPengajarById($id){
    $user = $this->db->get_where('pengajar',['id_pengajar'=> $id]);
    return $user;
  }

  function getPengajarByKeyword($limit,$start,$key){
    $this->db->select('p.*');
    $this->db->from('pengajar p');
    $this->db->like('p.nama_pengajar', $key);
    $this->db->or_like('p.id_pengajar', $key);
    $this->db->or_like('p.deskripsi_pengajar', $key);
    $this->db->limit($limit,$start);
    $data = $this->db->get();
    return $data;
  }

  //insert
  function insertQuery(){
    $data = [
        'nama_pengajar' => $this->input->post('nama_pengajar'),
        'deskripsi_pengajar' => $this->input->post('deskripsi_pengajar'),
        'prestasi_pengajar' => $this->input->post('prestasi_pengajar')
    ];
    $builder=$this->db->insert('pengajar',$data);
    if($builder){
        return true;
    }else{
        return false;
    }
}

    function updateQuery($id){
      $data = [
          'nama_pengajar' => $this->input->post('nama_pengajar'),
          'deskripsi_pengajar' => $this->input->post('deskripsi_pengajar'),
          'prestasi_pengajar' => $this->input->post('prestasi_pengajar')
      ];

    $this->db->where('id_pengajar', $id);

    $builder=$this->db->update('pengajar',$data);
    if($builder){
        return true;
    }else{
        return false;
    }
    }

    function deleteQuery($id){
        $this->db->where('id_pengajar', $id);
        $builder=$this->db->delete('pengajar');
        if($builder){
            return true;
        }else{
            return false;
    }
    }
  }

 ?>
