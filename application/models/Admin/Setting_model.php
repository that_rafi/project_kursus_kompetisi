<?php
class Setting_model extends CI_model
 {
     // CRUD
     // READ
   function getAllSetting(){
     $user = $this->db->get('setting');
     return $user;
   }
   function getSettingById($id){
    $user = $this->db->get_where('setting',['id_setting'=> $id]);
    return $user;
  }

    function updateQuery($id){
      $data = [
          'kontak' => $this->input->post('kontak'),
          'web' => $this->input->post('web'),
          'email' => $this->input->post('email'),
          'setting_status' => $this->input->post('setting_status'),
          'max_kelas' => $this->input->post('max_kelas'),
          'max_tempat' => $this->input->post('max_tempat'),
          'max_res_kelas' => $this->input->post('max_res_kelas'),
          'max_res_tempat' => $this->input->post('max_res_tempat'),
          'max_res_kursus' => $this->input->post('max_res_kursus'),
          'max_res_artikel' => $this->input->post('max_res_artikel')
      ];

    $this->db->where('id_setting', $id);

    $builder=$this->db->update('setting',$data);
    if($builder){
        return true;
    }else{
        return false;
    }
    }

    function deleteQuery($id){
        $this->db->where('id_setting', $id);
        $builder=$this->db->delete('setting');
        if($builder){
            return true;
        }else{
            return false;
    }
    }
  }

 ?>
