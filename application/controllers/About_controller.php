<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class About_controller extends CI_Controller {
    private $data=null;
    public function __construct(){
        parent::__construct();
        $this->load->database();
        $this->load->library('form_validation');
        $this->load->library('pagination');
        $this->load->model('Pengguna_model');
        $this->data['pengguna'] = $this->Pengguna_model->getpenggunaById($this->session->userdata('pengguna'))->row_array();
        }


	public function index()
	{
    $this->data['login_button'] = $this->getAccess();
    $this->load->view('template/header',$this->data);
    $this->load->view('about');
    $this->load->view('template/footer');
  }

  public function getAccess(){
    $login_button = '';
    if(!$this->data['pengguna'])
    {
        $path = $_SERVER['DOCUMENT_ROOT']."/vendor/autoload.php";
        require_once $path;
        $google_client = new Google_Client();
        $google_client->setClientId('996506495419-pnlbjbokslr7l3l79v4g4lcvckas3let.apps.googleusercontent.com'); //Define your ClientID
        $google_client->setClientSecret('qNgHf2vOx9_vxAFG9RK5dndO'); //Define your Client Secret Key
        $google_client->setRedirectUri(base_url().'login'); //Define your Redirect Uri
        $google_client->addScope('email');
        $google_client->addScope('profile');
        $login_button = '<a href="'.$google_client->createAuthUrl().'"><font color="black">Login</font></a>';
        return $login_button;
    }
    else
    {
      return $login_button;
    }
  }

}
?>
