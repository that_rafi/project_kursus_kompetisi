<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard_controller extends CI_Controller {
    private $data=null;
    public function __construct(){
        parent::__construct();
        $this->load->database();
        $this->load->library('form_validation');
        $this->load->library('pagination');
        $this->load->model('Pengguna_model');
        $this->load->model('Admin/Kursus_model');
        $this->load->model('Admin/Kelas_model');
        $this->load->model('Admin/Tempat_model');
        $this->load->model('Admin/Setting_model');
        $this->data['pengguna'] = $this->Pengguna_model->getpenggunaById($this->session->userdata('pengguna'))->row_array();
        $this->data['login_button'] = $this->getAccess();
        if($this->data['pengguna']!= null){
          $this->data['status'] = explode(",",$this->Setting_model->getSettingById(1)->row_array()['setting_status']);
          // redirect ke profile
          $this->data['infokursus'] = $this->Kursus_model->getKursusByIdPengguna($this->data['pengguna']['id_pengguna'])->row_array();
          // KELAS
          $this->data['daftar_kelas'] = $this->Kelas_model->getKelasByIdPengguna($this->data['pengguna']['id_pengguna'])->result_array();
          $this->data['num_kelas'] = count($this->data['daftar_kelas']);
          $this->data['max_kelas'] = $this->Setting_model->getSettingById(1)->row_array()['max_kelas'];
          $this->data['thumbnail'] = array();
          foreach($this->data['daftar_kelas'] as $a) :
            $this->data['thumbnail'][] =  $this->Kelas_model->getResourceByIdKelas($a['id_kelas'])->row_array();
          endforeach;
          // Tempat
          $this->data['daftar_tempat'] = $this->Tempat_model->getTempatByIdPengguna($this->data['pengguna']['id_pengguna'])->result_array();
          $this->data['num_tempat'] = count($this->data['daftar_tempat']);
          $this->data['max_tempat'] = $this->Setting_model->getSettingById(1)->row_array()['max_tempat'];
          $this->data['thumbnail_tempat'] = array();
          foreach($this->data['daftar_tempat'] as $t) :
            $this->data['thumbnail_tempat'][] =  $this->Tempat_model->getResourceByIdTempat($t['id_tempat'])->row_array();
          endforeach;
        }else{
          redirect('/');
        }

        }


	public function index()
	{
    $this->load->view('template/header',$this->data);
    $this->load->view('dashboard',$this->data);
    $this->load->view('template/footer');
  }

  public function getAccess(){
    $login_button = '';
    if(!$this->data['pengguna'])
    {
        $path = $_SERVER['DOCUMENT_ROOT']."/vendor/autoload.php";
        require_once $path;
        $google_client = new Google_Client();
        $google_client->setClientId('996506495419-pnlbjbokslr7l3l79v4g4lcvckas3let.apps.googleusercontent.com'); //Define your ClientID
        $google_client->setClientSecret('qNgHf2vOx9_vxAFG9RK5dndO'); //Define your Client Secret Key
        $google_client->setRedirectUri(base_url().'login'); //Define your Redirect Uri
        $google_client->addScope('email');
        $google_client->addScope('profile');
        $login_button = '<a href="'.$google_client->createAuthUrl().'"><font color="black">Login</font></a>';
        return $login_button;
    }
    else
    {
      return $login_button;
    }
  }


}
?>
