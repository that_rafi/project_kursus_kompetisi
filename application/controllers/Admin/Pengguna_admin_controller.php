<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pengguna_admin_controller extends CI_Controller {
  private $data=null;
    public function __construct(){
        parent::__construct();
            $this->load->database();
            $this->load->library('form_validation');
            $this->load->helper(array('form', 'url'));
			      $this->load->library('pagination');
            $this->data['keyword'] = "";
            $this->load->model('Pengguna_model');
            if($this->session->userdata('id_admin_websiteraga')==""){
                redirect('admin/login');
            }
        }


    public function index()
	{
        $id=1;
        $this->setting_pagination();
        $this->load->view('admin/template/header_admin');
        $this->load->view('admin/admin_pengguna_view',$this->data);
        $this->load->view('admin/template/footer_admin');
    }

    // READ AJAX DATA
    public function readData(){
      // hati2 dalam menuliskan var_dump dalam function yang berhub dg ajax
      // pagination`
      $row = 0;
      $config['per_page']=10;
      $start =(int) $this->input->post('startpage');
      if($start != 0){
        $row= ($start-1)*(int)$config['per_page'] ;
      }
      $session_data['keyword'] ="";
      // if there's keyword
      if($this->input->post('keyword')!=''){
        $this->data['keyword'] = $this->input->post('keyword');
        $session_data['keyword'] = $this->data['keyword'];
        // turn off the pagination
        $start =0;
        $config['per_page'] = $this->Pengguna_model->getAllpengguna()->num_rows();
      }
      $this->data['pengguna']=$this->Pengguna_model->getPenggunaByKeyword($config['per_page'],$start,$this->data['keyword']);
      $session_data['numrow'] = $this->data['pengguna']->num_rows();
      $session_data['startrow'] =$row;
      // return data to ajax
      $data['result'] = $this->data['pengguna']->result_array();
      $data['pageno'] = $this->data['pengguna']->num_rows();
      $this->session->set_userdata($session_data);
      echo json_encode($data);

    }

    // PAGINATION
	public function setting_pagination(){
		if($this->session->userdata('numrow')!=""){
			$config['total_rows'] = $this->session->userdata('numrow');
		}
		$config['per_page']=10;
		$start =0;
		$query=$this->Pengguna_model->getAllpengguna()->num_rows();
		$config['total_rows'] = $query;
		$config['base_url'] = base_url()."/admin/pengguna/";
		$config['full_tag_open']    = '<div class="pagging text-center"><nav><ul class="pagination">';
        $config['full_tag_close']   = '</ul></nav></div>';
        $config['num_tag_open']     = '<li class="page-item"><span class="page-link">';
        $config['num_tag_close']    = '</span></li>';
        $config['cur_tag_open']     = '<li class="page-item active"><span class="page-link">';
        $config['cur_tag_close']    = '<span class="sr-only">(current)</span></span></li>';
        $config['next_tag_open']    = '<li class="page-item"><span class="page-link">';
        $config['next_tag_close']  = '<span aria-hidden="true"></span></span></li>';
        $config['prev_tag_open']    = '<li class="page-item"><span class="page-link">';
        $config['prev_tag_close']  = '</span></li>';
        $config['first_tag_open']   = '<li class="page-item"><span class="page-link">';
        $config['first_tag_close'] = '</span></li>';
        $config['last_tag_open']    = '<li class="page-item"><span class="page-link">';
		$config['last_tag_close']  = '</span></li>';
		$this->pagination->initialize($config);
		//$data['pagination'] = $this->pagination->create_links();
	}

  public function detail(){
    if($this->input->post('id_pengguna')!= null){
      if($this->input->post('isFormUnactive')){
        $data['pengguna'] = $this->Pengguna_model->getPenggunaByIdPengguna($this->input->post('id_pengguna'))->row_array();
        $this->load->view('admin/template/header_admin');
        $this->load->view('admin/admin_pengguna_detail_view',$data['pengguna']);
        $this->load->view('admin/template/footer_admin');
      }else{
        $this->form_validation->set_rules('nama','Nama','trim|required');
        $this->form_validation->set_rules('email','Email','trim|required');
        $this->form_validation->set_rules('tgl_lahir','Tanggal Lahir','trim|required');
        $this->form_validation->set_rules('t_badan','Tinggi Badan','trim|required');
        $this->form_validation->set_rules('b_badan','Berat Badan','trim|required');
        $this->form_validation->set_rules('no_tlp','Nomor Telepon','trim|required');
        if($this->form_validation->run() == FALSE){
          $data['pengguna'] = $this->Pengguna_model->getPenggunaByIdPengguna($this->input->post('id_pengguna'))->row_array();
          $this->load->view('admin/template/header_admin');
          $this->load->view('admin/admin_pengguna_detail_view',$data['pengguna']);
          $this->load->view('admin/template/footer_admin');
        }else{
          $id = $this->input->post('id_pengguna');
          $nama = $this->input->post('nama');
          $email = $this->input->post('email');
          $tgl_lahir = $this->input->post('tgl_lahir');
          $t_badan = $this->input->post('t_badan');
          $b_badan = $this->input->post('b_badan');
          $no_tlp = $this->input->post('no_tlp');
          $alamat = $this->input->post('alamat');
          $prestasi = $this->input->post('prestasi');
          $foto = "";
          if($this->Pengguna_model->updateQueryById($id,$nama,$email,$tgl_lahir,$alamat,$t_badan,$b_badan,$no_tlp,$prestasi,$foto)){
            $this->session->set_flashdata('message','<div class="alert alert-success" role="alert">
           Success!</div>');
           redirect('admin/pengguna');
          }else{
            $this->session->set_flashdata('message','<div class="alert alert-danger" role="alert">
           Failed!</div>');
           redirect('admin/pengguna');
          }
      }

      }
    }
  }

  public function delete(){
    if($this->input->post('id_pengguna')!= null){
      if($this->Pengguna_model->deleteQuery($this->input->post('id_pengguna'))){
        $this->session->set_flashdata('message','<div class="alert alert-success" role="alert">
       Success!</div>');
       redirect('admin/pengguna');
      }else{
        $this->session->set_flashdata('message','<div class="alert alert-danger" role="alert">
       Failed!</div>');
       redirect('admin/pengguna');
      }
    }
  }



}
?>
