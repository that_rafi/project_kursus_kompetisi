<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Kursus_admin_controller extends CI_Controller {
  private $data=null;
    public function __construct(){
        parent::__construct();
            $this->load->database();
            $this->load->library('form_validation');
            $this->load->helper(array('form', 'url'));
			      $this->load->library('pagination');
            $this->data['keyword'] = "";
            $this->load->model('Admin/Kursus_model');
            $this->load->model('Admin/Cabang_model');
            $this->load->model('Admin/Setting_model');
            if($this->session->userdata('id_admin_websiteraga')==""){
                redirect('admin/login');
            }
        }


    public function index()
	{
        $id=1;
        $this->setting_pagination();
        $this->load->view('admin/template/header_admin');
        $this->load->view('admin/admin_kursus_view',$this->data);
        $this->load->view('admin/template/footer_admin');
    }

    // READ AJAX DATA
    public function readData(){
      // hati2 dalam menuliskan var_dump dalam function yang berhub dg ajax
      // pagination`
      $row = 0;
      $config['per_page']=10;
      $start =(int) $this->input->post('startpage');
      if($start != 0){
        $row= ($start-1)*(int)$config['per_page'] ;
      }
      $session_data['keyword'] ="";
      // if there's keyword
      if($this->input->post('keyword')!=''){
        $this->data['keyword'] = $this->input->post('keyword');
        $session_data['keyword'] = $this->data['keyword'];
        // turn off the pagination
        $start =0;
        $config['per_page'] = $this->Kursus_model->getAllKursus()->num_rows();
      }
      $this->data['kursus']=$this->Kursus_model->getKursusByKeyword($config['per_page'],$start,$this->data['keyword']);
      $session_data['numrow'] = $this->data['kursus']->num_rows();
      $session_data['startrow'] =$row;
      // return data to ajax
      $data['result'] = $this->data['kursus']->result_array();
      $data['pageno'] = $this->data['kursus']->num_rows();
      $this->session->set_userdata($session_data);
      echo json_encode($data);

    }

    // PAGINATION
	public function setting_pagination(){
		if($this->session->userdata('numrow')!=""){
			$config['total_rows'] = $this->session->userdata('numrow');
		}
		$config['per_page']=10;
		$start =0;
		$query=$this->Kursus_model->getAllKursus()->num_rows();
		$config['total_rows'] = $query;
		$config['base_url'] = base_url()."/admin/kursus";
		$config['full_tag_open']    = '<div class="pagging text-center"><nav><ul class="pagination">';
        $config['full_tag_close']   = '</ul></nav></div>';
        $config['num_tag_open']     = '<li class="page-item"><span class="page-link">';
        $config['num_tag_close']    = '</span></li>';
        $config['cur_tag_open']     = '<li class="page-item active"><span class="page-link">';
        $config['cur_tag_close']    = '<span class="sr-only">(current)</span></span></li>';
        $config['next_tag_open']    = '<li class="page-item"><span class="page-link">';
        $config['next_tag_close']  = '<span aria-hidden="true"></span></span></li>';
        $config['prev_tag_open']    = '<li class="page-item"><span class="page-link">';
        $config['prev_tag_close']  = '</span></li>';
        $config['first_tag_open']   = '<li class="page-item"><span class="page-link">';
        $config['first_tag_close'] = '</span></li>';
        $config['last_tag_open']    = '<li class="page-item"><span class="page-link">';
		$config['last_tag_close']  = '</span></li>';
		$this->pagination->initialize($config);
		//$data['pagination'] = $this->pagination->create_links();
	}


  public function detail(){
    $id = $this->_getId('krs',$this->uri->segment(4));
    $this->form_validation->set_rules('nama_kursus','Nama Kursus','trim|required');
    $this->form_validation->set_rules('deskripsi','Deskripsi','trim|required');
    $this->form_validation->set_rules('kontak_kursus','No.Hp / Kontak','trim|required');
    $this->form_validation->set_rules('lokasi','Link Lokasi','trim|required');
    $this->form_validation->set_rules('createdBy','Pengguna','trim|required');
    if($id!= null){
      $data['kursus'] = $this->Kursus_model->getKursusById($id)->row_array();
      $data['resource'] = $this->Kursus_model->getResourceByIdKursus($id)->result_array();
      $data['cabang'] = $this->Cabang_model->getAllCabang()->result_array();
      $data['countres'] = $this->Kursus_model->getResourceByIdKursus($id)->num_rows();
      $data['status'] = explode(",",$this->Setting_model->getSettingById(1)->row_array()['setting_status']);
      if($this->form_validation->run() == FALSE){
      $this->load->view('admin/template/header_admin');
      $this->load->view('admin/admin_kursus_detail_view',$data);
      $this->load->view('admin/template/footer_admin');
      }else{
        $this->_update($id);
      }
    }
  }

  private function _getId($keyword,$uri){
    if (strpos($uri, $keyword) !== false) {
      if($this->Kursus_model->getKursusById(preg_replace('/\D/', '', $uri))->num_rows()>0){
        return preg_replace('/\D/', '', $uri);
      }else{
        return null;
      }
    }else{
      return null;
    }
  }

  private function _add(){
    if($this->kursus_model->insertQuery()){
      $this->session->set_flashdata('message','<div class="alert alert-success" role="alert">
     Success!</div>');
     redirect('admin/kursus/');
    }else{
      $this->session->set_flashdata('message','<div class="alert alert-danger" role="alert">
     Failed, Please Try Again!</div>');
     redirect('admin/kursus/');
    }
  }


  private function _update($id){
    $status = $this->input->post('status');
    $logo = $this->input->post('logo');
    if($this->Kursus_model->updateQuery($logo,$status)){
      $this->session->set_flashdata('message','<div class="alert alert-success" role="alert">
     Success!</div>');
     redirect('admin/kursus/detail/krs'.$id);
    }else{
      $this->session->set_flashdata('message','<div class="alert alert-danger" role="alert">
     Failed, Please Try Again!</div>');
     redirect('admin/kursus/detail/krs'.$id);
    }
  }

  public function delete(){
    $id = $this->_getId('krs',$this->uri->segment(4));
    if($id!= null){
      // delete resource
      $kursus = $this->Kursus_model->getKursusById($id)->row_array();
      $resource = $this->Kursus_model->getResourceByIdKursus($id)->result_array();
      $rescount = $this->Kursus_model->getResourceByIdKursus($id)->num_rows();
      if($rescount > 0){
        foreach ($resource as $res ) :
          $filePath = './assets/upload/resource_kursus/'.$res['filename'];
          if($res['filename'] != 'default.png'){
            if(file_exists($filePath)){
                unlink($filePath);
            }
          }
        endforeach;
      }
      if($this->Kursus_model->deleteQuery($id)){
        // delete logo
        if($kursus['logo'] != 'default.png'){
          $path = './assets/upload/logo_kursus/'.$kursus['logo'];
          if(file_exists($path)){
            unlink($path);
          }
        }
        $this->session->set_flashdata('message','<div class="alert alert-success" role="alert">Success!</div>');
       redirect('admin/kursus');
      }else{
        $this->session->set_flashdata('message','<div class="alert alert-danger" role="alert">Failed!</div>');
       redirect('admin/kursus');
      }
    }
  }

  public function upload_single(){
    $id = $_POST['id_kursus'];
    $uploaded = array();
    if($id!=null){
      if(!empty($_FILES['file']['name'])){
        // upload files
        $fileName = $this->move_upload($_FILES['file']['tmp_name'],$_FILES['file']['name'],'./assets/upload/training_mandiri/');
        if($fileName!=null){
          if($this->kursus_model->insertImg($id,$fileName,$_POST['keterangan'])){
            $uploaded= array(
              'file' => $fileName,'upload' => true,
              'newid' => $this->kursus_model->getIDRes($id)
            );
          }else{
            $uploaded= array(
              'file' => $fileName,'upload' => false
            );
          }
          $data['result'] = $uploaded;
          echo json_encode($data);
        }
      }
    }
  }

  public function delete_res(){
    $id = $_POST['id_resource'];
    if($id!=null){
        $resource = $this->kursus_model->getResourceByIdRes($id)->row_array();
        $filePath = './assets/upload/training_mandiri/'.$resource['filename'];
        if(file_exists($filePath)){
          if($this->kursus_model->deleteIMGQuery($id)){
            unlink($filePath);
            $data['result'] = true;
            $data['message'] = "berhasil menghapus ".$resource['filename'];
          }
        }else{
            $data['result'] = false;
            $data['message'] = "File ".$resource['filename']." Tidak Ditemukan!";
        }
        echo json_encode($data);
    }

  }

  // Move Upload
  public function move_upload($tmp_name,$name,$path){
    $tempFile = $tmp_name;
    $temp = $name;
    $path_parts = pathinfo($temp);
    $t = preg_replace('/\s+/', '', microtime());
    $fileName = $path_parts['filename']. $t . '.' . $path_parts['extension'];
    $targetPath = $path;
    $targetFile = $targetPath . $fileName ;
    if(move_uploaded_file($tempFile, $targetFile)){
      return $fileName;
    }else{
      return null;
    }
  }

  // FORM VALIDATION
	public function formvalid(){
		$this->form_validation->set_error_delimiters('', '');
		$this->form_validation->set_rules('isi','Isi','trim|required');
		$this->form_validation->set_rules('judul','Judul','trim|required');
		$this->form_validation->set_rules('createdOn','Tanggal','trim|required');
		if($this->form_validation->run()==false){
			return true;
		}else{
			return false;
		}

	}
}

?>
