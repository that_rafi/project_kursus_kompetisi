<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Kelas_admin_controller extends CI_Controller {
  private $data=null;
    public function __construct(){
        parent::__construct();
            $this->load->database();
            $this->load->library('form_validation');
            $this->load->helper(array('form', 'url'));
			      $this->load->library('pagination');
            $this->data['keyword'] = "";
            $this->load->model('Admin/Kelas_model');
            $this->load->model('Admin/Cabang_model');
            $this->load->model('Admin/Setting_model');
            if($this->session->userdata('id_admin_websiteraga')==""){
                redirect('admin/login');
            }
        }


    public function index()
	{
        $id=1;
        $this->setting_pagination();
        $this->load->view('admin/template/header_admin');
        $this->load->view('admin/admin_kelas_view',$this->data);
        $this->load->view('admin/template/footer_admin');
    }

    // READ AJAX DATA
    public function readData(){
      // hati2 dalam menuliskan var_dump dalam function yang berhub dg ajax
      // pagination`
      $row = 0;
      $config['per_page']=10;
      $start =(int) $this->input->post('startpage');
      if($start != 0){
        $row= ($start-1)*(int)$config['per_page'] ;
      }
      $session_data['keyword'] ="";
      // if there's keyword
      if($this->input->post('keyword')!=''){
        $this->data['keyword'] = $this->input->post('keyword');
        $session_data['keyword'] = $this->data['keyword'];
        // turn off the pagination
        $start =0;
        $config['per_page'] = $this->Kelas_model->getAllKelas()->num_rows();
      }
      $this->data['kelas']=$this->Kelas_model->getKelasByKeyword($config['per_page'],$start,$this->data['keyword']);
      $session_data['numrow'] = $this->data['kelas']->num_rows();
      $session_data['startrow'] =$row;
      // return data to ajax
      $data['result'] = $this->data['kelas']->result_array();
      $data['pageno'] = $this->data['kelas']->num_rows();
      $this->session->set_userdata($session_data);
      echo json_encode($data);

    }

    // PAGINATION
	public function setting_pagination(){
		if($this->session->userdata('numrow')!=""){
			$config['total_rows'] = $this->session->userdata('numrow');
		}
		$config['per_page']=10;
		$start =0;
		$query=$this->Kelas_model->getAllKelas()->num_rows();
		$config['total_rows'] = $query;
		$config['base_url'] = base_url()."/admin/kelas";
		$config['full_tag_open']    = '<div class="pagging text-center"><nav><ul class="pagination">';
        $config['full_tag_close']   = '</ul></nav></div>';
        $config['num_tag_open']     = '<li class="page-item"><span class="page-link">';
        $config['num_tag_close']    = '</span></li>';
        $config['cur_tag_open']     = '<li class="page-item active"><span class="page-link">';
        $config['cur_tag_close']    = '<span class="sr-only">(current)</span></span></li>';
        $config['next_tag_open']    = '<li class="page-item"><span class="page-link">';
        $config['next_tag_close']  = '<span aria-hidden="true"></span></span></li>';
        $config['prev_tag_open']    = '<li class="page-item"><span class="page-link">';
        $config['prev_tag_close']  = '</span></li>';
        $config['first_tag_open']   = '<li class="page-item"><span class="page-link">';
        $config['first_tag_close'] = '</span></li>';
        $config['last_tag_open']    = '<li class="page-item"><span class="page-link">';
		$config['last_tag_close']  = '</span></li>';
		$this->pagination->initialize($config);
		//$data['pagination'] = $this->pagination->create_links();
	}


  public function detail(){
    $id = $this->_getId('kls',$this->uri->segment(4));
    $this->form_validation->set_rules('nama_kelas','Nama Kelas','trim|required');
    $this->form_validation->set_rules('deskripsi_kelas','Deskripsi','trim|required');
    $this->form_validation->set_rules('link','Link Gform','trim|required');
    $this->form_validation->set_rules('kontak_kelas','Kontak','trim|required');
    $this->form_validation->set_rules('biaya','Biaya','trim|required');
    $this->form_validation->set_rules('jadwal_latihan','Jadwal Latihan','trim|required');
    $this->form_validation->set_rules('jangka_wkt','Jangka Waktu','trim|required');
    $this->form_validation->set_rules('id_kursus_p','ID Kursus','trim|required');
    $this->form_validation->set_rules('id_pengajar','ID Pengajar','trim|required');
    $this->form_validation->set_rules('status_kelas','Status','trim|required');
    if($id!= null){
      $data['kelas'] = $this->Kelas_model->getKelasById($id)->row_array();
      $data['resource'] = $this->Kelas_model->getResourceByIdKelas($id)->result_array();
      $data['cabang'] = $this->Cabang_model->getAllCabang()->result_array();
      $data['countres'] = $this->Kelas_model->getResourceByIdKelas($id)->num_rows();
      $data['status'] = explode(",",$this->Setting_model->getSettingById(1)->row_array()['setting_status']);
      if($this->form_validation->run() == FALSE){
      $this->load->view('admin/template/header_admin');
      $this->load->view('admin/admin_kelas_detail_view',$data);
      $this->load->view('admin/template/footer_admin');
      }else{
        $this->_update($id);
      }
    }
  }

  private function _getId($keyword,$uri){
    if (strpos($uri, $keyword) !== false) {
      if($this->Kelas_model->getKelasById(preg_replace('/\D/', '', $uri))->num_rows()>0){
        return preg_replace('/\D/', '', $uri);
      }else{
        return null;
      }
    }else{
      return null;
    }
  }

  private function _update($id){
    if($this->Kelas_model->updateQuery($id)){
      $this->session->set_flashdata('message','<div class="alert alert-success" role="alert">
     Success!</div>');
     redirect('admin/kelas/detail/kls'.$id);
    }else{
      $this->session->set_flashdata('message','<div class="alert alert-danger" role="alert">
     Failed, Please Try Again!</div>');
     redirect('admin/kelas/detail/kls'.$id);
    }
  }

  public function delete(){
    $id = $this->_getId('kls',$this->uri->segment(4));
    if($id!= null){
      // delete resource
      $Kelas = $this->Kelas_model->getKelasById($id)->row_array();
      $resource = $this->Kelas_model->getResourceByIdKelas($id)->result_array();
      $rescount = $this->Kelas_model->getResourceByIdKelas($id)->num_rows();
      if($rescount > 0){
        foreach ($resource as $res ) :
          $filePath = './assets/upload/resource_kelas/'.$res['filename'];
          if($res['filename'] != 'default.png'){
            if(file_exists($filePath)){
                unlink($filePath);
            }
          }
        endforeach;
      }
      if($this->Kelas_model->deleteQuery($id)){
        $this->session->set_flashdata('message','<div class="alert alert-success" role="alert">Success!</div>');
       redirect('admin/kelas');
      }else{
        $this->session->set_flashdata('message','<div class="alert alert-danger" role="alert">Failed!</div>');
       redirect('admin/kelas');
      }
    }
  }

}

?>
