<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard_admin_controller extends CI_Controller {
    public function __construct(){
        parent::__construct();
            $this->load->database();
            $this->load->model('Admin/Admin_model');
            if($this->session->userdata('id_admin_websiteraga')==""){
                redirect('admin/login');
            }
        }


    public function index()
	{
        $admin = $this->Admin_model->getAdminById($this->session->userdata('id_admin'));
        $this->load->view('admin/template/header_admin',$admin);
        $this->load->view('admin/admin_dashboard_view',$admin);
        $this->load->view('admin/template/footer_admin',$admin);
    }

    public function logout(){
        $this->session->unset_userdata('id_admin_websiteraga');
        redirect('admin');
    }

}
?>
