<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Tempat_admin_controller extends CI_Controller {
  private $data=null;
    public function __construct(){
        parent::__construct();
            $this->load->database();
            $this->load->library('form_validation');
            $this->load->helper(array('form', 'url'));
			      $this->load->library('pagination');
            $this->data['keyword'] = "";
            $this->load->model('Admin/Tempat_model');
            $this->load->model('Admin/Cabang_model');
            $this->load->model('Admin/Setting_model');
            if($this->session->userdata('id_admin_websiteraga')==""){
                redirect('admin/login');
            }
        }


    public function index()
	 {
        $id=1;
        $this->setting_pagination();
        $this->load->view('admin/template/header_admin');
        $this->load->view('admin/admin_tempat_view',$this->data);
        $this->load->view('admin/template/footer_admin');
    }

    // READ AJAX DATA
    public function readData(){
      // hati2 dalam menuliskan var_dump dalam function yang berhub dg ajax
      // pagination`
      $row = 0;
      $config['per_page']=10;
      $start =(int) $this->input->post('startpage');
      if($start != 0){
        $row= ($start-1)*(int)$config['per_page'] ;
      }
      $session_data['keyword'] ="";
      // if there's keyword
      if($this->input->post('keyword')!=''){
        $this->data['keyword'] = $this->input->post('keyword');
        $session_data['keyword'] = $this->data['keyword'];
        // turn off the pagination
        $start =0;
        $config['per_page'] = $this->Tempat_model->getAllTempat()->num_rows();
      }
      $this->data['tempat'] = $this->Tempat_model->getTempatByKeyword($config['per_page'],$start,$this->data['keyword']);
      $session_data['numrow'] = $this->data['tempat']->num_rows();
      $session_data['startrow'] =$row;
      // return data to ajax
      $data['result'] = $this->data['tempat']->result_array();
      $data['pageno'] = $this->data['tempat']->num_rows();
      $this->session->set_userdata($session_data);
      echo json_encode($data);
    }

    // PAGINATION
	public function setting_pagination(){
		if($this->session->userdata('numrow')!=""){
			$config['total_rows'] = $this->session->userdata('numrow');
		}
		$config['per_page']=10;
		$start =0;
		$query=$this->Tempat_model->getAllTempat()->num_rows();
		$config['total_rows'] = $query;
		$config['base_url'] = base_url()."/admin/tempat";
		$config['full_tag_open']    = '<div class="pagging text-center"><nav><ul class="pagination">';
        $config['full_tag_close']   = '</ul></nav></div>';
        $config['num_tag_open']     = '<li class="page-item"><span class="page-link">';
        $config['num_tag_close']    = '</span></li>';
        $config['cur_tag_open']     = '<li class="page-item active"><span class="page-link">';
        $config['cur_tag_close']    = '<span class="sr-only">(current)</span></span></li>';
        $config['next_tag_open']    = '<li class="page-item"><span class="page-link">';
        $config['next_tag_close']  = '<span aria-hidden="true"></span></span></li>';
        $config['prev_tag_open']    = '<li class="page-item"><span class="page-link">';
        $config['prev_tag_close']  = '</span></li>';
        $config['first_tag_open']   = '<li class="page-item"><span class="page-link">';
        $config['first_tag_close'] = '</span></li>';
        $config['last_tag_open']    = '<li class="page-item"><span class="page-link">';
		$config['last_tag_close']  = '</span></li>';
		$this->pagination->initialize($config);
		//$data['pagination'] = $this->pagination->create_links();
	}


  public function detail(){
    $id = $this->_getId('tpt',$this->uri->segment(4));
    $this->form_validation->set_rules('nama_tempat','Nama tempat','trim|required');
    $this->form_validation->set_rules('deskripsi','Deskripsi','trim|required');
    $this->form_validation->set_rules('lokasi','Link Gmaps','trim|required');
    $this->form_validation->set_rules('biaya_sewa','Biaya','trim|required');
    $this->form_validation->set_rules('jam','Jadwal Operasional','trim|required');
    $this->form_validation->set_rules('kontak','Kontak','trim|required');
    $this->form_validation->set_rules('status','Status','trim|required');
    if($id!= null){
      $data['tempat'] = $this->Tempat_model->getTempatById($id)->row_array();
      $data['resource'] = $this->Tempat_model->getResourceByIdTempat($id)->result_array();
      $data['cabang'] = $this->Cabang_model->getAllCabang()->result_array();
      $data['countres'] = $this->Tempat_model->getResourceByIdTempat($id)->num_rows();
      $data['status'] = explode(",",$this->Setting_model->getSettingById(1)->row_array()['setting_status']);
      if($this->form_validation->run() == FALSE){
      $this->load->view('admin/template/header_admin');
      $this->load->view('admin/admin_tempat_detail_view',$data);
      $this->load->view('admin/template/footer_admin');
      }else{
        $this->_update($id);
      }
    }
  }

  private function _getId($keyword,$uri){
    if (strpos($uri, $keyword) !== false) {
      if($this->Tempat_model->getTempatById(preg_replace('/\D/', '', $uri))->num_rows()>0){
        return preg_replace('/\D/', '', $uri);
      }else{
        return null;
      }
    }else{
      return null;
    }
  }

  private function _add(){
    if($this->Tempat_model->insertQuery()){
      $this->session->set_flashdata('message','<div class="alert alert-success" role="alert">
     Success!</div>');
     redirect('admin/tempat/');
    }else{
      $this->session->set_flashdata('message','<div class="alert alert-danger" role="alert">
     Failed, Please Try Again!</div>');
     redirect('admin/tempat/');
    }
  }


  private function _update($id){
    if($this->Tempat_model->updateQuery($id)){
      $this->session->set_flashdata('message','<div class="alert alert-success" role="alert">
     Success!</div>');
     redirect('admin/tempat/detail/tpt'.$id);
    }else{
      $this->session->set_flashdata('message','<div class="alert alert-danger" role="alert">
     Failed, Please Try Again!</div>');
     redirect('admin/tempat/detail/tpt'.$id);
    }
  }

  public function delete(){
    $id = $this->_getId('tpt',$this->uri->segment(4));
    if($id!= null){
      // delete resource
      $tempat = $this->Tempat_model->getTempatById($id)->row_array();
      $resource = $this->Tempat_model->getResourceByIdTempat($id)->result_array();
      $rescount = $this->Tempat_model->getResourceByIdTempat($id)->num_rows();
      if($rescount > 0){
        foreach ($resource as $res ) :
          $filePath = './assets/upload/resource_tempat/'.$res['filename'];
          if($res['filename'] != 'default.png'){
            if(file_exists($filePath)){
                unlink($filePath);
            }
          }
        endforeach;
      }
      if($this->Tempat_model->deleteQuery($id)){
        $this->session->set_flashdata('message','<div class="alert alert-success" role="alert">Success!</div>');
       redirect('admin/tempat');
      }else{
        $this->session->set_flashdata('message','<div class="alert alert-danger" role="alert">Failed!</div>');
       redirect('admin/tempat');
      }
    }
  }

  public function upload(){
      // Add
      if($this->formvalid()){
			$array = array(
				'error'   => true,
				'msg' => array(form_error('judul'),form_error('isi'),form_error('createdOn')),
			   );
			$data['form_error']= $array;
			echo json_encode($data);
		}else{ // if form valid
			$array = array('error'   => false,'insert' => false );
			if($this->Tempat_model->insertQuery()){ // if insert valid
				$array = array('error'   => false,'insert' => true );
        $id = $this->Tempat_model->getID();
        $uploaded = array();
        if(!empty($_FILES['file']['name'][0])){ // if there is img
          foreach ($_FILES['file']['name'] as $index => $name) {
            $tempFile = $_FILES['file']['tmp_name'][$index];
            $temp = $_FILES["file"]["name"][$index];
            $fileName = $this->move_upload($tempFile,$temp,'./assets/upload/training_mandiri/');
            if($fileName!=null){
              if($this->Tempat_model->insertImg($id,$fileName,$_POST['keterangan'][$index])){
                $uploaded[]= array('file' => $tempFile,'name' => $fileName,);
              }
            }
          }
          $data['uploaded'] = $uploaded;
    			$data ['_id'] = $id;
          // }
        }
			}
			$data['form_error']= $array;
			echo json_encode($data);
		}
  }

  public function upload_single(){
    $id = $_POST['id_tempat'];
    $uploaded = array();
    if($id!=null){
      if(!empty($_FILES['file']['name'])){
        // upload files
        $fileName = $this->move_upload($_FILES['file']['tmp_name'],$_FILES['file']['name'],'./assets/upload/training_mandiri/');
        if($fileName!=null){
          if($this->Tempat_model->insertImg($id,$fileName,$_POST['keterangan'])){
            $uploaded= array(
              'file' => $fileName,'upload' => true,
              'newid' => $this->Tempat_model->getIDRes($id)
            );
          }else{
            $uploaded= array(
              'file' => $fileName,'upload' => false
            );
          }
          $data['result'] = $uploaded;
          echo json_encode($data);
        }
      }
    }
  }

  public function delete_res(){
    $id = $_POST['id_resource'];
    if($id!=null){
        $resource = $this->Tempat_model->getResourceByIdRes($id)->row_array();
        $filePath = './assets/upload/training_mandiri/'.$resource['filename'];
        if(file_exists($filePath)){
          if($this->Tempat_model->deleteIMGQuery($id)){
            unlink($filePath);
            $data['result'] = true;
            $data['message'] = "berhasil menghapus ".$resource['filename'];
          }
        }else{
            $data['result'] = false;
            $data['message'] = "File ".$resource['filename']." Tidak Ditemukan!";
        }
        echo json_encode($data);
    }

  }

  // Move Upload
  public function move_upload($tmp_name,$name,$path){
    $tempFile = $tmp_name;
    $temp = $name;
    $path_parts = pathinfo($temp);
    $t = preg_replace('/\s+/', '', microtime());
    $fileName = $path_parts['filename']. $t . '.' . $path_parts['extension'];
    $targetPath = $path;
    $targetFile = $targetPath . $fileName ;
    if(move_uploaded_file($tempFile, $targetFile)){
      return $fileName;
    }else{
      return null;
    }
  }

  // FORM VALIDATION
	public function formvalid(){
		$this->form_validation->set_error_delimiters('', '');
		$this->form_validation->set_rules('isi','Isi','trim|required');
		$this->form_validation->set_rules('judul','Judul','trim|required');
		$this->form_validation->set_rules('createdOn','Tanggal','trim|required');
		if($this->form_validation->run()==false){
			return true;
		}else{
			return false;
		}

	}
}

?>
