<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Setting_admin_controller extends CI_Controller {
  private $data=null;
    public function __construct(){
        parent::__construct();
            $this->load->database();
            $this->load->library('form_validation');
            $this->load->helper(array('form', 'url'));
			      $this->load->library('pagination');
            $this->data['keyword'] = "";
            $this->load->model('Admin/Setting_model');
            if($this->session->userdata('id_admin_websiteraga')==""){
                redirect('admin/login');
            }
        }


    public function index()
	{

    $this->form_validation->set_rules('kontak','Kontak','trim|required');
    $this->form_validation->set_rules('email','Email','trim|required|valid_email');
    $this->form_validation->set_rules('web','Website','trim|required');
    $this->form_validation->set_rules('setting_status','Setting Status','trim|required');
    $this->form_validation->set_rules('max_kelas','Max jumlah kelas per user','trim|required');
    $this->form_validation->set_rules('max_tempat','Max jumlah tempat per user','trim|required');
    $this->form_validation->set_rules('max_res_kelas','Max jumlah resource kelas per user','trim|required');
    $this->form_validation->set_rules('max_res_tempat','Max jumlah resource tempat per user','trim|required');
    $this->form_validation->set_rules('max_res_kursus','Max jumlah resource kursus per user','trim|required');
    $this->form_validation->set_rules('max_res_artikel','Max jumlah resource artikel','trim|required');
    $this->data['setting'] = $this->Setting_model->getSettingById(1)->row_array();
      if($this->form_validation->run() == FALSE){
        $this->load->view('admin/template/header_admin');
        $this->load->view('admin/admin_setting_view',$this->data);
        $this->load->view('admin/template/footer_admin');
      }else{
        $this->_update(1);
      }
    }


  private function _update($id){
    if($id!= null){
      if($this->Setting_model->updateQuery($id)){
        $this->session->set_flashdata('message','<div class="alert alert-success" role="alert">
       Success!</div>');
       redirect('admin/setting');
      }else{
        $this->session->set_flashdata('message','<div class="alert alert-danger" role="alert">
       Failed!</div>');
       redirect('admin/setting');
      }
    }
  }



}
?>
