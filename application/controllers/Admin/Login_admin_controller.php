<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login_admin_controller extends CI_Controller {
    public function __construct(){
        parent::__construct();
            $this->load->database();
            $this->load->library('form_validation');
            // panggil model
            $this->load->model('Admin/Admin_model');
            if($this->session->userdata('id_admin_websiteraga')!=""){
                redirect('admin', 'refresh');
            }
        }


	public function index()
	{
        $this->form_validation->set_rules('uname','Username is invalid','trim|required');
        $this->form_validation->set_rules('pass','Password','trim|required');
        if($this->form_validation->run()==false){
        $this->load->view('admin/admin_login_view');
        }else{
        $this->_login();
        }
    }

    private function _login(){
        $uname  = $this->input->post('uname');
        $pass = $this->input->post('pass');
        $isUname = $this->Admin_model->isUnameExist($uname)->num_rows();
        $isMatch = $this->Admin_model->isVerify($uname,$pass)->num_rows();
        // email ada di database atau tidak
        // email sama passs cocok atau tidak
        if($isUname>0){
            // tandaya ada email di db
            if($isMatch>0){
                // masuk ke dashboard
                // set session
                $data =['id_admin_websiteraga' => $this->Admin_model->getID()];
                $this->session->set_userdata($data);
                // pesan
                 $this->session->set_flashdata('message','<div class="alert alert-success" role="alert">
                Success!</div>');
                redirect('admin');
            }else{
                // pesan password dan email salah
                $this->session->set_flashdata('message','<div class="alert alert-danger" role="alert">
                Password and Username is not match!</div>');
                redirect('admin/login');
            }
        }else{
            // ngasih pesan kalau admin belum terdaftar
            $this->session->set_flashdata('message','<div class="alert alert-danger" role="alert">
            You are not registered yet!</div>');
            redirect('admin/login');
        }
    }

}
?>
