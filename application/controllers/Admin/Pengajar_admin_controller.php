<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pengajar_admin_controller extends CI_Controller {
  private $data=null;
    public function __construct(){
        parent::__construct();
            $this->load->database();
            $this->load->library('form_validation');
            $this->load->helper(array('form', 'url'));
			      $this->load->library('pagination');
            $this->data['keyword'] = "";
            $this->load->model('Admin/Pengajar_model');
            if($this->session->userdata('id_admin_websiteraga')==""){
                redirect('admin/login');
            }
        }


    public function index()
	{
        $id=1;
        $this->setting_pagination();
        $this->load->view('admin/template/header_admin');
        $this->load->view('admin/admin_pengajar_view',$this->data);
        $this->load->view('admin/template/footer_admin');
    }

    // READ AJAX DATA
    public function readData(){
      // hati2 dalam menuliskan var_dump dalam function yang berhub dg ajax
      // pagination`
      $row = 0;
      $config['per_page']=10;
      $start =(int) $this->input->post('startpage');
      if($start != 0){
        $row= ($start-1)*(int)$config['per_page'] ;
      }
      $session_data['keyword'] ="";
      // if there's keyword
      if($this->input->post('keyword')!=''){
        $this->data['keyword'] = $this->input->post('keyword');
        $session_data['keyword'] = $this->data['keyword'];
        // turn off the pagination
        $start =0;
        $config['per_page'] = $this->Pengajar_model->getAllPengajar()->num_rows();
      }
      $this->data['pengajar']=$this->Pengajar_model->getPengajarByKeyword($config['per_page'],$start,$this->data['keyword']);
      $session_data['numrow'] = $this->data['pengajar']->num_rows();
      $session_data['startrow'] =$row;
      // return data to ajax
      $data['result'] = $this->data['pengajar']->result_array();
      $data['pageno'] = $this->data['pengajar']->num_rows();
      $this->session->set_userdata($session_data);
      echo json_encode($data);

    }

    // PAGINATION
	public function setting_pagination(){
		if($this->session->userdata('numrow')!=""){
			$config['total_rows'] = $this->session->userdata('numrow');
		}
		$config['per_page']=10;
		$start =0;
		$query=$this->Pengajar_model->getAllPengajar()->num_rows();
		$config['total_rows'] = $query;
		$config['base_url'] = base_url()."/admin/pengajar";
		$config['full_tag_open']    = '<div class="pagging text-center"><nav><ul class="pagination">';
        $config['full_tag_close']   = '</ul></nav></div>';
        $config['num_tag_open']     = '<li class="page-item"><span class="page-link">';
        $config['num_tag_close']    = '</span></li>';
        $config['cur_tag_open']     = '<li class="page-item active"><span class="page-link">';
        $config['cur_tag_close']    = '<span class="sr-only">(current)</span></span></li>';
        $config['next_tag_open']    = '<li class="page-item"><span class="page-link">';
        $config['next_tag_close']  = '<span aria-hidden="true"></span></span></li>';
        $config['prev_tag_open']    = '<li class="page-item"><span class="page-link">';
        $config['prev_tag_close']  = '</span></li>';
        $config['first_tag_open']   = '<li class="page-item"><span class="page-link">';
        $config['first_tag_close'] = '</span></li>';
        $config['last_tag_open']    = '<li class="page-item"><span class="page-link">';
		$config['last_tag_close']  = '</span></li>';
		$this->pagination->initialize($config);
		//$data['pagination'] = $this->pagination->create_links();
	}


  public function detail(){
    $id = $this->_getId('pjr',$this->uri->segment(4));
    $this->form_validation->set_rules('nama_pengajar','Nama Pengajar','trim|required');
    $this->form_validation->set_rules('deskripsi_pengajar','Deskripsi','trim|required');
    if($id!= null){
      $data['pengajar'] = $this->Pengajar_model->getPengajarById($id)->row_array();
      if($this->form_validation->run() == FALSE){
      $this->load->view('admin/template/header_admin');
      $this->load->view('admin/admin_pengajar_detail_view',$data);
      $this->load->view('admin/template/footer_admin');
      }else{
        $this->_update($id);
      }
    }
  }

  private function _getId($keyword,$uri){
    if (strpos($uri, $keyword) !== false) {
      if($this->Pengajar_model->getPengajarById(preg_replace('/\D/', '', $uri))->num_rows()>0){
        return preg_replace('/\D/', '', $uri);
      }else{
        return null;
      }
    }else{
      return null;
    }
  }

  private function _add(){
    if($this->Pengajar_model->insertQuery()){
      $this->session->set_flashdata('message','<div class="alert alert-success" role="alert">
     Success!</div>');
     redirect('admin/pengajar/');
    }else{
      $this->session->set_flashdata('message','<div class="alert alert-danger" role="alert">
     Failed, Please Try Again!</div>');
     redirect('admin/pengajar/');
    }
  }


  private function _update($id){
    if($this->Pengajar_model->updateQuery($id)){
      $this->session->set_flashdata('message','<div class="alert alert-success" role="alert">
     Success!</div>');
     redirect('admin/pengajar/detail/pjr'.$id);
    }else{
      $this->session->set_flashdata('message','<div class="alert alert-danger" role="alert">
     Failed, Please Try Again!</div>');
     redirect('admin/pengajar/detail/pjr'.$id);
    }
  }

  public function delete(){
      if($this->Pengajar_model->deleteQuery($id)){
        $this->session->set_flashdata('message','<div class="alert alert-success" role="alert">Success!</div>');
       redirect('admin/pengajar');
      }else{
        $this->session->set_flashdata('message','<div class="alert alert-danger" role="alert">Failed!</div>');
       redirect('admin/pengajar');
      }
  }
}

?>
