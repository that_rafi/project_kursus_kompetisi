<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {
    private $data=null;
    public function __construct(){
        parent::__construct();
        $this->load->database();
        $this->load->library('form_validation');
        $this->load->library('pagination');
        $this->load->model('Pengguna_model');
        $this->load->model('Admin/Kelas_model');
        $this->load->model('Admin/Artikel_model');
        $this->load->model('Admin/Tempat_model');
        $this->load->model('Admin/Cabang_model');
        $this->data['pengguna'] = $this->Pengguna_model->getpenggunaById($this->session->userdata('pengguna'))->row_array();
        }


	public function index()
	{
        $login_button = '';
        if(!$this->data['pengguna'])
        {
            $path = $_SERVER['DOCUMENT_ROOT']."/vendor/autoload.php";
            require_once $path;
            $google_client = new Google_Client();
            $google_client->setClientId('996506495419-pnlbjbokslr7l3l79v4g4lcvckas3let.apps.googleusercontent.com'); //Define your ClientID
            $google_client->setClientSecret('qNgHf2vOx9_vxAFG9RK5dndO'); //Define your Client Secret Key
            $google_client->setRedirectUri(base_url().'login'); //Define your Redirect Uri
            $google_client->addScope('email');
            $google_client->addScope('profile');
            $login_button = '<a href="'.$google_client->createAuthUrl().'"><font color="black">Login</font></a>';
            $this->data['login_button'] = $login_button;
            $this->getData();
            $this->load->view('template/header',$this->data);
            $this->load->view('home',$this->data);
            $this->load->view('template/footer');
        }
        else
        {
            $this->getData();
            $this->load->view('template/header',$this->data);
            $this->load->view('home',$this->data);
            $this->load->view('template/footer');
        }
    }

    public function getData(){
      $this->data['cabang'] = $this->Cabang_model->getAllCabang()->result_array();
      $this->data['kelas'] = $this->Kelas_model->getAllKelas()->result_array();
      $this->data['num_kelas'] = count($this->data['kelas']);
      $this->data['thumbnail'] = array();
      foreach($this->data['kelas'] as $k) :
        $this->data['thumbnail'][] =  $this->Kelas_model->getResourceByIdKelas($k['id_kelas'])->row_array();
      endforeach;
      // data Tempat
      $this->data['tempat'] = $this->Tempat_model->getAllTempat()->result_array();
      $this->data['num_tempat'] = count($this->data['kelas']);
      $this->data['thumbnail_tempat'] = array();
      foreach($this->data['tempat'] as $t) :
        $this->data['thumbnail_tempat'][] =  $this->Tempat_model->getResourceByIdTempat($t['id_tempat'])->row_array();
      endforeach;
      // data Artikel
      $this->data['artikel'] = $this->Artikel_model->getAllArtikel()->result_array();
      $this->data['num_artikel'] = count($this->data['artikel']);
      $this->data['thumbnail_artikel'] = array();
      foreach($this->data['artikel'] as $a) :
        $this->data['thumbnail_artikel'][] =  $this->Artikel_model->getResourceById($a['id_artikel'])->row_array();
      endforeach;
    }

}
?>
