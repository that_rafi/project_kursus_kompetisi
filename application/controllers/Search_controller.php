<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Search_controller extends CI_Controller {
    private $data=null;
    public function __construct(){
        parent::__construct();
        $this->load->database();
        $this->load->library('form_validation');
        $this->load->library('pagination');
        $this->load->model('Pengguna_model');
        $this->load->model('Admin/Kursus_model');
        $this->load->model('Admin/Kelas_model');
        $this->load->model('Admin/Tempat_model');
        $this->load->model('Admin/Setting_model');
        $this->load->model('Admin/Cabang_model');
        $this->data['pengguna'] = $this->Pengguna_model->getpenggunaById($this->session->userdata('pengguna'))->row_array();
        $this->data['login_button'] = $this->getAccess();

        }

	public function index()
	{
    $this->data['keyword'] = "";$this->data['tags'] = "";$this->data['sort']="";
    if($this->input->get('keyword')!= null){ $this->data['keyword'] = $this->input->get('keyword'); }
    if($this->input->get('tags')!= null){ $this->data['tags'] = $this->input->get('tags'); }
    if($this->input->get('sort')!= null){ $this->data['sort'] = $this->input->get('sort'); }
    $this->data['cabang'] = $this->Cabang_model->getAllCabang()->result_array();
    // kelas
    $this->data['kelas'] = $this->Kelas_model->getSearchApprovedKelas($this->data['keyword'],$this->data['tags'],$this->data['sort'])->result_array();
    $this->data['num_kelas'] = count($this->data['kelas']);
    $this->data['thumbnail'] = array();
    foreach($this->data['kelas'] as $k) :
      $this->data['thumbnail'][] =  $this->Kelas_model->getResourceByIdKelas($k['id_kelas'])->row_array();
    endforeach;
    // tempat
    $this->data['tempat'] = $this->Tempat_model->getSearchApprovedTempat($this->data['keyword'],$this->data['tags'],$this->data['sort'])->result_array();
    $this->data['num_tempat'] = count($this->data['tempat']);
    $this->data['thumbnail_tempat'] = array();
    foreach($this->data['tempat'] as $t) :
      $this->data['thumbnail_tempat'][] =  $this->Tempat_model->getResourceByIdTempat($t['id_tempat'])->row_array();
    endforeach;

    $this->load->view('template/header',$this->data);
    $this->load->view('search-page',$this->data);
    $this->load->view('template/footer');
  }

  public function getAccess(){
    $login_button = '';
    if(!$this->data['pengguna'])
    {
        $path = $_SERVER['DOCUMENT_ROOT']."/vendor/autoload.php";
        require_once $path;
        $google_client = new Google_Client();
        $google_client->setClientId('996506495419-pnlbjbokslr7l3l79v4g4lcvckas3let.apps.googleusercontent.com'); //Define your ClientID
        $google_client->setClientSecret('qNgHf2vOx9_vxAFG9RK5dndO'); //Define your Client Secret Key
        $google_client->setRedirectUri(base_url().'login'); //Define your Redirect Uri
        $google_client->addScope('email');
        $google_client->addScope('profile');
        $login_button = '<a href="'.$google_client->createAuthUrl().'"><font color="black">Login</font></a>';
        return $login_button;
    }
    else
    {
      return $login_button;
    }
  }


}
?>
