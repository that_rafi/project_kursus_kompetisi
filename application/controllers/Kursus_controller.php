<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Kursus_controller extends CI_Controller {
    private $data=null;
    public function __construct(){
        parent::__construct();
        $this->load->database();
        $this->load->library('form_validation');
        $this->load->library('pagination');
        $this->load->model('Pengguna_model');
        $this->load->model('Admin/Kursus_model');
        $this->load->model('Admin/Cabang_model');
        $this->load->model('Admin/Setting_model');
        $this->load->model('Admin/Kelas_model');
        $this->data['pengguna'] = $this->Pengguna_model->getpenggunaById($this->session->userdata('pengguna'))->row_array();
        $this->data['login_button'] = $this->getAccess();
        $this->data['status'] = explode(",",$this->Setting_model->getSettingById(1)->row_array()['setting_status']);
        $this->data['max_kursus'] = $this->Setting_model->getSettingById(1)->row_array()['max_res_kursus'];
        }


	public function index()
	{
    if($this->data['pengguna']!= null){
      if($this->data['pengguna']['no_telp']== null || $this->data['pengguna']['tgl_lahir']== null){
        $this->session->set_flashdata('message','<div class="alert alert-warning" style="text-align: center;" role="alert">
          Silahkan lengkapi data diri anda!!!</div>');
          redirect('profile');
      }
      $infokursus = $this->Kursus_model->getKursusByIdPengguna($this->data['pengguna']['id_pengguna'])->row_array();
      if($infokursus == null ){ // ditolak
        $this->data['cabang'] = $this->Cabang_model->getAllCabang()->result_array();
        $this->load->view('template/header',$this->data);
        $this->load->view('tambah-kursus',$this->data);
        $this->load->view('template/footer');
      }else{
        redirect('dashboard');
      }
    }else{
      $this->session->set_flashdata('message','<div class="alert alert-warning" style="text-align: center;" role="alert">
     Silahkan login terlebih dahulu!!!</div>');
     redirect('');
    }

    // }
  }

  public function detail(){
    if($this->data['pengguna']!= null){
      if($this->data['pengguna']['no_telp']== null || $this->data['pengguna']['tgl_lahir']== null){
        $this->session->set_flashdata('message','<div class="alert alert-warning" style="text-align: center;" role="alert">
          Silahkan lengkapi data diri anda!!!</div>');
          redirect('profile');
      }
      $id = $this->data['pengguna']['id_pengguna'];
      $infokursus = $this->Kursus_model->getKursusByIdPengguna($id)->row_array();
      if($infokursus != null ){ // ditolak
        $idkursus = $infokursus['id_kursus'];
        $this->data['kursus'] = $this->Kursus_model->getKursusById($idkursus)->row_array();
        $this->data['resource'] = $this->Kursus_model->getResourceByIdKursus($idkursus)->result_array();
        $this->data['cabang'] = $this->Cabang_model->getAllCabang()->result_array();
        $this->data['countres'] = $this->Kursus_model->getResourceByIdKursus($idkursus)->num_rows();
        $this->data['status'] = explode(",",$this->Setting_model->getSettingById(1)->row_array()['setting_status']);
        // form valid
        $this->form_validation->set_rules('nama_kursus','Nama Kursus','trim|required');
        $this->form_validation->set_rules('deskripsi','Deskripsi','trim|required');
        $this->form_validation->set_rules('kontak_kursus','No. Hp / Kontak','trim|required');
        $this->form_validation->set_rules('lokasi','Link Lokasi','trim|required');
        $this->form_validation->set_rules('createdBy','Pengguna','trim|required');
        if($this->form_validation->run() == FALSE){
          $this->load->view('template/header',$this->data);
          $this->load->view('detail-kursus',$this->data);
          $this->load->view('template/footer');
        }else{
          $this->_update();
        }
      }else{
        redirect('dashboard');
      }
    }else{
      $this->session->set_flashdata('message','<div class="alert alert-warning" style="text-align: center;" role="alert">
     Silahkan login terlebih dahulu!!!</div>');
     redirect('');
    }


  }

  private function _update(){
    // apakah upload logo
    $status = $this->input->post('status');
    $logoName = $this->input->post('old_logo');
    if(!empty($_FILES['file_logo']['name'])){
      if($this->input->post('old_logo')!='default.png'){
        $filePath = './assets/upload/logo_kursus/'.$this->input->post('old_logo');
        if(file_exists($filePath)){
          unlink($filePath);
        }
      }
      $tempFile = $_FILES['file_logo']['tmp_name'];
      $temp = $_FILES['file_logo']['name'];
      $path_parts = pathinfo($temp);
      $t = preg_replace('/\s+/', '', microtime());
      $logoName = 'logo_'.$path_parts['filename'].'_'.$t.'.'.$path_parts['extension'];
      move_uploaded_file($tempFile, './assets/upload/logo_kursus/'.$logoName);
    }

    if( $status != 'disetujui'){
      $status = 'pending';
    }
    if($this->Kursus_model->updateQuery($logoName,$status)){
      $this->session->set_flashdata('message','<div class="alert alert-success" role="alert">
     Success!</div>');
     redirect('dashboard');
    }else{
      $this->session->set_flashdata('message','<div class="alert alert-danger" role="alert">
     Erorr!</div>');
     redirect('dashboard');
    }
  }

  public function add(){
    // Add
      if($this->formvalid()){
      $array = array(
        'error'   => true,
        'msg' => array(form_error('nama_kursus'),form_error('deskripsi'),form_error('kontak_kursus'),form_error('lokasi')),
         );
      $data['form_error']= $array;
      echo json_encode($data);
    }else{ // if form valid
      $array = array('error'   => false,'insert' => false );
      $logoName = "default.png";
      // apakah upload logo
      if(!empty($_FILES['logo']['name'])){
        $tempFile = $_FILES['logo']['tmp_name'];
        $temp = $_FILES['logo']['name'];
        $path_parts = pathinfo($temp);
        $t = preg_replace('/\s+/', '', microtime());
        $logoName = 'logo_'.$path_parts['filename'].'_'.$t.'.'.$path_parts['extension'];
        move_uploaded_file($tempFile, './assets/upload/logo_kursus/'.$logoName);
      }
      if($this->Kursus_model->insertQuery($logoName)){ // if insert valid
        $array = array('error'   => false,'insert' => true );
        $id = $this->Kursus_model->getID();
        $uploaded = array();
        if(!empty($_FILES['file']['name'][0])){ // if there is img
          foreach ($_FILES['file']['name'] as $index => $name) {
            $tempFile = $_FILES['file']['tmp_name'][$index];
            $temp = $_FILES["file"]["name"][$index];
            $fileName = $this->move_upload($tempFile,$temp,'./assets/upload/resource_kursus/');
            if($fileName!=null){
              if($this->Kursus_model->insertImg($id,$fileName,$_POST['keterangan'][$index])){
                $uploaded[]= array('file' => $tempFile,'name' => $fileName,);
              }
            }
          }
          $data['uploaded'] = $uploaded;
          $data ['_id'] = $id;
          // }
        }
      }
      $data['form_error']= $array;
      echo json_encode($data);
    }
  }

  public function hapus(){
    $infokursus = $this->Kursus_model->getKursusByIdPengguna($this->data['pengguna']['id_pengguna'])->row_array();
    if($infokursus != null ){
        // delete Kelas
        $queryKelas = $this->Kelas_model->getKelasByIdKursus($infokursus['id_kursus']);
        $countKelas = $queryKelas->num_rows();
        $infokelas = $queryKelas->result_array();
        if($countKelas > 0){
          // deleteRes Kelass
          foreach ($infokelas as $kls ) :
            $resourceKelas = $this->Kelas_model->getResourceByIdKelas($kls['id_kelas'])->result_array();
            $rescountKelas = $this->Kelas_model->getResourceByIdKelas($kls['id_kelas'])->num_rows();
            if($rescountKelas > 0){
              foreach ($resourceKelas as $reskls ) :
                $filePathKls = './assets/upload/resource_kelas/'.$reskls['filename'];
                if($reskls['filename'] != 'default.png'){
                  if(file_exists($filePathKls)){
                      unlink($filePathKls);
                  }
                }
              endforeach;
            }
          endforeach;
        }
        // delete resource kursus
        $resource = $this->Kursus_model->getResourceByIdKursus($infokursus['id_kursus'])->result_array();
        $rescount = $this->Kursus_model->getResourceByIdKursus($infokursus['id_kursus'])->num_rows();
        if($rescount > 0){
          foreach ($resource as $res ) :
            $filePath = './assets/upload/resource_kursus/'.$res['filename'];
            if($res['filename'] != 'default.png'){
              if(file_exists($filePath)){
                  unlink($filePath);
              }
            }
          endforeach;
        }
        // delete logo
        if($this->Kursus_model->deleteQuery($infokursus['id_kursus'])){
          if($infokursus['logo'] != 'default.png'){
            $path = './assets/upload/logo_kursus/'.$infokursus['logo'];
            if(file_exists($path)){
              unlink($path);
            }
          }
          $this->session->set_flashdata('message','<div class="alert alert-success" role="alert">Success!'.$path.'</div>');
         redirect('dashboard');
        }else{
          $this->session->set_flashdata('message','<div class="alert alert-danger" role="alert">Failed!</div>');
         redirect('dashboard');
        }
    }else{
      redirect('dashboard');
    }
  }



  public function upload_single(){
    $id = $_POST['id_kursus'];
    $uploaded = array();
    if($id!=null){
      if(!empty($_FILES['file']['name'])){
        // upload files
        $fileName = $this->move_upload($_FILES['file']['tmp_name'],$_FILES['file']['name'],'./assets/upload/resource_kursus/');
        if($fileName!=null){
          if($this->Kursus_model->insertImg($id,$fileName,$_POST['keterangan'])){
            $uploaded= array(
              'file' => $fileName,'upload' => true,
              'newid' => $this->Kursus_model->getIDRes($id)
            );
          }else{
            $uploaded= array(
              'file' => $fileName,'upload' => false
            );
          }
          $data['result'] = $uploaded;
          echo json_encode($data);
        }
      }
    }
  }

  public function delete_res(){
    $id = $_POST['id_resource'];
    if($id!=null){
        $resource = $this->Kursus_model->getResourceByIdRes($id)->row_array();
        $filePath = './assets/upload/resource_kursus/'.$resource['filename'];
        if(file_exists($filePath)){
          if($this->Kursus_model->deleteIMGQuery($id)){
            unlink($filePath);
            $data['result'] = true;
            $data['message'] = "berhasil menghapus ".$resource['filename'];
          }
        }else{
            $data['result'] = false;
            $data['message'] = "File ".$resource['filename']." Tidak Ditemukan!";
        }
        echo json_encode($data);
    }

  }

  public function getAccess(){
    $login_button = '';
    if(!$this->data['pengguna'])
    {
        $path = $_SERVER['DOCUMENT_ROOT']."/vendor/autoload.php";
        require_once $path;
        $google_client = new Google_Client();
        $google_client->setClientId('996506495419-pnlbjbokslr7l3l79v4g4lcvckas3let.apps.googleusercontent.com'); //Define your ClientID
        $google_client->setClientSecret('qNgHf2vOx9_vxAFG9RK5dndO'); //Define your Client Secret Key
        $google_client->setRedirectUri(base_url().'login'); //Define your Redirect Uri
        $google_client->addScope('email');
        $google_client->addScope('profile');
        $login_button = '<a href="'.$google_client->createAuthUrl().'"><font color="black">Login</font></a>';
        return $login_button;
    }
    else
    {
      return $login_button;
    }
  }

  // Move Upload
  public function move_upload($tmp_name,$name,$path){
    $tempFile = $tmp_name;
    $temp = $name;
    $path_parts = pathinfo($temp);
    $t = preg_replace('/\s+/', '', microtime());
    $fileName = $path_parts['filename']. $t . '.' . $path_parts['extension'];
    $targetPath = $path;
    $targetFile = $targetPath . $fileName ;
    if(move_uploaded_file($tempFile, $targetFile)){
      return $fileName;
    }else{
      return null;
    }
  }

  // FORM VALIDATION
	public function formvalid(){
		$this->form_validation->set_error_delimiters('', '');
    $this->form_validation->set_rules('nama_kursus','Nama Kursus','trim|required');
    $this->form_validation->set_rules('deskripsi','Deskripsi','trim|required');
    $this->form_validation->set_rules('kontak_kursus','No. Hp / Kontak','trim|required');
    $this->form_validation->set_rules('lokasi','Link Lokasi','trim|required');
		if($this->form_validation->run()==false){
			return true;
		}else{
			return false;
		}

	}




}
?>
