<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Kelas_controller extends CI_Controller {
    private $data=null;
    public function __construct(){
        parent::__construct();
        $this->load->database();
        $this->load->library('form_validation');
        $this->load->library('pagination');
        $this->load->model('Pengguna_model');
        $this->load->model('Admin/Kelas_model');
        $this->load->model('Admin/Kursus_model');
        $this->load->model('Admin/Pengajar_model');
        $this->load->model('Admin/Setting_model');
        $this->load->model('Admin/Cabang_model');
        $this->data['max_res_kelas'] = $this->Setting_model->getSettingById(1)->row_array()['max_res_kelas'];
        $this->data['max_kelas'] = $this->Setting_model->getSettingById(1)->row_array()['max_kelas'];
        $this->data['pengguna'] = $this->Pengguna_model->getpenggunaById($this->session->userdata('pengguna'))->row_array();
        $this->data['login_button'] = $this->getAccess();

        }


	public function index()
	{
    if($this->uri->segment(2)== NULL){ $start=0; } else { $start=$this->uri->segment(2); }
    $this->data['keyword'] = "";$this->data['tags'] = "";$this->data['sort']="";
    if($this->input->get('keyword')!= null){ $this->data['keyword'] = $this->input->get('keyword'); }
    if($this->input->get('tags')!= null){ $this->data['tags'] = $this->input->get('tags'); }
    if($this->input->get('sort')!= null){ $this->data['sort'] = $this->input->get('sort'); }
    $per_page=4;
    $this->data['cabang'] = $this->Cabang_model->getAllCabang()->result_array();
    $query = $this->Kelas_model->getAllApprovedKelas();
    $total_page= $query->num_rows();
    $this->setting_pagination($total_page,$per_page);
    $this->data['kelas'] = $this->Kelas_model->getApprovedKelasByKeyword($per_page,$start,$this->data['keyword'],$this->data['tags'],$this->data['sort'])->result_array();
    $this->data['thumbnail'] = array();
    foreach($this->data['kelas'] as $k) :
      $this->data['thumbnail'][] =  $this->Kelas_model->getResourceByIdKelas($k['id_kelas'])->row_array();
    endforeach;
    $this->load->view('template/header',$this->data);
    $this->load->view('kelas',$this->data);
    $this->load->view('template/footer');

  }


  public function detail(){
    $id = $this->uri->segment(3);
    $query= $this->Kelas_model->getKelasById($id)->row_array();
    if($query != NULL){
      $this->data['main_resource'] = $this->Kelas_model->getResourceByIdKelas($id)->row_array();
      $this->data['resource'] = $this->Kelas_model->getResourceByIdKelas($id)->result_array();
      $this->data['main_resource_kursus'] = $this->Kursus_model->getResourceByIdKursus($query['id_kursus_p'])->row_array();
      $this->data['resource_kursus'] = $this->Kursus_model->getResourceByIdKursus($query['id_kursus_p'])->result_array();
      $this->data['kelas'] = $query;
      $this->load->view('template/header',$this->data);
      $this->load->view('kelas-detail',$this->data);
      $this->load->view('template/footer');
    }

  }

  public function tambah(){
    if($this->data['pengguna']!= null){
      if($this->data['pengguna']['no_telp']== null || $this->data['pengguna']['tgl_lahir']== null){
        $this->session->set_flashdata('message','<div class="alert alert-warning" style="text-align: center;" role="alert">
          Silahkan lengkapi data diri anda!!!</div>');
          redirect('profile');
      }
      $this->data['infokursus'] = $this->Kursus_model->getApprovedKursusByIdPengguna($this->data['pengguna']['id_pengguna'])->row_array();
        if($this->data['infokursus']==NULL){
           redirect('dashboard');
        }
      $infokelas = $this->Kelas_model->getKelasByIdPengguna($this->data['pengguna']['id_pengguna'])->num_rows();
      if($infokelas < $this->data['max_kelas']  ){ // ditolak
      	$this->load->view('template/header',$this->data);
      	$this->load->view('tambah-kelas',$this->data);
      	$this->load->view('template/footer');
      }else{
        $this->session->set_flashdata('message','<div class="alert alert-danger" role="alert">
    	   Kamu sudah mencapai maksimal jumlah kelas !</div>');
      	redirect('dashboard');
      }
    }else{
      $this->session->set_flashdata('message','<div class="alert alert-warning" style="text-align: center;" role="alert">
     Silahkan login terlebih dahulu!!!</div>');
     redirect('');
    }

  }

  public function manage(){
    if($this->data['pengguna']!= null){
      if($this->data['pengguna']['no_telp']== null || $this->data['pengguna']['tgl_lahir']== null){
        $this->session->set_flashdata('message','<div class="alert alert-warning" style="text-align: center;" role="alert">
          Silahkan lengkapi data diri anda!!!</div>');
          redirect('profile');
      }
      $this->data['infokursus'] = $this->Kursus_model->getApprovedKursusByIdPengguna($this->data['pengguna']['id_pengguna'])->row_array();
        if($this->data['infokursus']==NULL){
           redirect('dashboard');
        }
      $id = $this->_getId('kls',$this->uri->segment(3));
      $infokelas = $this->Kelas_model->getKelasByIdAndPengguna($this->data['pengguna']['id_pengguna'],$id)->row_array();
      if($infokelas != null ){ // ditolak
      	$this->data['kelas'] = $infokelas;
      	$this->data['resource'] = $this->Kelas_model->getResourceByIdKelas($infokelas['id_kelas'])->result_array();
      	$this->data['countres'] = $this->Kelas_model->getResourceByIdKelas($infokelas['id_kelas'])->num_rows();
      	$this->data['status'] = explode(",",$this->Setting_model->getSettingById(1)->row_array()['setting_status']);
      	// form valid
        $this->form_validation->set_rules('nama_kelas','Nama Kelas','trim|required');
        $this->form_validation->set_rules('deskripsi_kelas','Deskripsi','trim|required');
        $this->form_validation->set_rules('link','Link Gform','trim|required');
        $this->form_validation->set_rules('biaya','Biaya','trim|required');
        $this->form_validation->set_rules('jadwal_latihan','Jadwal Latihan','trim|required');
        $this->form_validation->set_rules('jangka_wkt','Jangka Waktu','trim|required');
        $this->form_validation->set_rules('id_kursus_p','ID Kursus','trim|required');
        $this->form_validation->set_rules('nama_pengajar','Nama Pengajar','trim|required');
        $this->form_validation->set_rules('kontak_kelas','Kontak','trim|required');
      	if($this->form_validation->run() == FALSE){
      		$this->load->view('template/header',$this->data);
      		$this->load->view('detail-kelas',$this->data);
      		$this->load->view('template/footer');
      	}else{
      		$this->_update($infokelas['id_kelas']);
      	}
      }else{
      	redirect('dashboard');
      }
    }else{
      $this->session->set_flashdata('message','<div class="alert alert-warning" style="text-align: center;" role="alert">
     Silahkan login terlebih dahulu!!!</div>');
     redirect('');
    }

  }

  public function hapus(){
    $id = $this->_getId('kls',$this->uri->segment(3));
    if($id!= null){
      // delete resource
      $kelas = $this->Kelas_model->getKelasById($id)->row_array();
      $resource = $this->Kelas_model->getResourceByIdKelas($id)->result_array();
      $rescount = $this->Kelas_model->getResourceByIdKelas($id)->num_rows();
      if($rescount > 0){
        foreach ($resource as $res ) :
          $filePath = './assets/upload/resource_kelas/'.$res['filename'];
          if($res['filename'] != 'default.png'){
            if(file_exists($filePath)){
                unlink($filePath);
            }
          }
        endforeach;
      }
      if($this->Kelas_model->deleteQuery($id)){
        $this->session->set_flashdata('message','<div class="alert alert-success" role="alert">Success!</div>');
       redirect('dashboard');
      }else{
        $this->session->set_flashdata('message','<div class="alert alert-danger" role="alert">Failed!</div>');
       redirect('dashboard');
      }
    }
  }

  private function _getId($keyword,$uri){
    if (strpos($uri, $keyword) !== false) {
      if($this->Kelas_model->getKelasById(preg_replace('/\D/', '', $uri))->num_rows()>0){
        return preg_replace('/\D/', '', $uri);
      }else{
        return null;
      }
    }else{
      return null;
    }
  }

  private function _update($id){
  	// apakah upload logo
    if($id!=null){
      if($this->Kelas_model->updateQuery($id)){
        if($this->Pengajar_model->updateQuery($this->input->post('id_pengajar'))){
          $this->session->set_flashdata('message','<div class="alert alert-success" role="alert">
         Success!</div>');
         redirect('kelas/manage/kls'.$id);
        }
      }else{
        $this->session->set_flashdata('message','<div class="alert alert-danger" role="alert">
       Erorr!</div>');
       redirect('kelas/manage/kls'.$id);
      }
    }

  }

  public function add(){
  	// Add
  		if($this->formvalid()){
  		$array = array(
  			'error'   => true,
  			'msg' => array(
          form_error('nama_kelas'),
          form_error('deskripsi_kelas'),
          form_error('link'),
          form_error('kontak_kelas'),
          form_error('biaya'),
          form_error('jadwal_latihan'),
          form_error('jangka_wkt'),
          form_error('id_kursus_p'),
          form_error('nama_pengajar'),
          ),
  			 );
  		$data['form_error']= $array;
  		echo json_encode($data);
  	}else{ // if form valid
  		$array = array('error'   => false,'insert' => false );
  		$idpengajar = "";
  		// input pengajar
  		if($this->input->post('nama_pengajar')!= null){
        if($this->Pengajar_model->insertQuery()){
          $idpengajar = $this->Pengajar_model->getID();
        }
  		}
  		if($this->Kelas_model->insertQuery($idpengajar)){ // if insert valid
  			$array = array('error'   => false,'insert' => true );
  			$id = $this->Kelas_model->getID();
  			$uploaded = array();
  			if(!empty($_FILES['file']['name'][0])){ // if there is img
  				foreach ($_FILES['file']['name'] as $index => $name) {
  					$tempFile = $_FILES['file']['tmp_name'][$index];
  					$temp = $_FILES["file"]["name"][$index];
  					$fileName = $this->move_upload($tempFile,$temp,'./assets/upload/resource_kelas/');
  					if($fileName!=null){
  						if($this->Kelas_model->insertImg($id,$fileName,$_POST['keterangan'][$index])){
  							$uploaded[]= array('file' => $tempFile,'name' => $fileName,);
  						}
  					}
  				}
  				$data['uploaded'] = $uploaded;
  				$data ['_id'] = $id;
  				// }
  			}
  		}
  		$data['form_error']= $array;
  		echo json_encode($data);
  	}
  }


  public function upload_single(){
  	$id = $_POST['id_kelas'];
  	$uploaded = array();
  	if($id!=null){
  		if(!empty($_FILES['file']['name'])){
  			// upload files
  			$fileName = $this->move_upload($_FILES['file']['tmp_name'],$_FILES['file']['name'],'./assets/upload/resource_kelas/');
  			if($fileName!=null){
  				if($this->Kelas_model->insertImg($id,$fileName,$_POST['keterangan'])){
  					$uploaded= array(
  						'file' => $fileName,'upload' => true,
  						'newid' => $this->Kelas_model->getIDRes($id)
  					);
  				}else{
  					$uploaded= array(
  						'file' => $fileName,'upload' => false
  					);
  				}
  				$data['result'] = $uploaded;
  				echo json_encode($data);
  			}
  		}
  	}
  }

  public function delete_res(){
  	$id = $_POST['id_resource'];
  	if($id!=null){
  			$resource = $this->Kelas_model->getResourceById($id)->row_array();
  			$filePath = './assets/upload/resource_kelas/'.$resource['filename'];
  			if(file_exists($filePath)){
  				if($this->Kelas_model->deleteIMGQuery($id)){
  					unlink($filePath);
  					$data['result'] = true;
  					$data['message'] = "berhasil menghapus ".$resource['filename'];
  				}
  			}else{
  					$data['result'] = false;
  					$data['message'] = "File ".$resource['filename']." Tidak Ditemukan!";
  			}
  			echo json_encode($data);
  	}

  }

  // Move Upload
  public function move_upload($tmp_name,$name,$path){
  	$tempFile = $tmp_name;
  	$temp = $name;
  	$path_parts = pathinfo($temp);
  	$t = preg_replace('/\s+/', '', microtime());
  	$fileName = $path_parts['filename']. $t . '.' . $path_parts['extension'];
  	$targetPath = $path;
  	$targetFile = $targetPath . $fileName ;
  	if(move_uploaded_file($tempFile, $targetFile)){
  		return $fileName;
  	}else{
  		return null;
  	}
  }

  // FORM VALIDATION
  public function formvalid(){
  	$this->form_validation->set_error_delimiters('', '');
    $this->form_validation->set_rules('nama_kelas','Nama Kelas','trim|required');
    $this->form_validation->set_rules('deskripsi_kelas','Deskripsi','trim|required');
    $this->form_validation->set_rules('link','Link Gform','trim|required');
    $this->form_validation->set_rules('kontak_kelas','Kontak','trim|required');
    $this->form_validation->set_rules('biaya','Biaya','trim|required');
    $this->form_validation->set_rules('jadwal_latihan','Jadwal Latihan','trim|required');
    $this->form_validation->set_rules('jangka_wkt','Jangka Waktu','trim|required');
    $this->form_validation->set_rules('id_kursus_p','ID Kursus','trim|required');
    $this->form_validation->set_rules('nama_pengajar','Nama Pengajar','trim|required');
  	if($this->form_validation->run()==false){
  		return true;
  	}else{
  		return false;
  	}

  }

  public function getAccess(){
    $login_button = '';
    if(!$this->data['pengguna'])
    {
        $path = $_SERVER['DOCUMENT_ROOT']."/vendor/autoload.php";
        require_once $path;
        $google_client = new Google_Client();
        $google_client->setClientId('996506495419-pnlbjbokslr7l3l79v4g4lcvckas3let.apps.googleusercontent.com'); //Define your ClientID
        $google_client->setClientSecret('qNgHf2vOx9_vxAFG9RK5dndO'); //Define your Client Secret Key
        $google_client->setRedirectUri(base_url().'login'); //Define your Redirect Uri
        $google_client->addScope('email');
        $google_client->addScope('profile');
        $login_button = '<a href="'.$google_client->createAuthUrl().'"><font color="black">Login</font></a>';
        return $login_button;
    }
    else
    {
      return $login_button;
    }
  }

    // PAGINATION
  public function setting_pagination($query,$per_page){
    $config['per_page']=$per_page;
    $config['total_rows'] = $query;
    $config['base_url'] = base_url()."kelas";
    $config['full_tag_open']    = '<div class="pagging text-center"><nav><ul class="pagination">';
        $config['full_tag_close']   = '</ul></nav></div>';
        $config['num_tag_open']     = '<li class="page-item"><span class="page-link">';
        $config['num_tag_close']    = '</span></li>';
        $config['cur_tag_open']     = '<li class="page-item active"><span class="page-link">';
        $config['cur_tag_close']    = '<span class="sr-only">(current)</span></span></li>';
        $config['next_tag_open']    = '<li class="page-item"><span class="page-link">';
        $config['next_tag_close']  = '<span aria-hidden="true"></span></span></li>';
        $config['prev_tag_open']    = '<li class="page-item"><span class="page-link">';
        $config['prev_tag_close']  = '</span></li>';
        $config['first_tag_open']   = '<li class="page-item"><span class="page-link">';
        $config['first_tag_close'] = '</span></li>';
        $config['last_tag_open']    = '<li class="page-item"><span class="page-link">';
    $config['last_tag_close']  = '</span></li>';
    $this->pagination->initialize($config);
    //$this->data['pagination'] = $this->pagination->create_links();
  }

}
?>
