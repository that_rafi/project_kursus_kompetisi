<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login_controller extends CI_Controller {
    private $data=null;
    public function __construct(){
        parent::__construct();
        $this->load->database();
        $this->load->library('form_validation');
        $this->load->library('pagination');
        $this->load->model('Pengguna_model');
        }


	public function index()
	{
        $this->_login();
    }

    private function _login(){
        $path = $_SERVER['DOCUMENT_ROOT']."/vendor/autoload.php";
        require_once $path;
        $google_client = new Google_Client();
        $google_client->setClientId('996506495419-pnlbjbokslr7l3l79v4g4lcvckas3let.apps.googleusercontent.com'); //Define your ClientID
        $google_client->setClientSecret('qNgHf2vOx9_vxAFG9RK5dndO'); //Define your Client Secret Key
        $google_client->setRedirectUri(base_url().'login'); //Define your Redirect Uri
        $google_client->addScope('email');
        $google_client->addScope('profile');
        if(isset($_GET["code"]))
        {
            $token = $google_client->fetchAccessTokenWithAuthCode($_GET["code"]);
                if(!isset($token["error"]))
                {
                    $google_client->setAccessToken($token['access_token']);
                    $this->session->set_userdata('access_token', $token['access_token']);
                    $google_service = new Google_Service_Oauth2($google_client);
                    $data = $google_service->userinfo->get();
                    $current_datetime = date('Y-m-d H:i:s');
                    $nama = $data['given_name']." ".$data['family_name'];
                    $email = $data['email'];
                        if($this->Pengguna_model->isUserExist($data['id']))
                        {
                            //update data
                            $pengguna = $this->Pengguna_model->getpenggunaById($data['id'])->row_array();
                            $this->Pengguna_model->updateQuery($data['id'],$nama,$email,$pengguna['tgl_lahir'],$pengguna['alamat'],$pengguna['t_badan'],$pengguna['b_badan'],$pengguna['no_telp'],$pengguna['prestasi'],$pengguna['foto']);
                        }
                        else
                        {
                        //insert data
                            $foto = $data['picture'];$tgl_lahir = "";
                            $alamat="";$t_badan="";$b_badan="";$no="";$prestasi="";
                            $this->Pengguna_model->insertQuery($data['id'],$nama,$email,$tgl_lahir,$alamat,$t_badan,$b_badan,$no,$prestasi,$foto);
                        }
                        $this->session->set_userdata('pengguna', $data['id']);
                        redirect(site_url());

                }
            }

    }

    function logout()
    {
    $this->session->unset_userdata('access_token');
    $this->session->unset_userdata('pengguna');
    redirect(site_url(),'refresh');
    }

}
?>
