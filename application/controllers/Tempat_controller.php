<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Tempat_controller extends CI_Controller {
    private $data=null;
    public function __construct(){
        parent::__construct();
        $this->load->database();
        $this->load->library('form_validation');
        $this->load->library('pagination');
        $this->load->model('Pengguna_model');
        $this->load->model('Admin/Tempat_model');
        $this->load->model('Admin/Cabang_model');
        $this->load->model('Admin/Setting_model');
        $this->data['pengguna'] = $this->Pengguna_model->getpenggunaById($this->session->userdata('pengguna'))->row_array();
        $this->data['login_button'] = $this->getAccess();
        $this->data['status'] = explode(",",$this->Setting_model->getSettingById(1)->row_array()['setting_status']);
        $this->data['max_res_tempat'] = $this->Setting_model->getSettingById(1)->row_array()['max_res_tempat'];
        $this->data['max_tempat'] = $this->Setting_model->getSettingById(1)->row_array()['max_tempat'];
        $this->data['cabang'] = $this->Cabang_model->getAllCabang()->result_array();
        }

        public function index()
      	{
          if($this->uri->segment(2)== NULL){ $start=0; } else { $start=$this->uri->segment(2); }
          $this->data['keyword'] = "";$this->data['tags'] = "";$this->data['sort']="";
          if($this->input->get('keyword')!= null){ $this->data['keyword'] = $this->input->get('keyword'); }
          if($this->input->get('tags')!= null){ $this->data['tags'] = $this->input->get('tags'); }
          if($this->input->get('sort')!= null){ $this->data['sort'] = $this->input->get('sort'); }
          $per_page=4;
          $this->data['cabang'] = $this->Cabang_model->getAllCabang()->result_array();
          // tempat
          $query = $this->Tempat_model->getAllApprovedTempat();
          $total_page= $query->num_rows();
          $this->setting_pagination($total_page,$per_page);
          $this->data['tempat'] = $this->Tempat_model->getApprovedTempatByKeyword($per_page,$start,$this->data['keyword'],$this->data['tags'],$this->data['sort'])->result_array();
          $this->data['thumbnail'] = array();
          foreach($this->data['tempat'] as $t) :
            $this->data['thumbnail'][] =  $this->Tempat_model->getResourceByIdTempat($t['id_tempat'])->row_array();
          endforeach;
          $this->load->view('template/header',$this->data);
          $this->load->view('tempat',$this->data);
          $this->load->view('template/footer');
        }


  public function detail(){
    $id = $this->uri->segment(3);
    $query= $this->Tempat_model->getTempatById($id)->row_array();
    if($query != NULL){
      $this->data['main_resource'] = $this->Tempat_model->getResourceByIdTempat($id)->row_array();
      $this->data['resource'] = $this->Tempat_model->getResourceByIdTempat($id)->result_array();
      $this->data['tempat'] = $query;
      $this->load->view('template/header',$this->data);
      $this->load->view('tempat-detail',$this->data);
      $this->load->view('template/footer');
  }
}

public function tambah(){
  if($this->data['pengguna']!=null){
    $infotempat = $this->Tempat_model->getTempatByIdPengguna($this->data['pengguna']['id_pengguna'])->num_rows();
    if($infotempat < $this->data['max_tempat']  ){ // ditolak
      $this->load->view('template/header',$this->data);
      $this->load->view('tambah-tempat',$this->data);
      $this->load->view('template/footer');
    }else{
      $this->session->set_flashdata('message','<div class="alert alert-danger" role="alert">
       Kamu sudah mencapai maksimal jumlah tempat !</div>');
      redirect('dashboard');
    }
  }else{
    $this->session->set_flashdata('message','<div class="alert alert-warning" style="text-align: center;" role="alert">
   Silahkan login terlebih dahulu!!!</div>');
   redirect('');
  }

}

private function _getId($keyword,$uri){
  if (strpos($uri, $keyword) !== false) {
    if($this->Tempat_model->getTempatById(preg_replace('/\D/', '', $uri))->num_rows()>0){
      return preg_replace('/\D/', '', $uri);
    }else{
      return null;
    }
  }else{
    return null;
  }
}

public function add(){
  // Add
    if($this->formvalid()){
    $array = array(
      'error'   => true,
      'msg' => array(
        form_error('createdBy'),
        form_error('nama_tempat'),
        form_error('deskripsi'),
        form_error('lokasi'),
        form_error('biaya_sewa'),
        form_error('jam'),
        form_error('kontak')
        ),
       );
    $data['form_error']= $array;
    echo json_encode($data);
  }else{ // if form valid
    $array = array('error'   => false,'insert' => false );
    if($this->Tempat_model->insertQuery()){ // if insert valid
      $array = array('error'   => false,'insert' => true );
      $id = $this->Tempat_model->getID();
      $uploaded = array();
      if(!empty($_FILES['file']['name'][0])){ // if there is img
        foreach ($_FILES['file']['name'] as $index => $name) {
          $tempFile = $_FILES['file']['tmp_name'][$index];
          $temp = $_FILES["file"]["name"][$index];
          $fileName = $this->move_upload($tempFile,$temp,'./assets/upload/resource_tempat/');
          if($fileName!=null){
            if($this->Tempat_model->insertImg($id,$fileName,$_POST['keterangan'][$index])){
              $uploaded[]= array('file' => $tempFile,'name' => $fileName,);
            }
          }
        }
        $data['uploaded'] = $uploaded;
        $data ['_id'] = $id;
        // }
      }
    }
    $data['form_error']= $array;
    echo json_encode($data);
  }
}

public function manage(){
  if($this->data['pengguna']!=null){
    $id = $this->_getId('tpt',$this->uri->segment(3));
    $infotempat = $this->Tempat_model->getTempatByIdAndPengguna($this->data['pengguna']['id_pengguna'],$id)->row_array();
    if($infotempat != null ){ // ditolak
      $this->data['tempat'] = $infotempat;
      $this->data['resource'] = $this->Tempat_model->getResourceByIdTempat($infotempat['id_tempat'])->result_array();
      $this->data['countres'] = $this->Tempat_model->getResourceByIdTempat($infotempat['id_tempat'])->num_rows();
      $this->data['status'] = explode(",",$this->Setting_model->getSettingById(1)->row_array()['setting_status']);
      // form valid
      $this->form_validation->set_rules('nama_tempat','Nama tempat','trim|required');
      $this->form_validation->set_rules('deskripsi','Deskripsi','trim|required');
      $this->form_validation->set_rules('lokasi','Link Gmaps','trim|required');
      $this->form_validation->set_rules('biaya_sewa','Biaya','trim|required');
      $this->form_validation->set_rules('createdBy','ID Pengguna','trim|required');
      $this->form_validation->set_rules('jam','Jadwal Operasional','trim|required');
      $this->form_validation->set_rules('kontak','Kontak','trim|required');
      if($this->form_validation->run() == FALSE){
        $this->load->view('template/header',$this->data);
        $this->load->view('detail-tempat',$this->data);
        $this->load->view('template/footer');
      }else{
        $this->_update($infotempat['id_tempat']);
      }
    }else{
      redirect('dashboard');
    }
  }else{
    $this->session->set_flashdata('message','<div class="alert alert-warning" style="text-align: center;" role="alert">
   Silahkan login terlebih dahulu!!!</div>');
   redirect('');
  }

}

private function _update($id){
  // apakah upload logo
  if($id!=null){
    if($this->Tempat_model->updateQuery($id)){
        $this->session->set_flashdata('message','<div class="alert alert-success" role="alert">
       Success!</div>');
       redirect('tempat/manage/tpt'.$id);
    }else{
      $this->session->set_flashdata('message','<div class="alert alert-danger" role="alert">
     Erorr!</div>');
     redirect('tempat/manage/tpt'.$id);
    }
  }

}

public function hapus(){
  $id = $this->_getId('tpt',$this->uri->segment(3));
  if($id!= null){
    // delete resource
    $tempat = $this->Tempat_model->getTempatById($id)->row_array();
    $resource = $this->Tempat_model->getResourceByIdTempat($id)->result_array();
    $rescount = $this->Tempat_model->getResourceByIdTempat($id)->num_rows();
    if($rescount > 0){
      foreach ($resource as $res ) :
        $filePath = './assets/upload/resource_tempat/'.$res['filename'];
        if($res['filename'] != 'default.png'){
          if(file_exists($filePath)){
              unlink($filePath);
          }
        }
      endforeach;
    }
    if($this->Tempat_model->deleteQuery($id)){
      $this->session->set_flashdata('message','<div class="alert alert-success" role="alert">Success!</div>');
     redirect('dashboard');
    }else{
      $this->session->set_flashdata('message','<div class="alert alert-danger" role="alert">Failed!</div>');
     redirect('dashboard');
    }
  }
}

public function upload_single(){
  $id = $_POST['id_tempat'];
  $uploaded = array();
  if($id!=null){
    if(!empty($_FILES['file']['name'])){
      // upload files
      $fileName = $this->move_upload($_FILES['file']['tmp_name'],$_FILES['file']['name'],'./assets/upload/resource_tempat/');
      if($fileName!=null){
        if($this->Tempat_model->insertImg($id,$fileName,$_POST['keterangan'])){
          $uploaded= array(
            'file' => $fileName,'upload' => true,
            'newid' => $this->Tempat_model->getIDRes($id)
          );
        }else{
          $uploaded= array(
            'file' => $fileName,'upload' => false
          );
        }
        $data['result'] = $uploaded;
        echo json_encode($data);
      }
    }
  }
}

public function delete_res(){
  $id = $_POST['id_resource'];
  if($id!=null){
      $resource = $this->Tempat_model->getResourceById($id)->row_array();
      $filePath = './assets/upload/resource_tempat/'.$resource['filename'];
      if(file_exists($filePath)){
        if($this->Tempat_model->deleteIMGQuery($id)){
          unlink($filePath);
          $data['result'] = true;
          $data['message'] = "berhasil menghapus ".$resource['filename'];
        }
      }else{
          $data['result'] = false;
          $data['message'] = "File ".$resource['filename']." Tidak Ditemukan!";
      }
      echo json_encode($data);
  }

}

// Move Upload
public function move_upload($tmp_name,$name,$path){
  $tempFile = $tmp_name;
  $temp = $name;
  $path_parts = pathinfo($temp);
  $t = preg_replace('/\s+/', '', microtime());
  $fileName = $path_parts['filename']. $t . '.' . $path_parts['extension'];
  $targetPath = $path;
  $targetFile = $targetPath . $fileName ;
  if(move_uploaded_file($tempFile, $targetFile)){
    return $fileName;
  }else{
    return null;
  }
}

// FORM VALIDATION
public function formvalid(){
  $this->form_validation->set_error_delimiters('', '');
  $this->form_validation->set_rules('nama_tempat','Nama tempat','trim|required');
  $this->form_validation->set_rules('deskripsi','Deskripsi','trim|required');
  $this->form_validation->set_rules('lokasi','Link Gmaps','trim|required');
  $this->form_validation->set_rules('biaya_sewa','Biaya','trim|required');
  $this->form_validation->set_rules('createdBy','ID Pengguna','trim|required');
  $this->form_validation->set_rules('jam','Jadwal Operasional','trim|required');
  $this->form_validation->set_rules('kontak','Kontak','trim|required');
  if($this->form_validation->run()==false){
    return true;
  }else{
    return false;
  }

}

  public function getAccess(){
    $login_button = '';
    if(!$this->data['pengguna'])
    {
        $path = $_SERVER['DOCUMENT_ROOT']."/vendor/autoload.php";
        require_once $path;
        $google_client = new Google_Client();
        $google_client->setClientId('996506495419-pnlbjbokslr7l3l79v4g4lcvckas3let.apps.googleusercontent.com'); //Define your ClientID
        $google_client->setClientSecret('qNgHf2vOx9_vxAFG9RK5dndO'); //Define your Client Secret Key
        $google_client->setRedirectUri(base_url().'login'); //Define your Redirect Uri
        $google_client->addScope('email');
        $google_client->addScope('profile');
        $login_button = '<a href="'.$google_client->createAuthUrl().'"><font color="black">Login</font></a>';
        return $login_button;
    }
    else
    {
      return $login_button;
    }
  }

  // PAGINATION
public function setting_pagination($query,$per_page){
  $config['per_page']=$per_page;
  $config['total_rows'] = $query;
  $config['base_url'] = base_url()."tempat";
  $config['full_tag_open']    = '<div class="pagging text-center"><nav><ul class="pagination">';
      $config['full_tag_close']   = '</ul></nav></div>';
      $config['num_tag_open']     = '<li class="page-item"><span class="page-link">';
      $config['num_tag_close']    = '</span></li>';
      $config['cur_tag_open']     = '<li class="page-item active"><span class="page-link">';
      $config['cur_tag_close']    = '<span class="sr-only">(current)</span></span></li>';
      $config['next_tag_open']    = '<li class="page-item"><span class="page-link">';
      $config['next_tag_close']  = '<span aria-hidden="true"></span></span></li>';
      $config['prev_tag_open']    = '<li class="page-item"><span class="page-link">';
      $config['prev_tag_close']  = '</span></li>';
      $config['first_tag_open']   = '<li class="page-item"><span class="page-link">';
      $config['first_tag_close'] = '</span></li>';
      $config['last_tag_open']    = '<li class="page-item"><span class="page-link">';
  $config['last_tag_close']  = '</span></li>';
  $this->pagination->initialize($config);
  //$this->data['pagination'] = $this->pagination->create_links();
}

}
?>
