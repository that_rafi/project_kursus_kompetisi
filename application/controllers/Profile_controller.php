<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Profile_controller extends CI_Controller {
    private $data=null;
    public function __construct(){
        parent::__construct();
        $this->load->database();
        $this->load->library('form_validation');
        $this->load->library('pagination');
        $this->load->model('Pengguna_model');
        $this->data['pengguna'] = $this->Pengguna_model->getpenggunaById($this->session->userdata('pengguna'))->row_array();
        $this->data['login_button'] = $this->getAccess();
            if($this->data['pengguna']== null){
               $this->session->set_flashdata('message','<div></div>');
             redirect('');
            }
        }


	public function index()
	{
      if($this->data['pengguna']!=null){
        $this->form_validation->set_rules('nama','Nama','trim|required');
        $this->form_validation->set_rules('email','Email','trim|required');
        $this->form_validation->set_rules('tgl_lahir','Tanggal Lahir','trim|required');
        $this->form_validation->set_rules('t_badan','Tinggi Badan','trim|required');
        $this->form_validation->set_rules('b_badan','Berat Badan','trim|required');
        $this->form_validation->set_rules('no_telp','Nomor Telepon','trim|required');
        if($this->form_validation->run() == FALSE){
        $this->load->view('template/header',$this->data);
        $this->load->view('profile',$this->data);
        $this->load->view('template/footer');
      }else{
        $this->_update();
      }
    }
  }

  private function _update(){
    $id = $this->input->post('id_pengguna');
    $nama = $this->input->post('nama');
    $email = $this->input->post('email');
    $tgl_lahir = $this->input->post('tgl_lahir');
    $t_badan = $this->input->post('t_badan');
    $b_badan = $this->input->post('b_badan');
    $no_tlp = $this->input->post('no_telp');
    $alamat = $this->input->post('alamat');
    $prestasi = $this->input->post('prestasi');
    $foto = $this->input->post('foto');
    if($_FILES['file']['name']!=null){
      $path_parts = pathinfo($_FILES['file']['name']);
      $foto = $email."_".$id.'.'.$path_parts['extension'];
    }

    if($this->Pengguna_model->updateQueryById($id,$nama,$email,$tgl_lahir,$alamat,$t_badan,$b_badan,$no_tlp,$prestasi,$foto)){
      if($_FILES['file']['name']!=null){
        // replace if there is foto in folder
        $filePath = './assets/upload/pengguna/'.$this->input->post('foto');
        if(file_exists($filePath)){
            unlink($filePath);
        }
        // save to drive
        move_uploaded_file($_FILES['file']['tmp_name'], './assets/upload/pengguna/'.$foto);
      }
      $this->session->set_flashdata('message','<div class="alert alert-success" role="alert">
     Success!</div>');
     redirect('dashboard');
    }else{
      $this->session->set_flashdata('message','<div class="alert alert-danger" role="alert">
     Failed!</div>');
     redirect('profile');
    }
  }


  public function getAccess(){
    $login_button = '';
    if(!$this->data['pengguna'])
    {
        $path = $_SERVER['DOCUMENT_ROOT']."/vendor/autoload.php";
        require_once $path;
        $google_client = new Google_Client();
        $google_client->setClientId('996506495419-pnlbjbokslr7l3l79v4g4lcvckas3let.apps.googleusercontent.com'); //Define your ClientID
        $google_client->setClientSecret('qNgHf2vOx9_vxAFG9RK5dndO'); //Define your Client Secret Key
        $google_client->setRedirectUri(base_url().'login'); //Define your Redirect Uri
        $google_client->addScope('email');
        $google_client->addScope('profile');
        $login_button = '<a href="'.$google_client->createAuthUrl().'"><font color="black">Login</font></a>';
        return $login_button;
    }
    else
    {
      return $login_button;
    }
  }


}
?>
