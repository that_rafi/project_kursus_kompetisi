<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Artikel_controller extends CI_Controller {
    private $data=null;
    public function __construct(){
        parent::__construct();
        $this->load->database();
        $this->load->library('form_validation');
        $this->load->library('pagination');
        $this->load->model('Pengguna_model');
        $this->load->model('Admin/Artikel_model');
        $this->data['pengguna'] = $this->Pengguna_model->getpenggunaById($this->session->userdata('pengguna'))->row_array();
        $this->data['login_button'] = $this->getAccess();
        // for sidebar
        $this->data['artikel_all'] = $this->Artikel_model->getAllArtikel()->result_array();
        $this->data['thumbnail_all'] = array();
        foreach($this->data['artikel_all'] as $a) :
          $this->data['thumbnail_all'][] =  $this->Artikel_model->getResourceById($a['id_artikel'])->row_array();
        endforeach;
        }


	public function index()
	{
    $keyword = "";
    if($this->uri->segment(2)== NULL){ $start=0; } else { $start=$this->uri->segment(2); }
    if($this->input->get('keyword')!= null){ $keyword = $this->input->get('keyword'); }
    $per_page=2;
    $query = $this->Artikel_model->getAllArtikel();
    $total_page= $query->num_rows();
    $this->setting_pagination($total_page,$per_page);
    $this->data['artikel'] = $this->Artikel_model->getArtikelByKeyword($per_page,$start,$keyword)->result_array();
    $this->data['thumbnail'] = array();
    foreach($this->data['artikel'] as $a) :
      $this->data['thumbnail'][] =  $this->Artikel_model->getResourceById($a['id_artikel'])->row_array();
    endforeach;
    $this->load->view('template/header',$this->data);
    $this->load->view('artikel',$this->data);
    $this->load->view('template/footer');
  }

  public function detail(){
    $id = $this->uri->segment(3);
    $query= $this->Artikel_model->getArtikelById($id)->row_array();
    if($query != NULL){
      $this->data['main_resource'] = $this->Artikel_model->getResourceById($id)->row_array();
      $this->data['resource'] = $this->Artikel_model->getResourceById($id)->result_array();
      $this->data['artikel'] = $query;
      $this->load->view('template/header',$this->data);
      $this->load->view('artikel-detail',$this->data);
      $this->load->view('template/footer');
  }
  }

  public function getAccess(){
    $login_button = '';
    if(!$this->data['pengguna'])
    {
        $path = $_SERVER['DOCUMENT_ROOT']."/vendor/autoload.php";
        require_once $path;
        $google_client = new Google_Client();
        $google_client->setClientId('996506495419-pnlbjbokslr7l3l79v4g4lcvckas3let.apps.googleusercontent.com'); //Define your ClientID
        $google_client->setClientSecret('qNgHf2vOx9_vxAFG9RK5dndO'); //Define your Client Secret Key
        $google_client->setRedirectUri(base_url().'login'); //Define your Redirect Uri
        $google_client->addScope('email');
        $google_client->addScope('profile');
        $login_button = '<a href="'.$google_client->createAuthUrl().'"><font color="black">Login</font></a>';
        return $login_button;
    }
    else
    {
      return $login_button;
    }
  }

  public function setting_pagination($query,$per_page){
    $config['per_page']=$per_page;
    $config['total_rows'] = $query;
    $config['base_url'] = base_url()."artikel";
    $config['full_tag_open']    = '<div class="pagging text-center"><nav><ul class="pagination">';
        $config['full_tag_close']   = '</ul></nav></div>';
        $config['num_tag_open']     = '<li class="page-item"><span class="page-link">';
        $config['num_tag_close']    = '</span></li>';
        $config['cur_tag_open']     = '<li class="page-item active"><span class="page-link">';
        $config['cur_tag_close']    = '<span class="sr-only">(current)</span></span></li>';
        $config['next_tag_open']    = '<li class="page-item"><span class="page-link">';
        $config['next_tag_close']  = '<span aria-hidden="true"></span></span></li>';
        $config['prev_tag_open']    = '<li class="page-item"><span class="page-link">';
        $config['prev_tag_close']  = '</span></li>';
        $config['first_tag_open']   = '<li class="page-item"><span class="page-link">';
        $config['first_tag_close'] = '</span></li>';
        $config['last_tag_open']    = '<li class="page-item"><span class="page-link">';
    $config['last_tag_close']  = '</span></li>';
    $this->pagination->initialize($config);
    //$this->data['pagination'] = $this->pagination->create_links();
  }

}
?>
