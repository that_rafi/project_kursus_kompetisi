<div class="container" style="margin-top:50px">
    <div class="row align-items-center">
        <div class="col-lg-12">
            <h2>Buat Kursusmu!</h2>
            <?= $this->session->flashdata('message'); ?>
            <?= $this->session->flashdata('error'); ?>
            <div id="msgtxt"></div>
            <h3>Informasi Kursus</h3>
            <hr>
            <form class="form-horizontal" method="post" enctype="multipart/form-data">
            <div class="row">
              <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6" style="float: center;">

                      <!--{GLOBAL_MESSAGES_aae3749ba9c2e308ffa9c240ac185959}-->
                      <!--/{GLOBAL_MESSAGES_aae3749ba9c2e308ffa9c240ac185959}-->

                      <script type="text/javascript" src="/js/sinaprinting/jquery.uploadfile.min.js"></script>
                        <div class="uploader_form" style="float: center;" align="center">
                          <input type="hidden" name="id" value="291">
                          <input type="hidden" name="productid" value="210">
                          <input type="hidden" name="sides" value="1">
                          <input type="hidden" name="templatesize" value="77">
                          <input type="hidden" name="product" value="18">
                          <input type="hidden" id="oneSideOrTwoSides" name="oneSideOrTwoSides" value="1">
                          <div class="fieldset">
                            <ul style="list-style: none;">
                              <li style="padding: 10px;">
                                <div class="button-set">
                                  <div class="ajax-upload-dragdrop upload-drag-and-drop-box" id="imgbox" >
                                    <div id="imgplaceholder">
                                    <img src="https://res.cloudinary.com/dtutqsucw/image/upload/v1438955603/file-upload-01.png" alt="upload-file" />
                                    </br>
                                    <p class="file-type-desc">( <font color="red">*</font>file extension allowed: <strong>.jpg</strong> )</p>
                                    </div>
                                    <br/><br/>
                                    <input type="file" id="inputpic" name="file_logo" class="browse-button" style="width: 280px;position: relative; overflow: hidden; cursor: default;" ></input>
                                    </div>
                                </div>
                                </div>
                              </li>
                            </ul>

                      </div>

                      </div>
            <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">

              <div class="form-group row">
                <input type="hidden" class="form-control" name="idpengguna" id="inputIDPengguna" value="<?= $pengguna['id_pengguna']; ?>" >
                <label for="inputName" class="col-sm-2 col-form-label">Nama Kursus</label>
                <div class="col-sm-10">
                  <input type="text" class="form-control" name="nama_kursus" id="inputName" placeholder="Nama Kursus">
                  <?= form_error('nama_kursus','<small class="text-danger pl-3">','</small>'); ?>
                </div>
              </div>
              <div class="form-group row">
                <label for="inputCabang" class="col-sm-2 col-form-label">Cabang Olahraga</label>
                <div class="col-sm-10">
                  <select name="cabang_olahraga" id="inputCB" class="form-control">
                      <?php foreach ($cabang as $c):
                          if($c['id_cabang'] == $kursus['id_cabang'] ){ ?>
                            <option value="<?= $c['id_cabang'] ?>" selected><?= $c['nama_cabang'] ?></option>
                          <?php }else{ ?>
                            <option value="<?= $c['id_cabang'] ?>" ><?= $c['nama_cabang'] ?></option>
                      <?php }endforeach; ?>
                  </select>
                  <?= form_error('cabang_olahraga','<small class="text-danger pl-3">','</small>'); ?>
                </div>
              </div>
              <div class="form-group row">
                <label for="inputLokasi" class="col-sm-2 col-form-label">Lokasi</label>
                <div class="col-sm-10">
                  <textarea class="form-control" name="lokasi" id="inputLokasi" placeholder="Masukan link gmaps.."></textarea>
                  <?= form_error('lokasi','<small class="text-danger pl-3">','</small>'); ?>
                </div>
              </div>
              <div class="form-group row">
                <label for="inputKontak" class="col-sm-2 col-form-label">No. Hp / Kontak</label>
                <div class="col-sm-10">
                  <input type="tel" class="form-control" name="kontak_kursus" id="inputKontak" placeholder="Kontak.."></input>
                  <?= form_error('kontak_kursus','<small class="text-danger pl-3">','</small>'); ?>
                </div>
              </div>
              <div class="form-group row">
                <label for="inputDeskripsi" class="col-sm-2 col-form-label">Deskripsi</label>
                <div class="col-sm-10">
                  <textarea class="form-control" name="deskripsi" id="inputDeskripsi" placeholder="Deskripsi.."></textarea>
                  <?= form_error('deskripsi','<small class="text-danger pl-3">','</small>'); ?>
                </div>
              </div>
              <button type="submit" id="btnsave" class="btn btn-success btn-block">Simpan</button>
        </div>
        </div>
</div>
</div>
</div>
<div class="container">
    <div class="row align-items-center">
        <div class="col-lg-12">
            <h3>Tambah Gallery</h3>
            <hr>
        </div>
    </div>
</div>
<style>
  .dropzone{
    width: 300px;
    height: 300px;
    padding: 10px;
    display: flex;
    justify-content: center;
    align-items: center;
    border-radius: 10px;
    border: 2px dashed rgb(204, 204, 204);
    color: rgb(204, 204, 204);
    text-align: center;
    -webkit-box-align: center;
    -webkit-box-pack: center;
    z-index: 100;
    cursor: default;
  }
  .dropzone.dragover{
    border-color: #000;
    color: #000;
  }
  .dropzone_thumb{
    width: 100%;
    height: 100%;
    border-radius: 10px;
    background-image: url('<?= base_url() ?>assets/upload/default/img_tumbnail.png');
    background-size: contain;
    background-repeat: no-repeat;
    position: relative;
    overflow: hidden;
  }
  .dropzone_thumb::after{
    content: attr(data-label);
    position: absolute;
    bottom: 0;
    left: 0;
    width: 100%;
    padding: 5px 0;
    color: #ffffff;
    background-color: grey;
    font-size: 12px;
    text-align: center;
  }
</style>
<div class="col-12">
          <div class="container">
                <div class="form-group row" >
                  <div class="col-lg-6" style="justify-content:center;align-items:center;display:flex">
                    <div class="dropzone" >
                    <label for="browse" class="dropzone_msg">Drop files here</label>
                    <input type="file" name="browse" class="dropzone_input" style="display: none;">
                    <!-- <div class="dropzone_thumb" data-label="mytext.txt"></div> -->
                    </div>
                  </div>
                  <div class="col-lg-6">
                    <div class="col-lg-12">
                    <textarea class="form-control" name="keterangan" id="inputKeterangan" placeholder="Keterangan"></textarea>
                    </div>
                    <div class="col-lg-12">
                      <!-- <button id="getdatainfo" class="btn btn-warning">result</i></a> -->
                      <button id="addinforow" style="margin-top:20px" class="btn btn-success"><i class="fa fa-plus"></i></a>
                    </div>
                  </div>
                </div>
                <div class="form-group row" id="info1">
                  <div class="card-body table-responsive p-0">
                    <?= $this->session->flashdata('message'); ?>
                    <?= $this->session->flashdata('error'); ?>
                    <table class="table table-hover text-nowrap">
                      <thead>
                        <tr>
                          <th>Foto</th>
                          <th>Keterangan</th>
                          <th>Action</th>
                        </tr>
                      </thead>
                      <tbody id="listinfo">

                      </tbody>
                    </table>
                  </div>
                </div>
              </form>
            <!-- /.tab-pane -->
          </div>
          <!-- /.tab-content -->
</div>

<script type="text/javascript">
var base = <?php echo json_encode(base_url()); ?>;
const max = <?php echo json_encode($max_kursus); ?>;
var listFile = [];
var listKet = [];
var logoData = '';
var formData = new FormData();
var tempfiles = "";

  $('#inputpic').on('change',function(e){
    console.log(e.target.files[0]);
    var reader = new FileReader();
    logoData = e.target.files[0];
    reader.readAsDataURL(e.target.files[0]);
    reader.onload = function(event) {
      $('#imgbox').attr('style','background-image: url("'+event.target.result+'");background-size:cover')
      $('#imgplaceholder').html('<br/><br/><br/><br/><br/><br/><br/><br/>')
    }
  })

  $('#addinforow').on('click',function(e){
    e.preventDefault();
    var $div = $('tr[id^="listcount"]:last');
    if($div.length > 0 ){
        var num = parseInt( $div.prop("id").match(/\d+/g), 10 ) +1;
    }else{
        var num = 0;
    }

    var url = $('.dropzone_thumb').data('imgurl')
    var imgname = $('.dropzone_thumb').data('label')
    var keterangan = $('#inputKeterangan').val()
    if (num < max ){
      if( keterangan!= "" && url !=null){
        // data for form
        listFile.push(tempfiles)
        listKet.push(keterangan)
        var row = '  <tr id="listcount'+num+'">'+
                  '<td><img src='+url+' width="100px" height="100px"></img></td>'+
                  '<td>'+keterangan+'</td>'+
                  '<td><button id="minusinforow'+num+'" onclick="event.preventDefault(); minusinforow('+num+');" class="btn btn-warning"><i class="fa fa-trash"></i></a></td></tr>'
        $('#listinfo').append(row)
      }else{
        alert('Tolong masukan informasi yang lengkap')
      }
    }else{
      alert('anda sudah mencapai maksimal!')
    }

  })

  function minusinforow(id){
    $('#listcount'+id).remove()
    listFile.splice(id,1);
    listKet.splice(id,1)
  }

  $('#btnsave').on('click',function(e){
    e.preventDefault();
    var idpengguna = $('#inputIDPengguna').val()
    var nama = $('#inputName').val()
    var cabang = $('#inputCB').val()
    var lokasi = $('#inputLokasi').val()
    var deskripsi = $('#inputDeskripsi').val()
    var kontak = $('#inputKontak').val()
    for(var i=0;i<listFile.length;i++){
      formData.append('file[]',listFile[i])
      formData.append('keterangan[]',listKet[i])
    }
    formData.append('createdBy',idpengguna)
    formData.append('logo',logoData)
    formData.append('nama_kursus',nama)
    formData.append('cabang_olahraga',cabang)
    formData.append('lokasi',lokasi)
    formData.append('deskripsi',deskripsi)
    formData.append('kontak_kursus',kontak)

    $.ajax({
      url : '<?= base_url(); ?>kursus/tambah/upload',
      type : 'POST',
      dataType : 'json',
      processData: false,
      contentType: false,
      data : formData,
      success : function(res){
        console.log(res)
        if(res.form_error.error){
           // if there is error
          form_validation(res.form_error.msg)
        }else{
          if(res.form_error.insert){
            alert('Sukses menambahkan kursus baru!');
            window.location = '<?= base_url() ?>dashboard';
          }else{
            alertCall('alert','Something wrong when input data! Please Try again','#msgtxt')
          }
        }
      }
    })
  })

  function setImg(dropzone,files){
    if(files.type.startsWith("image/")){
    var thumbnail = $('.dropzone_thumb').data('label')
    dropzone.attr('class','dropzone')
    if(!thumbnail){
      $('.dropzone_msg').hide()
      var thumb = $('<div/>').addClass('dropzone_thumb');
      dropzone.append(thumb)
    }
    // data for form
    tempfiles = files
    $('.dropzone_thumb').attr('data-label',files.name) // set img name
      var reader = new FileReader();
      reader.readAsDataURL(files);
      reader.onload = function(event) {
            $('.dropzone_thumb').attr('style','background-image: url("'+event.target.result+'")')
            $('.dropzone_thumb').data('imgurl',event.target.result) // hold img url
      };
    }else{
      alert('Please input image only!')
    }
  }

  jQuery(function($){
    $('.dropzone').on('click', function(){
        $('.dropzone_input')[0].click();
    });
  });

    $('.dropzone_input').on('change',function(e){
      $('.dropzone').attr('class','dropzone')
      setImg($('.dropzone'),e.target.files[0])
    })

    $('.dropzone').on('drop',function(e){
      e.preventDefault();
      $(this).attr('class','dropzone')
      setImg($(this),e.originalEvent.dataTransfer.files[0])
    })
    $('.dropzone').on('dragover',function(){
      $(this).attr('class','dropzone dragover')
      return false
    })
    $('.dropzone').on('dragleave',function(){
      $(this).attr('class','dropzone')
      return false
    })

    function form_validation(msg){
      var errors = msg;
                for(var i=0;i<errors.length;i++){
                  if(errors[i]!=""){errors[i] += '<br/>'}
                }
      alertCall('alert',errors.toString().replace(","," "),'#msgtxt')
    }

    function alertCall($type,$msg,$target){
      if($type == 'alert'){
        alt= '<div class="alert alert-danger alert-dismissible">'+
                      '<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>'+
                      '<h5><i class="icon fa fa-ban"></i> Alert!</h5>'+$msg+
                    '</div>'
      }else if($type == 'success'){
        alt= '<div class="alert alert-success alert-dismissible">'+
                      '<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>'+
                      '<h5><i class="icon fa fa-check"></i> Success!</h5>'+$msg+
                    '</div>'
      }
      $($target).html(alt)
    }

</script>
