


<?php if($infokursus!=null){ ?>
  <div class="container" style="margin-top: 80px; margin-bottom: 30px;">
      <div class="row align-items-center">
          <div class="col-lg-8">
              <h3>Selamat Datang!</h3>
              <h1 style="color:black"><?= $pengguna['nama']; ?></h1>
          </div>
      </div>
      <?= $this->session->flashdata('message'); ?>
      <?= $this->session->flashdata('error'); ?>
  </div>
  <?php if($infokursus['status']=="disetujui"){ ?>
    <!--  -->
  <?php }else if ($infokursus['status']=="pending") { ?>
    <div class="container">
    <div class="alert alert-warning" role="alert">
        Hi Terimakasi Sudah mendaftar , Kursus kamu sedang kami review
    </div></div>

  <?php }else if ($infokursus['status']=="ditolak") { ?>
    <div class="container">
    <div class="alert alert-danger" role="alert">
        Hi Terimakasi Sudah mendaftar , Kursus kamu kami <b>tolak</b>
    </div></div>
  <?php } ?>
<div class="container">
    <div class="row align-items-center">
        <div class="col-lg-12">
            <h3>Informasi Kursus</h3>
            <hr>
                <div class="row">
                    <div class="col-md-3">
                        <img src="<?= base_url() ?>assets/upload/logo_kursus/<?= $infokursus['logo'] ?>" alt="" class="img-fluid">
                    </div>
                    <div class="col-md-9 mt-sm-20">
                        <h3><b><?= $infokursus['nama_kursus'] ?></b></h3>
                        <h5><?= $infokursus['nama_cabang'] ?></h5>
                        <hr>
                        <p><?= $infokursus['deskripsi'] ?>.</p>
                        <div class="row" style="margin-left: 4px;margin-bottom:20px;color:#095816;" >
                            <div class="col-md-2" >
                                <i class="fa fa-map-marker fa-3x" aria-hidden="true"></i><br><br>
                            </div>
                            <div class="col-md-9 mt-sm-20">
                                <p><?= $infokursus['lokasi'] ?></p>
                            </div>
                            <div class="col-md-2"  style="color:#095816;">
                                <i class="fa fa-spinner fa-spin fa-3x"  ></i>
                            </div>
                            <div class="col-md-9 mt-sm-20">
                                <p>Status : <b><?= $infokursus['status'] ?></b></p>
                            </div>
                        </div>
                    </div>

                </div>

                <div class="container" style="margin-bottom:30px">
                <?php if ($infokursus['status']=="ditolak") { ?>
                  <a href="<?= base_url() ?>kursus/detail">
                  <button type="button"  class="btn btn-danger" style="float: right;">Upload Ulang!</button>
                  </a>
                <?php }else{ ?>
                  <a href="<?= base_url() ?>kursus/detail">
                  <button type="button" class="btn btn-success" style="float: right;">Edit Kursus</button>
                  </a>
                  <button type="button" class="btn btn-danger" id="btndel" data-toggle="modal" data-target="#modal-danger-delete" style="float: right;">Hapus Kursus</button>
                <?php } ?>
                </div>
        </div>
    </div>
</div>

<?php if($infokursus['status']=="disetujui"){ ?>
  <div class="container" style="margin-top: 100px;">
      <h3>Informasi Kelas</h3>
              <hr>
      <div class="row">
          <div class="MultiCarousel" data-items="1,3,5,6" data-slide="1" id="MultiCarousel"  data-interval="1000">
              <div class="MultiCarousel-inner">
                  <?php if($num_kelas > 0) { ?>
                    <?php $count=0; foreach($daftar_kelas as $dk) : ?>
                      <div class="item">
                          <div class="pad15">
                              <p class="lead">
                                <?php if($thumbnail[$count] != null){ ?>
                                <img src="<?= base_url(); ?>assets/upload/resource_kelas/<?= $thumbnail[$count]['filename'] ?>" width="100px" height="100px">
                                <?php }else{ ?>
                                <img src="<?= base_url(); ?>assets/raga/img/camera.png" width="100px">
                                <?php } ?>
                              </p>
                              <p><h4><?= $dk['nama_kelas'] ?></h4></p>
                              <p><h5>Status : <b><?= $dk['status_kelas'] ?></b></h5></p>
                              <?php if($dk['status_kelas'] != $status[2]) { ?>
                              <p> <a href="<?= base_url(); ?>kelas/manage/kls<?= $dk['id_kelas'] ?>">
                                <button type="button" class="btn btn-success"><i class="fa fa-pencil"></i></button>
                                </a>
                              <?php } ?>
                                  <button type="button" class="btn btn-danger" data-toggle="modal" data-target="#modal-danger-delete-kelas<?= $dk['id_kelas'] ?>"><i class="fa fa-trash"></i></button>
                              </p>
                          </div>
                      </div>
                    <?php $count++; endforeach; ?>
                    <?php if($num_kelas < $max_kelas ) { ?>
                        <div class="item">
                            <div class="pad15">
                                <p class="lead">
                                <img src="<?= base_url(); ?>assets/raga/img/camera.png" width="100px"></p>
                                <p>Tambah kelas?</p>
                                <p> <a href="<?= base_url(); ?>kelas/tambah">
                                  <button type="button" class="btn btn-success">Tambah Kelas</button>
                                  </a></p>
                            </div>
                        </div>
                    <?php }?>
                  <?php }else{ ?>
                  <div class="item">
                      <div class="pad15">
                          <p class="lead"><img src="<?= base_url(); ?>assets/raga/img/camera.png" width="100px"></p>
                          <p>Belum punya kelas?</p>
                          <p> <a href="<?= base_url(); ?>kelas/tambah">
                            <button type="button" class="btn btn-success">Tambah Kelas</button>
                            </a></p>
                      </div>
                  </div>
                <?php } ?>

              </div>
              <button class="btn btn-success leftLst"><</button>
              <button class="btn btn-success rightLst">></button>
          </div>
      </div>
   </div>
<?php } ?>

<!-- Tempat -->



<?php }else{ ?>
  <div class="container" style="margin-top: 80px; margin-bottom: 30px;">
      <div class="row align-items-center">
          <div class="col-lg-8">
              <h3>Selamat Datang!</h3>
              <h1 style="color:black"><?= $pengguna['nama']; ?></h1>
              <hr>
          </div>
          <div class="col-lg-4">
              <a href="<?= base_url() ?>kursus/tambah" class="btn_5" style="margin-left: 130px; padding-right: 50px; padding-left: 50px; font-size: 20px;">Buat Kursus</a>
          </div>
      </div>
  </div>
  <div class="container" style="height:300px">
      <h1>Belum ada Kursus!</h1>
  </div>
<?php } ?>

<div class="container" style="margin-top: 100px;">
    <h3>Tempat Olahraga</h3>
            <hr>
    <div class="row">
        <div class="MultiCarousel" data-items="1,3,5,6" data-slide="1" id="MultiCarousel"  data-interval="1000">
            <div class="MultiCarousel-inner">
                <?php if($num_tempat > 0) { ?>
                  <?php $count=0; foreach($daftar_tempat as $dt) : ?>
                    <div class="item">
                        <div class="pad15">
                            <p class="lead">
                              <?php if($thumbnail_tempat[$count] != null){ ?>
                              <img src="<?= base_url(); ?>assets/upload/resource_tempat/<?= $thumbnail_tempat[$count]['filename'] ?>" width="100px" height="100px">
                              <?php }else{ ?>
                              <img src="<?= base_url(); ?>assets/raga/img/camera.png" width="100px">
                              <?php } ?>
                            </p>
                            <p><h4><?= $dt['nama_tempat'] ?></h4></p>
                            <p><h5>Status : <b><?= $dt['status'] ?></b></h5></p>
                            <?php if($dt['status'] != $status[2]) { ?>
                            <p> <a href="<?= base_url(); ?>tempat/manage/tpt<?= $dt['id_tempat'] ?>">
                              <button type="button" class="btn btn-success"><i class="fa fa-pencil"></i></button>
                              </a>
                            <?php } ?>
                                <button type="button" class="btn btn-danger" data-toggle="modal" data-target="#modal-danger-delete-tempat<?= $dt['id_tempat'] ?>"><i class="fa fa-trash"></i></button>
                            </p>
                        </div>
                    </div>
                  <?php $count++; endforeach; ?>
                  <?php if($num_tempat < $max_tempat ) { ?>
                      <div class="item">
                          <div class="pad15">
                              <p class="lead">
                              <img src="<?= base_url(); ?>assets/raga/img/camera.png" width="100px"></p>
                              <p>Tambah Tempat Olahraga?</p>
                              <p> <a href="<?= base_url(); ?>tempat/tambah">
                                <button type="button" class="btn btn-success">Tambah Tempat OR</button>
                                </a></p>
                          </div>
                      </div>
                  <?php }?>
                <?php }else{ ?>
                <div class="item">
                    <div class="pad15">
                        <p class="lead"><img src="<?= base_url(); ?>assets/raga/img/camera.png" width="100px"></p>
                        <p>Belum punya tempat olahraga?</p>
                        <p> <a href="<?= base_url(); ?>tempat/tambah">
                          <button type="button" class="btn btn-success">Tambah Tempat OR</button>
                          </a></p>
                    </div>
                </div>
              <?php } ?>

            </div>
            <button class="btn btn-success leftLst"><</button>
            <button class="btn btn-success rightLst">></button>
        </div>
    </div>
 </div>
 <!-- modal delete kursus -->
 <div class="modal fade" id="modal-danger-delete" style="display: none;" aria-hidden="true">
         <div class="modal-dialog">
           <div class="modal-content bg-danger">
             <div class="modal-header">
               <h4 class="modal-title" ><font color="white"> Konfirmasi</font></h4>
               <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                 <span aria-hidden="true">×</span>
               </button>
             </div>
             <div class="modal-body">
               <p> <font color="white"> Apakah anda yakin akan menghapus kursus dan semua kelas ?</font></p>
             </div>
             <div class="modal-footer justify-content-between">
               <button type="button" class="btn btn-outline-light" data-dismiss="modal">Tidak</button>
               <a href="<?= base_url() ?>kursus/hapus" class="btn btn-outline-light" id="yesbtn">Ya</a>
             </div>
           </div>
           <!-- /.modal-content -->
         </div>
         <!-- /.modal-dialog -->
 </div>
<!-- modal delete kelas -->
<?php if($num_kelas > 0) { ?>
 <?php foreach($daftar_kelas as $dk) : ?>
   <div class="modal fade" id="modal-danger-delete-kelas<?= $dk['id_kelas'] ?>" style="display: none;" aria-hidden="true">
           <div class="modal-dialog">
             <div class="modal-content bg-danger">
               <div class="modal-header">
                 <h4 class="modal-title" ><font color="white"> Konfirmasi</font></h4>
                 <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                   <span aria-hidden="true">×</span>
                 </button>
               </div>
               <div class="modal-body">
                 <p> <font color="white"> Apakah anda yakin akan menghapus kelas <?= $dk['nama_kelas'] ?> ?</font></p>
               </div>
               <div class="modal-footer justify-content-between">
                 <button type="button" class="btn btn-outline-light" data-dismiss="modal">Tidak</button>
                 <a href="<?= base_url() ?>kelas/hapus/kls<?= $dk['id_kelas'] ?>" class="btn btn-outline-light" id="yesbtn">Ya</a>
               </div>
             </div>
             <!-- /.modal-content -->
           </div>
           <!-- /.modal-dialog -->
   </div>
 <?php endforeach;} ?>
<!-- modal delete tempat -->
<?php if($num_tempat > 0) { ?>
  <?php foreach($daftar_tempat as $dt) : ?>
    <div class="modal fade" id="modal-danger-delete-tempat<?= $dt['id_tempat'] ?>" style="display: none;" aria-hidden="true">
            <div class="modal-dialog">
              <div class="modal-content bg-danger">
                <div class="modal-header">
                  <h4 class="modal-title" ><font color="white"> Konfirmasi</font></h4>
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                  </button>
                </div>
                <div class="modal-body">
                  <p> <font color="white"> Apakah anda yakin akan menghapus kelas <?= $dt['nama_tempat'] ?> ?</font></p>
                </div>
                <div class="modal-footer justify-content-between">
                  <button type="button" class="btn btn-outline-light" data-dismiss="modal">Tidak</button>
                  <a href="<?= base_url() ?>tempat/hapus/tpt<?= $dt['id_tempat'] ?>" class="btn btn-outline-light" id="yesbtn">Ya</a>
                </div>
              </div>
              <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
    </div>
  <?php endforeach;} ?>

  <script>
      $(document).ready(function () {
var itemsMainDiv = ('.MultiCarousel');
var itemsDiv = ('.MultiCarousel-inner');
var itemWidth = "";

$('.leftLst, .rightLst').click(function () {
    var condition = $(this).hasClass("leftLst");
    if (condition)
        click(0, this);
    else
        click(1, this)
});

ResCarouselSize();

$(window).resize(function () {
    ResCarouselSize();
});

//this function define the size of the items
function ResCarouselSize() {
    var incno = 0;
    var dataItems = ("data-items");
    var itemClass = ('.item');
    var id = 0;
    var btnParentSb = '';
    var itemsSplit = '';
    var sampwidth = $(itemsMainDiv).width();
    var bodyWidth = $('body').width();
    $(itemsDiv).each(function () {
        id = id + 1;
        var itemNumbers = $(this).find(itemClass).length;
        btnParentSb = $(this).parent().attr(dataItems);
        itemsSplit = btnParentSb.split(',');
        $(this).parent().attr("id", "MultiCarousel" + id);


        if (bodyWidth >= 1200) {
            incno = itemsSplit[1];
            itemWidth = sampwidth / incno;
        }
        else if (bodyWidth >= 992) {
            incno = itemsSplit[1];
            itemWidth = sampwidth / incno;
        }
        else if (bodyWidth >= 768) {
            incno = itemsSplit[1];
            itemWidth = sampwidth / incno;
        }
        else {
            incno = itemsSplit[0];
            itemWidth = sampwidth / incno;
        }
        $(this).css({ 'transform': 'translateX(0px)', 'width': itemWidth * itemNumbers });
        $(this).find(itemClass).each(function () {
            $(this).outerWidth(itemWidth);
        });

        $(".leftLst").addClass("over");
        $(".rightLst").removeClass("over");

    });
}


//this function used to move the items
function ResCarousel(e, el, s) {
    var leftBtn = ('.leftLst');
    var rightBtn = ('.rightLst');
    var translateXval = '';
    var divStyle = $(el + ' ' + itemsDiv).css('transform');
    var values = divStyle.match(/-?[\d\.]+/g);
    var xds = Math.abs(values[4]);
    if (e == 0) {
        translateXval = parseInt(xds) - parseInt(itemWidth * s);
        $(el + ' ' + rightBtn).removeClass("over");

        if (translateXval <= itemWidth / 2) {
            translateXval = 0;
            $(el + ' ' + leftBtn).addClass("over");
        }
    }
    else if (e == 1) {
        var itemsCondition = $(el).find(itemsDiv).width() - $(el).width();
        translateXval = parseInt(xds) + parseInt(itemWidth * s);
        $(el + ' ' + leftBtn).removeClass("over");

        if (translateXval >= itemsCondition - itemWidth / 2) {
            translateXval = itemsCondition;
            $(el + ' ' + rightBtn).addClass("over");
        }
    }
    $(el + ' ' + itemsDiv).css('transform', 'translateX(' + -translateXval + 'px)');
}

//It is used to get some elements from btn
function click(ell, ee) {
    var Parent = "#" + $(ee).parent().attr("id");
    var slide = $(Parent).attr("data-slide");
    ResCarousel(ell, Parent, slide);
}

});
  </script>
