<div class="container" style="margin-top:50px">
    <div class="row align-items-center">
        <div class="col-lg-12">
            <h2>Buat Kelasmu!</h2>
            <?= $this->session->flashdata('message'); ?>
            <?= $this->session->flashdata('error'); ?>
            <div id="msgtxt"></div>
            <h3>Informasi Kelas</h3>
            <hr>
            <form class="form-horizontal" method="post" enctype="multipart/form-data">
                <div class="row">
              <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6" style="float: center;">
                <div class="form-group row">
                  <input type="hidden" class="form-control" name="id_kelas" id="inputid" >
                  <label for="inputName" class="col-sm-2 col-form-label">Nama Kelas</label>
                  <div class="col-sm-10">
                    <input type="text" class="form-control" name="nama_kelas" id="inputName" placeholder="Nama kelas">
                    <?= form_error('nama_kelas','<small class="text-danger pl-3">','</small>'); ?>
                  </div>
                </div>
                <div class="form-group row">
                  <input type="hidden" class="form-control" name="id_kursus_p" id="inputidkursus" value="<?= $infokursus['id_kursus'] ?>">
                  <label for="inputName" class="col-sm-2 col-form-label">Nama Kursus</label>
                  <div class="col-sm-10">
                    <input type="text" class="form-control" placeholder="Nama kursus" value="<?= $infokursus['nama_kursus'] ?>" readonly>
                  </div>
                </div>
                <div class="form-group row">
                  <label for="inputDeskripsi" class="col-sm-2 col-form-label">Deskripsi</label>
                  <div class="col-sm-10">
                    <textarea class="form-control" name="deskripsi_kelas" id="inputDeskripsi" placeholder="Deskripsi.."></textarea>
                    <?= form_error('deskripsi_kelas','<small class="text-danger pl-3">','</small>'); ?>
                  </div>
                </div>
                <div class="form-group row">
                  <label for="inputBiaya" class="col-sm-2 col-form-label">Biaya</label>
                  <div class="col-sm-10">
                    <input type="text" class="form-control" name="biaya"  id="inputbiaya" placeholder="biaya">
                      <?= form_error('biaya','<small class="text-danger pl-3">','</small>'); ?>
                  </div>
                </div>
                <div class="form-group row">
                  <label for="inputBiaya" class="col-sm-2 col-form-label">Jadwal Latihan</label>
                  <div class="col-sm-10">
                    <input type="text" class="form-control" name="jadwal_latihan" id="inputjadwal" placeholder="Jadwal Latihan">
                      <?= form_error('jadwal_latihan','<small class="text-danger pl-3">','</small>'); ?>
                  </div>
                </div>
                <div class="form-group row">
                  <label for="inputBiaya" class="col-sm-2 col-form-label">Jangka Waktu</label>
                  <div class="col-sm-10">
                    <input type="text" class="form-control" name="jangka_wkt" id="inputjangka" placeholder="Jangka Waktu">
                      <?= form_error('jangka_wkt','<small class="text-danger pl-3">','</small>'); ?>
                  </div>
                </div>
                <div class="form-group row">
                  <label for="inputBiaya" class="col-sm-2 col-form-label">Link Gform</label>
                  <div class="col-sm-10">
                    <input type="text" class="form-control" name="link" id="inputlink" placeholder="Link">
                      <?= form_error('link','<small class="text-danger pl-3">','</small>'); ?>
                  </div>
                </div>
                <div class="form-group row">
                  <label for="inputKontak" class="col-sm-2 col-form-label">Kontak</label>
                  <div class="col-sm-10">
                    <input type="text" class="form-control" name="kontak_kelas" id="inputKontak" placeholder="+6289XXXXXXX">
                      <?= form_error('kontak_kelas','<small class="text-danger pl-3">','</small>'); ?>
                  </div>
                </div>
              </div>
            <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
              <!-- pengajar  -->
              <div class="form-group row">
                <input type="hidden" class="form-control" name="id_pengajar" id="inputidpengajar" >
                <label for="inputCB" class="col-sm-2 col-form-label">Nama Pengajar</label>
                <div class="col-sm-10">
                  <input type="text" class="form-control" name="nama_pengajar" id="inputnamapengajar" placeholder="Nama Pengajar">
                </div>
              </div>
              <div class="form-group row">
                <label for="inputCB" class="col-sm-2 col-form-label">Deskripsi Pengajar</label>
                <div class="col-sm-10">
                  <textarea type="text" class="form-control" name="deskripsi_pengajar" id="inputdeskpengajar" ></textarea>
                </div>
              </div>
              <div class="form-group row">
                <label for="inputCB" class="col-sm-2 col-form-label">Prestasi Pengajar</label>
                <div class="col-sm-10">
                  <textarea type="text" class="form-control" name="prestasi_pengajar" id="inputprespengajar" ></textarea>
                </div>
              </div>
              <button type="submit" id="btnsave" class="btn btn-success btn-block">Simpan</button>
        </div>
        </div>

</div>
</div>
</div>
<div class="container">
    <div class="row align-items-center">
        <div class="col-lg-12">
            <h3>Tambah Gallery</h3>
            <hr>
        </div>
    </div>
</div>
<style>
  .dropzone{
    width: 300px;
    height: 300px;
    padding: 10px;
    display: flex;
    justify-content: center;
    align-items: center;
    border-radius: 10px;
    border: 2px dashed rgb(204, 204, 204);
    color: rgb(204, 204, 204);
    text-align: center;
    -webkit-box-align: center;
    -webkit-box-pack: center;
    z-index: 100;
    cursor: default;
  }
  .dropzone.dragover{
    border-color: #000;
    color: #000;
  }
  .dropzone_thumb{
    width: 100%;
    height: 100%;
    border-radius: 10px;
    background-image: url('<?= base_url() ?>assets/upload/default/img_tumbnail.png');
    background-size: contain;
    background-repeat: no-repeat;
    position: relative;
    overflow: hidden;
  }
  .dropzone_thumb::after{
    content: attr(data-label);
    position: absolute;
    bottom: 0;
    left: 0;
    width: 100%;
    padding: 5px 0;
    color: #ffffff;
    background-color: grey;
    font-size: 12px;
    text-align: center;
  }
</style>
<div class="col-12">
          <div class="container">
                <div class="form-group row" >
                  <div class="col-lg-6" style="justify-content:center;align-items:center;display:flex">
                    <div class="dropzone" >
                    <label for="browse" class="dropzone_msg">Drop files here</label>
                    <input type="file" name="browse" class="dropzone_input" style="display: none;">
                    <!-- <div class="dropzone_thumb" data-label="mytext.txt"></div> -->
                    </div>
                  </div>
                  <div class="col-lg-6">
                    <div class="col-lg-12">
                    <textarea class="form-control" name="keterangan" id="inputKeterangan" placeholder="Keterangan"></textarea>
                    </div>
                    <div class="col-lg-12">
                      <!-- <button id="getdatainfo" class="btn btn-warning">result</i></a> -->
                      <button id="addinforow" style="margin-top:20px" class="btn btn-success"><i class="fa fa-plus"></i></a>
                    </div>
                  </div>
                </div>
                <div class="form-group row" id="info1">
                  <div class="card-body table-responsive p-0">
                    <?= $this->session->flashdata('message'); ?>
                    <?= $this->session->flashdata('error'); ?>
                    <table class="table table-hover text-nowrap">
                      <thead>
                        <tr>
                          <th>Foto</th>
                          <th>Keterangan</th>
                          <th>Action</th>
                        </tr>
                      </thead>
                      <tbody id="listinfo">

                      </tbody>
                    </table>
                  </div>
                </div>
              </form>
            <!-- /.tab-pane -->
          </div>
          <!-- /.tab-content -->
</div>

<script type="text/javascript">
var base = <?php echo json_encode(base_url()); ?>;
const max = <?php echo json_encode($max_res_kelas); ?>;
var listFile = [];
var listKet = [];
var logoData = '';
var formData = new FormData();
var tempfiles = "";

  $('#inputpic').on('change',function(e){
    console.log(e.target.files[0]);
    var reader = new FileReader();
    logoData = e.target.files[0];
    reader.readAsDataURL(e.target.files[0]);
    reader.onload = function(event) {
      $('#imgbox').attr('style','background-image: url("'+event.target.result+'");background-size:cover')
      $('#imgplaceholder').html('<br/><br/><br/><br/><br/><br/><br/><br/>')
    }
  })

  $('#addinforow').on('click',function(e){
    e.preventDefault();
    var $div = $('tr[id^="listcount"]:last');
    if($div.length > 0 ){
        var num = parseInt( $div.prop("id").match(/\d+/g), 10 ) +1;
    }else{
        var num = 0;
    }

    var url = $('.dropzone_thumb').data('imgurl')
    var imgname = $('.dropzone_thumb').data('label')
    var keterangan = $('#inputKeterangan').val()
    if (num < max ){
      if( keterangan!= "" && url !=null){
        // data for form
        listFile.push(tempfiles)
        listKet.push(keterangan)
        var row = '  <tr id="listcount'+num+'">'+
                  '<td><img src='+url+' width="100px" height="100px"></img></td>'+
                  '<td>'+keterangan+'</td>'+
                  '<td><button id="minusinforow'+num+'" onclick="event.preventDefault(); minusinforow('+num+');" class="btn btn-warning"><i class="fa fa-trash"></i></a></td></tr>'
        $('#listinfo').append(row)
      }else{
        alert('Tolong masukan informasi yang lengkap')
      }
    }else{
      alert('anda sudah mencapai maksimal!')
    }

  })

  function minusinforow(id){
    $('#listcount'+id).remove()
    listFile.splice(id,1);
    listKet.splice(id,1)
  }

  $('#btnsave').on('click',function(e){
    e.preventDefault();
    var id_kursus = $('#inputidkursus').val()
    var nama = $('#inputName').val()
    var jangka_wkt = $('#inputjangka').val()
    var jadwal_latihan = $('#inputjadwal').val()
    var biaya = $('#inputbiaya').val()
    var link = $('#inputlink').val()
    var deskripsi = $('#inputDeskripsi').val()
    var nama_pengajar = $('#inputnamapengajar').val()
    var desk_pengajar = $('#inputdeskpengajar').val()
    var pres_pengajar = $('#inputprespengajar').val()
    var kontak_kelas = $('#inputKontak').val()
    for(var i=0;i<listFile.length;i++){
      formData.append('file[]',listFile[i])
      formData.append('keterangan[]',listKet[i])
    }
    formData.append('id_kursus_p',id_kursus)
    formData.append('nama_kelas',nama)
    formData.append('deskripsi_kelas',deskripsi)
    formData.append('nama_pengajar',nama_pengajar)
    formData.append('deskripsi_pengajar',desk_pengajar)
    formData.append('prestasi_pengajar',pres_pengajar)
    formData.append('biaya',biaya)
    formData.append('jadwal_latihan',jadwal_latihan)
    formData.append('jangka_wkt',jangka_wkt)
    formData.append('link',link)
    formData.append('kontak_kelas',kontak_kelas)


    $.ajax({
      url : '<?= base_url(); ?>kelas/tambah/upload',
      type : 'POST',
      dataType : 'json',
      processData: false,
      contentType: false,
      data : formData,
      success : function(res){
        console.log(res)
        if(res.form_error.error){
           // if there is error
          form_validation(res.form_error.msg)
        }else{
          if(res.form_error.insert){
            alert('Sukses menambahkan kelas baru!');
            window.location = '<?= base_url() ?>dashboard';
          }else{
            alertCall('alert','Something wrong when input data! Please Try again','#msgtxt')
          }
        }
      }
    })
  })

  function setImg(dropzone,files){
    if(files.type.startsWith("image/")){
    var thumbnail = $('.dropzone_thumb').data('label')
    dropzone.attr('class','dropzone')
    if(!thumbnail){
      $('.dropzone_msg').hide()
      var thumb = $('<div/>').addClass('dropzone_thumb');
      dropzone.append(thumb)
    }
    // data for form
    tempfiles = files
    $('.dropzone_thumb').attr('data-label',files.name) // set img name
      var reader = new FileReader();
      reader.readAsDataURL(files);
      reader.onload = function(event) {
            $('.dropzone_thumb').attr('style','background-image: url("'+event.target.result+'")')
            $('.dropzone_thumb').data('imgurl',event.target.result) // hold img url
      };
    }else{
      alert('Please input image only!')
    }
  }

  jQuery(function($){
    $('.dropzone').on('click', function(){
        $('.dropzone_input')[0].click();
    });
  });

    $('.dropzone_input').on('change',function(e){
      $('.dropzone').attr('class','dropzone')
      setImg($('.dropzone'),e.target.files[0])
    })

    $('.dropzone').on('drop',function(e){
      e.preventDefault();
      $(this).attr('class','dropzone')
      setImg($(this),e.originalEvent.dataTransfer.files[0])
    })
    $('.dropzone').on('dragover',function(){
      $(this).attr('class','dropzone dragover')
      return false
    })
    $('.dropzone').on('dragleave',function(){
      $(this).attr('class','dropzone')
      return false
    })

    function form_validation(msg){
      var errors = msg;
                for(var i=0;i<errors.length;i++){
                  if(errors[i]!=""){errors[i] += '<br/>'}
                }
      alertCall('alert',errors.toString().replace(","," "),'#msgtxt')
    }

    function alertCall($type,$msg,$target){
      if($type == 'alert'){
        alt= '<div class="alert alert-danger alert-dismissible">'+
                      '<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>'+
                      '<h5><i class="icon fa fa-ban"></i> Alert!</h5>'+$msg+
                    '</div>'
      }else if($type == 'success'){
        alt= '<div class="alert alert-success alert-dismissible">'+
                      '<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>'+
                      '<h5><i class="icon fa fa-check"></i> Success!</h5>'+$msg+
                    '</div>'
      }
      $($target).html(alt)
    }

</script>
