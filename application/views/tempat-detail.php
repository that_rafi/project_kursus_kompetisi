<!-- breadcrumb start-->
<section class="breadcrumb breadcrumb_bg">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="breadcrumb_iner text-center">
                    <div class="breadcrumb_iner_item">
                        <h2>Tempat Kursus Detail</h2>
                        <p>Beranda<span>/</span>Tempat Kursus Detail</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- breadcrumb start-->

<!--================ Start Course Details Area =================-->
<section class="course_details_area section_padding">
    <div class="container">
        <div class="row">
            <div class="col-lg-8 course_details_left">
                <div class="main_image">
                  <?php if($main_resource!= null){ ?>
                    <img class="img-fluid" src="<?= base_url() ?>assets/upload/resource_tempat/<?= $main_resource['filename'] ?>" style="height:auto;width:800px;" alt="">
                  <?php }else{ ?>
                     <img class="img-fluid" src="<?= base_url() ?>assets/upload/resource_tempat/default.png" alt="">
                  <?php } ?>
                </div>
                <div class="content_wrapper">
                    <h4 class="title_top"><?= $tempat['nama_tempat'] ?></h4>
                    <div class="content">
                        <?php echo $tempat['deskripsi']; ?>
                    </div>

                    <h4 class="title">Lokasi</h4>
                    <div class="content">
                        <?php echo $tempat['lokasi']; ?>
                    </div>
                    <div class="section-top-border">
                        <h3>Image Gallery</h3>
                        <div class="row gallery-item">
                          <?php foreach($resource as $r) : ?>
                            <div class="col-md-4">
                                <a href="<?= base_url() ?>assets/upload/resource_tempat/<?= $r['filename'] ?>" class="img-pop-up">
                                    <div class="single-gallery-image" style="background: url(<?= base_url().'assets/upload/resource_tempat/'.$r['filename'] ?>);"></div>
                                </a>
                            </div>
                          <?php endforeach; ?>
                        </div>
                    </div>
                </div>
            </div>




            <div class="col-lg-4 right-contents">
                <div class="sidebar_top">
                    <ul>
                        <li>
                            <a class="justify-content-between d-flex">
                                <p>Diposting Oleh</p>
                                <span class="color"><?= $tempat['nama'] ?></span>
                            </a>
                        </li>
                        <li>
                            <a class="justify-content-between d-flex" >
                                <p>Cabang Olahraga </p>
                                <span><?= $tempat['nama_cabang'] ?></span>
                            </a>
                        </li>
                        <li>
                            <a class="justify-content-between d-flex" >
                                <p>Biaya Sewa</p>
                                <span><?= $tempat['biaya_sewa'] ?></span>
                            </a>
                        </li>
                        <li>
                            <a class="justify-content-between d-flex" >
                                <p>Jam Operasional</p>
                                <span><?= $tempat['jam'] ?></span>
                            </a>
                        </li>
                        <li>
                            <a class="justify-content-between d-flex" >
                                <p>Kontak</p>
                                <span><?= $tempat['kontak'] ?></span>
                            </a>
                        </li>
                    </ul>
                    <a href="https://api.whatsapp.com/send?phone=<?= $tempat['kontak'] ?>" target="_blank" style="text-decoration:none;color:white" class="btn_1 d-block">Whatsapp kami!</a>
                    <!-- <a href="#" class="btn_1 d-block">Enroll the course</a> -->
                </div>

                <!-- <h4 class="title">Reviews</h4> -->
                <!-- <div class="content">
                    <div class="review-top row pt-40">
                        <div class="col-lg-12">
                            <h6 class="mb-15">Provide Your Rating</h6>
                            <div class="d-flex flex-row reviews justify-content-between">
                                <span>Quality</span>
                                <div class="rating">
                                        <a href="#"><img src="<?= base_url() ?>assets/raga/img/icon/color_star.svg" alt=""></a>
                                        <a href="#"><img src="<?= base_url() ?>assets/raga/img/icon/color_star.svg" alt=""></a>
                                        <a href="#"><img src="<?= base_url() ?>assets/raga/img/icon/color_star.svg" alt=""></a>
                                        <a href="#"><img src="<?= base_url() ?>assets/raga/img/icon/color_star.svg" alt=""></a>
                                        <a href="#"><img src="<?= base_url() ?>assets/raga/img/icon/star.svg" alt=""></a>
                                    </div>
                                <span>Outstanding</span>
                            </div>
                            <div class="d-flex flex-row reviews justify-content-between">
                                <span>Puncuality</span>
                                <div class="rating">
                                        <a href="#"><img src="<?= base_url() ?>assets/raga/img/icon/color_star.svg" alt=""></a>
                                        <a href="#"><img src="<?= base_url() ?>assets/raga/img/icon/color_star.svg" alt=""></a>
                                        <a href="#"><img src="<?= base_url() ?>assets/raga/img/icon/color_star.svg" alt=""></a>
                                        <a href="#"><img src="<?= base_url() ?>assets/raga/img/icon/color_star.svg" alt=""></a>
                                        <a href="#"><img src="<?= base_url() ?>assets/raga/img/icon/star.svg" alt=""></a>
                                    </div>
                                <span>Outstanding</span>
                            </div>
                            <div class="d-flex flex-row reviews justify-content-between">
                                <span>Quality</span>
                                <div class="rating">
                                        <a href="#"><img src="<?= base_url() ?>assets/raga/img/icon/color_star.svg" alt=""></a>
                                        <a href="#"><img src="<?= base_url() ?>assets/raga/img/icon/color_star.svg" alt=""></a>
                                        <a href="#"><img src="<?= base_url() ?>assets/raga/img/icon/color_star.svg" alt=""></a>
                                        <a href="#"><img src="<?= base_url() ?>assets/raga/img/icon/color_star.svg" alt=""></a>
                                        <a href="#"><img src="<?= base_url() ?>assets/raga/img/icon/star.svg" alt=""></a>
                                    </div>
                                <span>Outstanding</span>
                            </div>

                        </div>
                    </div>
                    <div class="feedeback">
                        <h6>Your Feedback</h6>
                        <textarea name="feedback" class="form-control" cols="10" rows="10"></textarea>
                        <div class="mt-10 text-right">
                            <a href="#" class="btn_1">Read more</a>
                        </div>
                    </div>
                    <div class="comments-area mb-30">
                        <div class="comment-list">
                            <div class="single-comment single-reviews justify-content-between d-flex">
                                <div class="user justify-content-between d-flex">
                                    <div class="thumb">
                                        <img src="<?= base_url() ?>assets/raga/img/cource/cource_1.png" alt="">
                                    </div>
                                    <div class="desc">
                                        <h5><a href="#">Emilly Blunt</a>
                                        </h5>
                                        <div class="rating">
                                            <a href="#"><img src="<?= base_url() ?>assets/raga/img/icon/color_star.svg" alt=""></a>
                                            <a href="#"><img src="<?= base_url() ?>assets/raga/img/icon/color_star.svg" alt=""></a>
                                            <a href="#"><img src="<?= base_url() ?>assets/raga/img/icon/color_star.svg" alt=""></a>
                                            <a href="#"><img src="<?= base_url() ?>assets/raga/img/icon/color_star.svg" alt=""></a>
                                            <a href="#"><img src="<?= base_url() ?>assets/raga/img/icon/star.svg" alt=""></a>
                                        </div>
                                        <p class="comment">
                                            Blessed made of meat doesn't lights doesn't was dominion and sea earth
                                            form
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="comment-list">
                            <div class="single-comment single-reviews justify-content-between d-flex">
                                <div class="user justify-content-between d-flex">
                                    <div class="thumb">
                                        <img src="<?= base_url() ?>assets/raga/img/cource/cource_2.png" alt="">
                                    </div>
                                    <div class="desc">
                                        <h5><a href="#">Elsie Cunningham</a>
                                        </h5>
                                        <div class="rating">
                                            <a href="#"><img src="<?= base_url() ?>assets/raga/img/icon/color_star.svg" alt=""></a>
                                            <a href="#"><img src="<?= base_url() ?>assets/raga/img/icon/color_star.svg" alt=""></a>
                                            <a href="#"><img src="<?= base_url() ?>assets/raga/img/icon/color_star.svg" alt=""></a>
                                            <a href="#"><img src="<?= base_url() ?>assets/raga/img/icon/color_star.svg" alt=""></a>
                                            <a href="#"><img src="<?= base_url() ?>assets/raga/img/icon/star.svg" alt=""></a>
                                        </div>
                                        <p class="comment">
                                            Blessed made of meat doesn't lights doesn't was dominion and sea earth
                                            form
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="comment-list">
                            <div class="single-comment single-reviews justify-content-between d-flex">
                                <div class="user justify-content-between d-flex">
                                    <div class="thumb">
                                        <img src="<?= base_url() ?>assets/raga/img/cource/cource_3.png" alt="">
                                    </div>
                                    <div class="desc">
                                        <h5><a href="#">Maria Luna</a>
                                        </h5>
                                        <div class="rating">
                                            <a href="#"><img src="<?= base_url() ?>assets/raga/img/icon/color_star.svg" alt=""></a>
                                            <a href="#"><img src="<?= base_url() ?>assets/raga/img/icon/color_star.svg" alt=""></a>
                                            <a href="#"><img src="<?= base_url() ?>assets/raga/img/icon/color_star.svg" alt=""></a>
                                            <a href="#"><img src="<?= base_url() ?>assets/raga/img/icon/color_star.svg" alt=""></a>
                                            <a href="#"><img src="<?= base_url() ?>assets/raga/img/icon/star.svg" alt=""></a>
                                        </div>
                                        <p class="comment">
                                            Blessed made of meat doesn't lights doesn't was dominion and sea earth
                                            form
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div> -->
            </div>
        </div>
    </div>
</section>
<!--================ End Course Details Area =================-->
