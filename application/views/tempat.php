<section class="breadcrumb breadcrumb_bg">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="breadcrumb_iner text-center">
                    <div class="breadcrumb_iner_item">
                        <h2>Tempat</h2>
                        <p>Beranda<span>/</span>Tempat</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- breadcrumb start-->

<section class="cari" style="padding-top:50px; padding-bottom:30px;">
    <div class="container">
      <form action="<?= base_url() ?>tempat" method="get">
        <div class="row">
            <div class="col-lg-12">
                <div class="input-group-append" style="height:50px; color:#095816;">
                  <input class="form-control my-0 py-1 amber-border" type="text" name="keyword" placeholder="Search" aria-label="Search" style="height:50px;">
                  <span class="glyphicon form-control-feedback"></span>
                  <button type="submit"><i class="fa fa-search"
                    aria-hidden="true"></i></button>
                </div>
                <a data-toggle="collapse" style="text-decoration:none;color:#095816;" href="#collapseExample" role="button" aria-expanded="false" aria-controls="collapseExample">
                  Add Filter
                </a>
                <?php if($sort != null || $tags != null) { ?>
                <div class="collapse show" id="collapseExample">
                <?php }else{ ?>
                  <div class="collapse" id="collapseExample">
                <?php } ?>
                  <div class="card card-body">
                    <div class="container" style="margin-top:10px">
                      <span><b>Order By :</b></span><br/>
                      <?php if($sort=="ASC"){ ?>
                        <input type="radio" name="sort" value="ASC" checked> A-Z </input>
                        <input type="radio" name="sort" value="DESC"> Z-A </input>
                      <?php }else if($sort=="DESC"){ ?>
                        <input type="radio" name="sort" value="ASC" > A-Z </input>
                        <input type="radio" name="sort" value="DESC" checked> Z-A </input>
                      <?php }else{ ?>
                        <input type="radio" name="sort" value="ASC" > A-Z </input>
                        <input type="radio" name="sort" value="DESC"> Z-A </input>
                      <?php } ?>
                    </div>
                    <div class="container">
                      <span><b>Tags :</b></span><br/>
                      <?php foreach ($cabang as $c): ?>
                        <?php if($tags == $c['nama_cabang']){ ?>
                        <input type="radio" name="tags" value="<?= $c['nama_cabang'] ?>" checked> <?= $c['nama_cabang'] ?> </input>
                      <?php }else{ ?>
                        <input type="radio" name="tags" value="<?= $c['nama_cabang'] ?>"> <?= $c['nama_cabang'] ?> </input>
                      <?php } ?>
                      <?php endforeach; ?>
                    </div>
                  </div>
                </div>
            </div>



        </div>
      </form>
    </div>
</section>


<?php if($tempat != null) { ?>
<?php $count=0; foreach($tempat as $k) : ?>

<div class="container" style="overflow:hidden">
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-body">
                        <div class="row">
                            <div class="col-md-3">
                              <?php if($thumbnail[$count]!= null){ ?>
                                <img src="<?= base_url() ?>assets/upload/resource_tempat/<?= $thumbnail[$count]['filename'] ?>" alt="" class="img-fluid" style="position:center;height:200px">
                              <?php }else{ ?>
                                <img src="<?= base_url() ?>assets/upload/resource_tempat/default.png" alt="" class="img-fluid" style="position:center;">
                              <?php } ?>
                            </div>
                            <div class="col-md-9 mt-sm-20" style="margin-bottom:20px">
                                <h3><?= $k['nama_tempat'];  ?></h3>
                                <p>by <?= $k['nama'];  ?></p>
                                <p><?= substr($k['deskripsi'], 0, 28) . '...'; ?></p>
                                    <a href="<?= base_url(); ?>tempat/detail/<?= $k['id_tempat']; ?>" class="btn_3" style="align-content: left; position: left;">Lihat Detail Tempat </a>
                                    &emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;
                                    <a href="#" class="btn_2" style="align-content: right; position: right;"><?= $k['nama_cabang'] ?> </a>
                            </div>
                        </div>

                </div>
              </div>
        </div>
    </div>
</div>

<?php $count++; endforeach; }else{ ?>

<div class="container" style="height:500px; padding_top:10px;">
  <h1>Tidak ada tempat yang tersedia</h1>
</div>

<?php } ?>


<?php if($tempat!=null) {?>
<div class="container"><?php echo $this->pagination->create_links(); ?></div>
<?php } ?>
