


    <!-- Header part end-->
    <!-- banner part start-->

    <style>
  /* Make the image fully responsive */

  .carousel-inner img {
    width: 100%;
    height: 100%;
  }
  </style>

  <section class="cari" style="margin-top:20px;padding-top:50px; padding-bottom:30px;">
        <div class="container">
          <form action="<?= base_url() ?>search" method="get">
            <div class="row">
                <div class="col-lg-12">
                    <div class="input-group-append" style="height:50px; color:#095816;">
                      <input class="form-control my-0 py-1 amber-border" type="text" name="keyword" placeholder="Search" aria-label="Search" style="height:50px;">
                      <span class="glyphicon form-control-feedback"></span>
                      <button type="submit"><i class="fa fa-search"
                        aria-hidden="true"></i></button>
                    </div>
                    <a data-toggle="collapse" style="text-decoration:none;color:#095816;" href="#collapseExample" role="button" aria-expanded="false" aria-controls="collapseExample">
                      Add Filter
                    </a>
                    <div class="collapse" id="collapseExample">
                      <div class="card card-body">
                        <div class="container" style="margin-top:10px">
                          <span><b>Order By :</b></span><br/>
                          <input type="radio" name="sort" value="ASC"> A-Z </input>
                          <input type="radio" name="sort" value="DESC"> Z-A </input>
                        </div>
                        <div class="container" style="margin-top:10px">
                          <span><b>Tags :</b></span><br/>
                          <?php foreach ($cabang as $c): ?>
                            <input type="radio" name="tags" value="<?= $c['nama_cabang'] ?>"> <?= $c['nama_cabang'] ?> </input>
                          <?php endforeach; ?>
                        </div>
                      </div>
                    </div>

                </div>
            </div>
              </form>
        </div>
    </section>


  <div id="demo" class="carousel slide" data-ride="carousel">
<ul class="carousel-indicators">
  <li data-target="#demo" data-slide-to="0" class="active"></li>
  <li data-target="#demo" data-slide-to="1"></li>
  <li data-target="#demo" data-slide-to="2"></li>
</ul>
<div class="carousel-inner">
  <?php if($artikel == null) {  ?>
  <div class="carousel-item active">
    <img src="<?= base_url()  ?>assets/raga/img/breadcrumb.png" alt="Los Angeles" width="1100" height="500">
    <div class="carousel-caption">
      <h3 style="color:white;">Los Angeles</h3>
      <p style="color:white;">We had such a great time in LA!</p>
    </div>
  </div>
<?php }else{ $count=0; foreach($artikel as $a): ?>
  <div class="carousel-item <?php if ($count==0){echo "active";}?>">
    <?php if($thumbnail_artikel[$count] != null){ ?>
    <a href="<?= base_url() ?>artikel/detail/<?= $a['id_artikel']; ?>">
    <img src="<?= base_url(); ?>assets/upload/training_mandiri/<?= $thumbnail_artikel[$count]['filename'] ?>" width="1100" height="500"></a>
    <?php }else{ ?>
    <img src="<?= base_url()  ?>assets/raga/img/breadcrumb.png" alt="Los Angeles" width="1100" height="500">
    <?php } ?>

    <div class="carousel-caption">
      <h3 style="color:white;"><?= $a['judul'] ?></h3>
      <p style="color:white;"><?= substr($a['isi'], 0, 28) . '...'; ?></p>
    </div>
  </div>
<?php $count++; endforeach; } ?>
</div>
<a class="carousel-control-prev" href="#demo" data-slide="prev">
  <span class="carousel-control-prev-icon"></span>
</a>
<a class="carousel-control-next" href="#demo" data-slide="next">
  <span class="carousel-control-next-icon"></span>
</a>
</div>
    <!-- banner part start-->
    <!-- Modal -->
<div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <!-- <h5 class="modal-title" id="exampleModalLongTitle"></h5> -->
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        Silahkan login terlebih dahulu!
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary" data-dismiss="modal">Ok</button>
      </div>
    </div>
  </div>
</div>

<div class="container" style="padding-top:100px">
        <div class="row justify-content-center">
            <div class="col-xl-5">
                <div class="section_tittle text-center">
                    <h2>Temukan Kelas</h2>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="MultiCarousel" data-items="1,3,5,6" data-slide="1" id="MultiCarousel"  data-interval="1000">
                <div class="MultiCarousel-inner">
                  <?php if($num_kelas > 0) { ?>
                    <?php $count=0; foreach($kelas as $dk) : ?>
                      <div class="item">
                          <div class="pad15">
                              <p class="lead">
                                <?php if($thumbnail[$count] != null){ ?>
                                <img src="<?= base_url(); ?>assets/upload/resource_kelas/<?= $thumbnail[$count]['filename'] ?>" width="100px" height="100px">
                                <?php }else{ ?>
                                <img src="<?= base_url(); ?>assets/raga/img/camera.png" width="100px">
                                <?php } ?>
                              </p>
                              <p><h4><?= $dk['nama_kelas'] ?></h4></p>
                              <p> <a href="<?= base_url(); ?>kelas/detail/<?= $dk['id_kelas'] ?>">
                                <button type="button" class="btn btn-success">Baca Selengkapnya</button>
                              </a></p>
                          </div>
                      </div>
                    <?php $count++; endforeach; ?>
                  <?php }else{ ?>
                  <div class="item">
                      <div class="pad15">
                          <p class="lead"><img src="<?= base_url(); ?>assets/raga/img/camera.png" width="100px"></p>
                          <p>Tidak ada kelas</p>
                      </div>
                  </div>
                <?php } ?>
                </div>
                <button class="btn btn-success leftLst"><</button>
                <button class="btn btn-success rightLst">></button>
            </div>
        </div>
     </div>

     <div class="container" style="padding-top:110px">
        <div class="row justify-content-center">
            <div class="col-xl-5">
                <div class="section_tittle text-center">
                    <h2>Temukan Tempat Olahraga</h2>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="MultiCarousel" data-items="1,3,5,6" data-slide="1" id="MultiCarousel"  data-interval="1000">
                <div class="MultiCarousel-inner">
                  <?php if($num_tempat > 0) { ?>
                    <?php $count=0; foreach($tempat as $t) : ?>
                      <div class="item">
                          <div class="pad15">
                              <p class="lead">
                                <?php if($thumbnail_tempat[$count] != null){ ?>
                                <img src="<?= base_url(); ?>assets/upload/resource_tempat/<?= $thumbnail_tempat[$count]['filename'] ?>" width="100px" height="100px">
                                <?php }else{ ?>
                                <img src="<?= base_url(); ?>assets/raga/img/camera.png" width="100px">
                                <?php } ?>
                              </p>
                              <p><h4><?= $t['nama_tempat'] ?></h4></p>
                              <p> <a href="<?= base_url(); ?>tempat/detail/<?= $t['id_tempat'] ?>">
                                <button type="button" class="btn btn-success">Baca Selengkapnya</button>
                              </a></p>
                          </div>
                      </div>
                    <?php $count++; endforeach; ?>
                  <?php }else{ ?>
                  <div class="item">
                      <div class="pad15">
                          <p class="lead"><img src="<?= base_url(); ?>assets/raga/img/camera.png" width="100px"></p>
                          <p>Tidak ada Tempat</p>
                      </div>
                  </div>
                <?php } ?>
                </div>
                <button class="btn btn-success leftLst"><</button>
                <button class="btn btn-success rightLst">></button>
            </div>
        </div>
     </div>

    <!-- learning part start-->
    <section class="learning_part">
        <div class="container">
            <div class="row align-items-sm-center align-items-lg-stretch">
                <div class="col-md-7 col-lg-7">
                    <div class="learning_img">
                        <img src="<?= base_url() ?>assets/raga/img/raga.png" alt="">
                    </div>
                </div>
                <div class="col-md-5 col-lg-5">
                    <div class="learning_member_text">
                        <h5>Tentang Kami</h5>
                        <h2>To Inform, To Influence, To Inspire</h2>
                        <p>Olahraga adalah senjata rahasia melawan kemiskinan, dengan olahraga banyak orang mendapat penghidupan yang lebih baik.
                            Kami percaya dengan kamudahan berolahraga akan menciptakan
                            kabaikan bagi banyak orang.
                            Raga hadir untuk mempermudah segala urusan yang berkaitan dengan olahraga.
                            Bekerja sama dengan mitra-mitra terpercaya, raga berusaha memberikan solusi atas persoalan yang ada.
                        </p>
                        <p>
                            Tujuan kami sederhana yaitu “kemudahan dalam berolahraga”,
                            kami menyajikan informasi tempat-tempat kursus olahraga dan kami mendampingi
                            dalam pengelolaannya dengan merumuskan standar pengajaran sesuai
                            dengan masing-masing cabang olahraga.
                        </p>
                        <a href="<?= base_url() ?>about" class="btn_1">Read More</a>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- learning part end-->





     <?php if($this->session->flashdata('message')!=null){ ?>
            <script type="text/javascript">
              $('#exampleModalCenter').modal('show')
            </script>
          <?php } ?>

      <script>
          $(document).ready(function () {
    var itemsMainDiv = ('.MultiCarousel');
    var itemsDiv = ('.MultiCarousel-inner');
    var itemWidth = "";

    $('.leftLst, .rightLst').click(function () {
        var condition = $(this).hasClass("leftLst");
        if (condition)
            click(0, this);
        else
            click(1, this)
    });

    ResCarouselSize();

    $(window).resize(function () {
        ResCarouselSize();
    });

    //this function define the size of the items
    function ResCarouselSize() {
        var incno = 0;
        var dataItems = ("data-items");
        var itemClass = ('.item');
        var id = 0;
        var btnParentSb = '';
        var itemsSplit = '';
        var sampwidth = $(itemsMainDiv).width();
        var bodyWidth = $('body').width();
        $(itemsDiv).each(function () {
            id = id + 1;
            var itemNumbers = $(this).find(itemClass).length;
            btnParentSb = $(this).parent().attr(dataItems);
            itemsSplit = btnParentSb.split(',');
            $(this).parent().attr("id", "MultiCarousel" + id);


            if (bodyWidth >= 1200) {
                incno = itemsSplit[1];
                itemWidth = sampwidth / incno;
            }
            else if (bodyWidth >= 992) {
                incno = itemsSplit[1];
                itemWidth = sampwidth / incno;
            }
            else if (bodyWidth >= 768) {
                incno = itemsSplit[1];
                itemWidth = sampwidth / incno;
            }
            else {
                incno = itemsSplit[0];
                itemWidth = sampwidth / incno;
            }
            $(this).css({ 'transform': 'translateX(0px)', 'width': itemWidth * itemNumbers });
            $(this).find(itemClass).each(function () {
                $(this).outerWidth(itemWidth);
            });

            $(".leftLst").addClass("over");
            $(".rightLst").removeClass("over");

        });
    }


    //this function used to move the items
    function ResCarousel(e, el, s) {
        var leftBtn = ('.leftLst');
        var rightBtn = ('.rightLst');
        var translateXval = '';
        var divStyle = $(el + ' ' + itemsDiv).css('transform');
        var values = divStyle.match(/-?[\d\.]+/g);
        var xds = Math.abs(values[4]);
        if (e == 0) {
            translateXval = parseInt(xds) - parseInt(itemWidth * s);
            $(el + ' ' + rightBtn).removeClass("over");

            if (translateXval <= itemWidth / 2) {
                translateXval = 0;
                $(el + ' ' + leftBtn).addClass("over");
            }
        }
        else if (e == 1) {
            var itemsCondition = $(el).find(itemsDiv).width() - $(el).width();
            translateXval = parseInt(xds) + parseInt(itemWidth * s);
            $(el + ' ' + leftBtn).removeClass("over");

            if (translateXval >= itemsCondition - itemWidth / 2) {
                translateXval = itemsCondition;
                $(el + ' ' + rightBtn).addClass("over");
            }
        }
        $(el + ' ' + itemsDiv).css('transform', 'translateX(' + -translateXval + 'px)');
    }

    //It is used to get some elements from btn
    function click(ell, ee) {
        var Parent = "#" + $(ee).parent().attr("id");
        var slide = $(Parent).attr("data-slide");
        ResCarousel(ell, Parent, slide);
    }

});
      </script>



      <!-- <section class="learning_part">
        <div class="container">
            <div class="row align-items-sm-center align-items-lg-stretch">
                <div class="col-md-7 col-lg-7">
                    <div class="learning_img">
                        <img src="<?= base_url() ?>assets/raga/img/class.png" alt="">
                    </div>
                </div>
                <div class="col-md-5 col-lg-5">
                    <div class="learning_member_text">
                        <h2>Buat Kelasmu Sendiri!</h2>
                        <p>Mengajar adalah salah satu cara terbaik untuk belajar sesuatu.
                            Yuk bagikan ilmu mu!
                            Buat kelasmu dan mulai memberikan manfaat dengan orang
                            banyak!
                            </p>

                        <a href="<?= base_url() ?>kelas/tambah" class="btn_1">Buat Kelas</a>
                    </div>
                </div>
            </div>
        </div>
    </section> -->

    <!-- feature_part start-->
    <section class="feature_part">
        <div class="container">
            <div class="row">
                <div class="col-sm-6 col-xl-3 align-self-center">
                    <div class="single_feature_text ">
                        <h2>Mengapa kami?</h2>
                        <p>Olahraga adalah senjata rahasia melawan kemiskinan, dengan
                            olahraga banyak orang mendapat penghidupan yang lebih baik.
                             </p>
                        <a href="<?= base_url() ?>about" class="btn_1">Read More</a>
                    </div>
                </div>
                <div class="col-sm-6 col-xl-3">
                    <div class="single_feature">
                        <div class="single_feature_part">
                            <img src="<?= base_url() ?>assets/raga/img/kemudahan.png" width="100px">
                            <h4>Kemudahan</h4>
                            <p> Raga memberi kemudahan  mulai dari mendaftar kursus olahraga
                                hingga menyewa tempat untuk berolahraga bisa melalui Raga.</p>
                        </div>
                    </div>
                </div>
                <div class="col-sm-6 col-xl-3">
                    <div class="single_feature">
                        <div class="single_feature_part">
                            <img src="<?= base_url() ?>assets/raga/img/berkompeten.png" width="100px">
                            <h4>Berkompeten</h4>
                            <p>Raga bekerja sama dengan mitra-mitra olahraga
                                yang berkompeten dan teruji dalam bidang olahraga.</p>
                        </div>
                    </div>
                </div>
                <div class="col-sm-6 col-xl-3">
                    <div class="single_feature">
                        <div class="single_feature_part single_feature_part_2">
                            <img src="<?= base_url() ?>assets/raga/img/terpercaya.png" width="100px">
                            <h4>Terpercaya</h4>
                            <p>Raga menyajikan informasi dengan tepat dan akurat.
                                Informasi yang Raga sajikanbisa dipertanggungjawabkan.</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- upcoming_event part start-->


    <br><br><br><br><br>
    <!-- <div class="container">
        <div class="row justify-content-center">
            <div class="col-xl-5">
                <div class="section_tittle text-center">
                    <h2>Artikel</h2>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="MultiCarousel" data-items="1,3,5,6" data-slide="1" id="MultiCarousel"  data-interval="1000">
                <div class="MultiCarousel-inner">
                    <?php if($num_artikel > 0) { ?>
                  <?php $count=0; foreach($artikel as $a) : ?>
                    <div class="item">
                        <div class="pad15">
                            <p class="lead">
                              <?php if($thumbnail_artikel[$count] != null){ ?>
                              <img src="<?= base_url(); ?>assets/upload/training_mandiri/<?= $thumbnail_artikel[$count]['filename'] ?>" width="100px" height="100px">
                              <?php }else{ ?>
                              <img src="<?= base_url(); ?>assets/raga/img/camera.png" width="100px">
                              <?php } ?>
                            </p>
                            <p><h4><?= $a['judul'] ?></h4></p>
                            <p> <a href="<?= base_url(); ?>artikel/detail/<?= $a['id_artikel'] ?>">
                              <button type="button" class="btn btn-success">Baca Selengkapnya</button>
                            </a></p>
                        </div>
                    </div>
                  <?php $count++; endforeach; }else{ ?>
                    <div class="item">
                        <div class="pad15">
                            <p class="lead"><img src="<?= base_url(); ?>assets/raga/img/camera.png" width="100px"></p>
                            <p>Tidak ada artikel</p>
                        </div>
                    </div>
                  <?php } ?>
                </div>
                <button class="btn btn-success leftLst"><</button>
                <button class="btn btn-success rightLst">></button>
            </div>
        </div>
     </div> -->



    <!-- member_counter counter start -->
    <!-- <section class="member_counter">
        <div class="container">
            <div class="row">
                <div class="col-lg-3 col-sm-6">
                    <div class="single_member_counter">
                        <span class="counter">1024</span>
                        <h4>All Teachers</h4>
                    </div>
                </div>
                <div class="col-lg-3 col-sm-6">
                    <div class="single_member_counter">
                        <span class="counter">960</span>
                        <h4> All Students</h4>
                    </div>
                </div>
                <div class="col-lg-3 col-sm-6">
                    <div class="single_member_counter">
                        <span class="counter">1020</span>
                        <h4>Online Students</h4>
                    </div>
                </div>
                <div class="col-lg-3 col-sm-6">
                    <div class="single_member_counter">
                        <span class="counter">820</span>
                        <h4>Ofline Students</h4>
                    </div>
                </div>
            </div>
        </div>
    </section> -->
    <!-- member_counter counter end -->

    <!--::review_part start::-->
    <!-- <section class="special_cource padding_top">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-xl-5">
                    <div class="section_tittle text-center">
                        <p>popular courses</p>
                        <h2>Special Courses</h2>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-6 col-lg-4">
                    <div class="single_special_cource">
                        <img src="<?= base_url() ?>assets/raga/img/special_cource_1.png" class="special_img" alt="">
                        <div class="special_cource_text">
                            <a href="course-details.html" class="btn_4">Web Development</a>
                            <h4>$130.00</h4>
                            <a href="course-details.html"><h3>Web Development</h3></a>
                            <p>Which whose darkness saying were life unto fish wherein all fish of together called</p>
                            <div class="author_info">
                                <div class="author_img">
                                    <img src="<?= base_url() ?>assets/raga/img/author/author_1.png" alt="">
                                    <div class="author_info_text">
                                        <p>Conduct by:</p>
                                        <h5><a href="#">James Well</a></h5>
                                    </div>
                                </div>
                                <div class="author_rating">
                                    <div class="rating">
                                        <a href="#"><img src="<?= base_url() ?>assets/raga/img/icon/color_star.svg" alt=""></a>
                                        <a href="#"><img src="<?= base_url() ?>assets/raga/img/icon/color_star.svg" alt=""></a>
                                        <a href="#"><img src="<?= base_url() ?>assets/raga/img/icon/color_star.svg" alt=""></a>
                                        <a href="#"><img src="<?= base_url() ?>assets/raga/img/icon/color_star.svg" alt=""></a>
                                        <a href="#"><img src="<?= base_url() ?>assets/raga/img/icon/star.svg" alt=""></a>
                                    </div>
                                    <p>3.8 Ratings</p>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
                <div class="col-sm-6 col-lg-4">
                    <div class="single_special_cource">
                        <img src="<?= base_url() ?>assets/raga/img/special_cource_2.png" class="special_img" alt="">
                        <div class="special_cource_text">
                            <a href="course-details.html" class="btn_4">design</a>
                            <h4>$160.00</h4>
                            <a href="course-details.html"> <h3>Web UX/UI Design </h3></a>
                            <p>Which whose darkness saying were life unto fish wherein all fish of together called</p>
                            <div class="author_info">
                                <div class="author_img">
                                    <img src="<?= base_url() ?>assets/raga/img/author/author_2.png" alt="">
                                    <div class="author_info_text">
                                        <p>Conduct by:</p>
                                        <h5><a href="#">James Well</a></h5>
                                    </div>
                                </div>
                                <div class="author_rating">
                                    <div class="rating">
                                        <a href="#"><img src="<?= base_url() ?>assets/raga/img/icon/color_star.svg" alt=""></a>
                                        <a href="#"><img src="<?= base_url() ?>assets/raga/img/icon/color_star.svg" alt=""></a>
                                        <a href="#"><img src="<?= base_url() ?>assets/raga/img/icon/color_star.svg" alt=""></a>
                                        <a href="#"><img src="<?= base_url() ?>assets/raga/img/icon/color_star.svg" alt=""></a>
                                        <a href="#"><img src="<?= base_url() ?>assets/raga/img/icon/star.svg" alt=""></a>
                                    </div>
                                    <p>3.8 Ratings</p>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
                <div class="col-sm-6 col-lg-4">
                    <div class="single_special_cource">
                        <img src="<?= base_url() ?>assets/raga/img/special_cource_3.png" class="special_img" alt="">
                        <div class="special_cource_text">
                            <a href="course-details.html" class="btn_4">Wordpress</a>
                            <h4>$140.00</h4>
                            <a href="course-details.html">  <h3>Wordpress Development</h3> </a>
                            <p>Which whose darkness saying were life unto fish wherein all fish of together called</p>
                            <div class="author_info">
                                <div class="author_img">
                                    <img src="<?= base_url() ?>assets/raga/img/author/author_3.png" alt="">
                                    <div class="author_info_text">
                                        <p>Conduct by:</p>
                                        <h5><a href="#">James Well</a></h5>
                                    </div>
                                </div>
                                <div class="author_rating">
                                    <div class="rating">
                                        <a href="#"><img src="<?= base_url() ?>assets/raga/img/icon/color_star.svg" alt=""></a>
                                        <a href="#"><img src="<?= base_url() ?>assets/raga/img/icon/color_star.svg" alt=""></a>
                                        <a href="#"><img src="<?= base_url() ?>assets/raga/img/icon/color_star.svg" alt=""></a>
                                        <a href="#"><img src="<?= base_url() ?>assets/raga/img/icon/color_star.svg" alt=""></a>
                                        <a href="#"><img src="<?= base_url() ?>assets/raga/img/icon/star.svg" alt=""></a>
                                    </div>
                                    <p>3.8 Ratings</p>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </section>
    ::blog_part end::-->

    <!-- learning part start-->
    <!-- <section class="advance_feature learning_part">
        <div class="container">
            <div class="row align-items-sm-center align-items-xl-stretch">
                <div class="col-md-6 col-lg-6">
                    <div class="learning_member_text">
                        <h5>Advance feature</h5>
                        <h2>Our Advance Educator
                            Learning System</h2>
                        <p>Fifth saying upon divide divide rule for deep their female all hath brind mid Days
                            and beast greater grass signs abundantly have greater also use over face earth
                            days years under brought moveth she star</p>
                        <div class="row">
                            <div class="col-sm-6 col-md-12 col-lg-6">
                                <div class="learning_member_text_iner">
                                    <span class="ti-pencil-alt"></span>
                                    <h4>Learn Anywhere</h4>
                                    <p>There earth face earth behold she star so made void two given and also our</p>
                                </div>
                            </div>
                            <div class="col-sm-6 col-md-12 col-lg-6">
                                <div class="learning_member_text_iner">
                                    <span class="ti-stamp"></span>
                                    <h4>Expert Teacher</h4>
                                    <p>There earth face earth behold she star so made void two given and also our</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6 col-md-6">
                    <div class="learning_img">
                        <img src="<?= base_url() ?>assets/raga/img/advance_feature_img.png" alt="">
                    </div>
                </div>
            </div>
        </div>
    </section>
    learning part end-->

    <!--::review_part start::-->
    <!-- <section class="testimonial_part">
        <div class="container-fluid">
            <div class="row justify-content-center">
                <div class="col-xl-5">
                    <div class="section_tittle text-center">
                        <p>tesimonials</p>
                        <h2>Happy Students</h2>
                    </div>
                </div>
            </div> -->
            <!-- <div class="row">
                <div class="col-lg-12">
                    <div class="textimonial_iner owl-carousel">
                        <div class="testimonial_slider">
                            <div class="row">
                                <div class="col-lg-8 col-xl-4 col-sm-8 align-self-center">
                                    <div class="testimonial_slider_text">
                                        <p>Behold place was a multiply creeping creature his domin to thiren open void
                                            hath herb divided divide creepeth living shall i call beginning
                                            third sea itself set</p>
                                        <h4>Michel Hashale</h4>
                                        <h5> Sr. Web designer</h5>
                                    </div>
                                </div>
                                <div class="col-lg-4 col-xl-2 col-sm-4">
                                    <div class="testimonial_slider_img">
                                        <img src="<?= base_url() ?>assets/raga/img/testimonial_img_1.png" alt="#">
                                    </div>
                                </div>
                                <div class="col-xl-4 d-none d-xl-block">
                                    <div class="testimonial_slider_text">
                                        <p>Behold place was a multiply creeping creature his domin to thiren open void
                                            hath herb divided divide creepeth living shall i call beginning
                                            third sea itself set</p>
                                        <h4>Michel Hashale</h4>
                                        <h5> Sr. Web designer</h5>
                                    </div>
                                </div>
                                <div class="col-xl-2 d-none d-xl-block">
                                    <div class="testimonial_slider_img">
                                        <img src="<?= base_url() ?>assets/raga/img/testimonial_img_1.png" alt="#">
                                    </div>
                                </div> -->
                            <!-- </div>
                        </div>
                        <div class="testimonial_slider">
                            <div class="row">
                                <div class="col-lg-8 col-xl-4 col-sm-8 align-self-center">
                                    <div class="testimonial_slider_text">
                                        <p>Behold place was a multiply creeping creature his domin to thiren open void
                                            hath herb divided divide creepeth living shall i call beginning
                                            third sea itself set</p>
                                        <h4>Michel Hashale</h4>
                                        <h5> Sr. Web designer</h5>
                                    </div>
                                </div>
                                <div class="col-lg-4 col-xl-2 col-sm-4">
                                    <div class="testimonial_slider_img">
                                        <img src="<?= base_url() ?>assets/raga/img/testimonial_img_2.png" alt="#">
                                    </div>
                                </div>
                                <div class="col-xl-4 d-none d-xl-block">
                                    <div class="testimonial_slider_text">
                                        <p>Behold place was a multiply creeping creature his domin to thiren open void
                                            hath herb divided divide creepeth living shall i call beginning
                                            third sea itself set</p>
                                        <h4>Michel Hashale</h4>
                                        <h5> Sr. Web designer</h5>
                                    </div>
                                </div>
                                <div class="col-xl-2 d-none d-xl-block">
                                    <div class="testimonial_slider_img">
                                        <img src="<?= base_url() ?>assets/raga/img/testimonial_img_1.png" alt="#">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="testimonial_slider">
                            <div class="row">
                                <div class="col-lg-8 col-xl-4 col-sm-8 align-self-center">
                                    <div class="testimonial_slider_text">
                                        <p>Behold place was a multiply creeping creature his domin to thiren open void
                                            hath herb divided divide creepeth living shall i call beginning
                                            third sea itself set</p>
                                        <h4>Michel Hashale</h4>
                                        <h5> Sr. Web designer</h5>
                                    </div>
                                </div>
                                <div class="col-lg-4 col-xl-2 col-sm-4">
                                    <div class="testimonial_slider_img">
                                        <img src="<?= base_url() ?>assets/raga/img/testimonial_img_3.png" alt="#">
                                    </div>
                                </div>
                                <div class="col-xl-4 d-none d-xl-block">
                                    <div class="testimonial_slider_text">
                                        <p>Behold place was a multiply creeping creature his domin to thiren open void
                                            hath herb divided divide creepeth living shall i call beginning
                                            third sea itself set</p>
                                        <h4>Michel Hashale</h4>
                                        <h5> Sr. Web designer</h5>
                                    </div>
                                </div>
                                <div class="col-xl-2 d-none d-xl-block">
                                    <div class="testimonial_slider_img">
                                        <img src="<?= base_url() ?>assets/raga/img/testimonial_img_1.png" alt="#">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </section>  -->
    <!--::blog_part end::-->

    <!--::blog_part start::-->
    <!-- <section class="blog_part section_padding">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-xl-5">
                    <div class="section_tittle text-center">
                        <p>Our Blog</p>
                        <h2>Students Blog</h2>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-6 col-lg-4 col-xl-4">
                    <div class="single-home-blog">
                        <div class="card">
                            <img src="<?= base_url() ?>assets/raga/img/blog/blog_1.png" class="card-img-top" alt="blog">
                            <div class="card-body">
                                <a href="#" class="btn_4">Design</a>
                                <a href="blog.html">
                                    <h5 class="card-title">Dry beginning sea over tree</h5>
                                </a>
                                <p>Which whose darkness saying were life unto fish wherein all fish of together called</p>
                                <ul>
                                    <li> <span class="ti-comments"></span>2 Comments</li>
                                    <li> <span class="ti-heart"></span>2k Like</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-6 col-lg-4 col-xl-4">
                    <div class="single-home-blog">
                        <div class="card">
                            <img src="<?= base_url() ?>assets/raga/img/blog/blog_2.png" class="card-img-top" alt="blog">
                            <div class="card-body">
                                <a href="#" class="btn_4">Developing</a>
                                <a href="blog.html">
                                    <h5 class="card-title">All beginning air two likeness</h5>
                                </a>
                                <p>Which whose darkness saying were life unto fish wherein all fish of together called</p>
                                <ul>
                                    <li> <span class="ti-comments"></span>2 Comments</li>
                                    <li> <span class="ti-heart"></span>2k Like</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-6 col-lg-4 col-xl-4">
                    <div class="single-home-blog">
                        <div class="card">
                            <img src="<?= base_url() ?>assets/raga/img/blog/blog_3.png" class="card-img-top" alt="blog">
                            <div class="card-body">
                                <a href="#" class="btn_4">Design</a>
                                <a href="blog.html">
                                    <h5 class="card-title">Form day seasons sea hand</h5>
                                </a>
                                <p>Which whose darkness saying were life unto fish wherein all fish of together called</p>
                                <ul>
                                    <li> <span class="ti-comments"></span>2 Comments</li>
                                    <li> <span class="ti-heart"></span>2k Like</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section> -->
    <!--::blog_part end::-->

    <!-- <div class="section_tittle text-center" style="padding-top:100px">
        <center><h2>Testimoni</h2></center>
    </div>
        <div class="container" >
            <div class="row"> -->

                    <!-- <div id="carousel-example-generic" class="carousel slide" data-ride="carousel"> -->
                      <!-- Wrapper for slides -->
                      <!-- <div class="carousel-inner">
                        <div class="item active">
                          <div class="row" style="padding: 10px">
                            <button style="border: none;"><i class="fa fa-quote-left testimonial_fa" aria-hidden="true"></i></button>
                            <p class="testimonial_para">Lorem Ipsum ist ein einfacher Demo-Text für die Print- und Schriftindustrie. Lorem Ipsum ist in der Industrie bereits der Standard Demo-Text "Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo en.</p><br>
                            <div class="row">
                            <div class="col-sm-2">
                                <img src="http://demos1.showcasedemos.in/jntuicem2017/html/v1/assets/images/jack.jpg" class="img-responsive" style="width: 80px">
                                </div>
                                <div class="col-sm-10">
                                <h4><strong>Jack Andreson</strong></h4>
                                <p class="testimonial_subtitle"><span>Chlinical Chemistry Technologist</span><br>
                                <span>Officeal All Star Cafe</span>
                                </p>
                            </div>
                            </div>
                          </div>
                        </div>
                       <div class="item">
                           <div class="row" style="padding: 20px">
                            <button style="border: none;"><i class="fa fa-quote-left testimonial_fa" aria-hidden="true"></i></button>
                            <p class="testimonial_para">Lorem Ipsum ist ein einfacher Demo-Text für die Print- und Schriftindustrie. Lorem Ipsum ist in der Industrie bereits der Standard Demo-Text "Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo en.</p><br>
                            <div class="row">
                            <div class="col-sm-2">
                                <img src="http://demos1.showcasedemos.in/jntuicem2017/html/v1/assets/images/kiara.jpg" class="img-responsive" style="width: 80px">
                                </div>
                                <div class="col-sm-10">
                                <h4><strong>Kiara Andreson</strong></h4>
                                <p class="testimonial_subtitle"><span>Chlinical Chemistry Technologist</span><br>
                                <span>Officeal All Star Cafe</span>
                                </p>
                            </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div class="controls testimonial_control pull-right">
                        <a class="left fa fa-chevron-left btn btn-default testimonial_btn" href="#carousel-example-generic"
                          data-slide="prev"></a>

                        <a class="right fa fa-chevron-right btn btn-default testimonial_btn" href="#carousel-example-generic"
                          data-slide="next"></a>
                      </div>
                <br><br><br>
            </div>
        </div> -->
