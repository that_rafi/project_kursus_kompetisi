<section class="breadcrumb breadcrumb_bg">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="breadcrumb_iner text-center">
                    <div class="breadcrumb_iner_item">
                        <h2>Kelas Detail</h2>
                        <p>Beranda<span>/</span>Kelas Detail</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- breadcrumb start-->

<!--================ Start Course Details Area =================-->
<section class="course_details_area section_padding">
    <div class="container">
        <div class="row">
            <div class="col-lg-8 course_details_left">
              <ul class="nav nav-pills mb-3" id="pills-tab" role="tablist">
              <li class="nav-item">
                <a class="nav-link active" id="pills-kelas-tab" data-toggle="pill" href="#pills-kelas" role="tab" aria-controls="pills-kelas" aria-selected="true">Kelas</a>
              </li>
              <li class="nav-item">
                <a class="nav-link" id="pills-kursus-tab" data-toggle="pill" href="#pills-kursus" role="tab" aria-controls="pills-kursus" aria-selected="false">Tempat Kursus</a>
              </li>
            </ul>
            <div class="tab-content" id="pills-tabContent">
              <div class="tab-pane active" id="pills-kelas" role="tabpanel" aria-labelledby="pills-kelas-tab">
                <div class="main_image">
                    <img class="img-fluid" src="<?= base_url() ?>assets/upload/resource_kelas/<?php echo $main_resource['filename']; ?>" alt="">
                </div>
                <div class="content_wrapper">
                    <h4 class="title_top"><?= $kelas['nama_kelas']; ?></h4>
                    <div class="content">
                        <?= $kelas['deskripsi_kelas']; ?>
                    </div>

                    <div class="section-top-border">
                        <h3>Image Gallery</h3>
                        <div class="row gallery-item">
                          <?php foreach($resource as $r): ?>
                            <div class="col-md-4">
                                <a href="<?= base_url() ?>assets/upload/resource_kelas/<?php echo $r['filename']; ?>"  class="img-pop-up">
                                    <div class="single-gallery-image" style="background: url(<?= base_url().'assets/upload/resource_kelas/'.$r['filename'] ?>);"></div>
                                </a>
                            </div>
                          <?php endforeach; ?>
                        </div>
                    </div>
                </div>
              </div>
              <div class="tab-pane fade" id="pills-kursus" role="tabpanel" aria-labelledby="pills-kursus-tab">
                <div class="row">
                <div class="col-lg-6">
                <div class="main_image">
                  <?php if($main_resource_kursus!=null){ ?>
                    <img class="img-fluid" src="<?= base_url() ?>assets/upload/resource_kursus/<?php echo $main_resource_kursus['filename']; ?>" style="height:auto;width:800px;" alt="">
                  <?php }else{ ?>
                    <img class="img-fluid" src="<?= base_url() ?>assets/upload/resource_kursus/default.png" alt="">
                <?php  } ?>
                </div>
                </div>
                <div class="col-lg-6">
                  <div class="content_wrapper">
                      <h4 ><?= $kelas['nama_kursus']; ?></h4>
                      <div class="content">
                          <?= $kelas['deskripsi']; ?>
                      </div>
                  </div>
                </div>
              </div>
              <div class="section-top-border">
                  <h3>Lokasi</h3>
                  <?php echo $kelas['lokasi']; ?>
              </div>
              <div class="section-top-border">
                  <h3>Image Gallery</h3>
                  <div class="row gallery-item">
                    <?php foreach($resource_kursus as $r): ?>
                      <div class="col-md-4">
                          <a href="<?= base_url() ?>assets/upload/resource_kursus/<?php echo $r['filename']; ?>" class="img-pop-up">
                              <div class="single-gallery-image" style="background: url(<?= base_url().'assets/upload/resource_kursus/'.$r['filename'] ?>);"></div>
                          </a>
                      </div>
                    <?php endforeach; ?>
                  </div>
              </div>
              </div>
            </div>

            </div>




            <div class="col-lg-4 right-contents">
              <h4 class="title">Informasi Pengajar</h4>
              <div class="content">
                  <div class="review-top row pt-40">
                      <div class="col-lg-12">
                          <!-- <h6 class="mb-15">Provide Your Rating</h6> -->
                          <div class="d-flex flex-row reviews justify-content-between">
                              <span>Nama</span>
                              <div class="container">
                                      <?= $kelas['nama_pengajar'] ?>
                            </div>

                          </div>
                          <div class="d-flex flex-row reviews justify-content-between">
                              <span>Prestasi</span>
                              <div class="container">
                                    <?= $kelas['prestasi_pengajar'] ?>
                              </div>
                          </div>
                          <!-- <div class="d-flex flex-row reviews justify-content-between">
                              <span>Deskripsi</span>
                              <div class="rating">
                                      <a href="#"><img src="<?= base_url() ?>assets/raga/img/icon/color_star.svg" alt=""></a>
                                      <a href="#"><img src="<?= base_url() ?>assets/raga/img/icon/color_star.svg" alt=""></a>
                                      <a href="#"><img src="<?= base_url() ?>assets/raga/img/icon/color_star.svg" alt=""></a>
                                      <a href="#"><img src="<?= base_url() ?>assets/raga/img/icon/color_star.svg" alt=""></a>
                                      <a href="#"><img src="<?= base_url() ?>assets/raga/img/icon/star.svg" alt=""></a>
                                  </div>
                              <span>Outstanding</span>
                          </div> -->

                      </div>
                  </div>
                  <!-- <div class="feedeback">
                      <h6>Your Feedback</h6>
                      <textarea name="feedback" class="form-control" cols="10" rows="10"></textarea>
                      <div class="mt-10 text-right">
                          <a href="#" class="btn_1">Read more</a>
                      </div>
                  </div> -->
                  <!-- <div class="comments-area mb-30">
                      <div class="comment-list">
                          <div class="single-comment single-reviews justify-content-between d-flex">
                              <div class="user justify-content-between d-flex">
                                  <div class="thumb">
                                      <img src="<?= base_url() ?>assets/raga/img/cource/cource_1.png" alt="">
                                  </div>
                                  <div class="desc">
                                      <h5><a href="#">Emilly Blunt</a>
                                      </h5>
                                      <div class="rating">
                                          <a href="#"><img src="<?= base_url() ?>assets/raga/img/icon/color_star.svg" alt=""></a>
                                          <a href="#"><img src="<?= base_url() ?>assets/raga/img/icon/color_star.svg" alt=""></a>
                                          <a href="#"><img src="<?= base_url() ?>assets/raga/img/icon/color_star.svg" alt=""></a>
                                          <a href="#"><img src="<?= base_url() ?>assets/raga/img/icon/color_star.svg" alt=""></a>
                                          <a href="#"><img src="<?= base_url() ?>assets/raga/img/icon/star.svg" alt=""></a>
                                      </div>
                                      <p class="comment">
                                          Blessed made of meat doesn't lights doesn't was dominion and sea earth
                                          form
                                      </p>
                                  </div>
                              </div>
                          </div>
                      </div>
                      <div class="comment-list">
                          <div class="single-comment single-reviews justify-content-between d-flex">
                              <div class="user justify-content-between d-flex">
                                  <div class="thumb">
                                      <img src="<?= base_url() ?>assets/raga/img/cource/cource_2.png" alt="">
                                  </div>
                                  <div class="desc">
                                      <h5><a href="#">Elsie Cunningham</a>
                                      </h5>
                                      <div class="rating">
                                          <a href="#"><img src="<?= base_url() ?>assets/raga/img/icon/color_star.svg" alt=""></a>
                                          <a href="#"><img src="<?= base_url() ?>assets/raga/img/icon/color_star.svg" alt=""></a>
                                          <a href="#"><img src="<?= base_url() ?>assets/raga/img/icon/color_star.svg" alt=""></a>
                                          <a href="#"><img src="<?= base_url() ?>assets/raga/img/icon/color_star.svg" alt=""></a>
                                          <a href="#"><img src="<?= base_url() ?>assets/raga/img/icon/star.svg" alt=""></a>
                                      </div>
                                      <p class="comment">
                                          Blessed made of meat doesn't lights doesn't was dominion and sea earth
                                          form
                                      </p>
                                  </div>
                              </div>
                          </div>
                      </div>
                      <div class="comment-list">
                          <div class="single-comment single-reviews justify-content-between d-flex">
                              <div class="user justify-content-between d-flex">
                                  <div class="thumb">
                                      <img src="<?= base_url() ?>assets/raga/img/cource/cource_3.png" alt="">
                                  </div>
                                  <div class="desc">
                                      <h5><a href="#">Maria Luna</a>
                                      </h5>
                                      <div class="rating">
                                          <a href="#"><img src="<?= base_url() ?>assets/raga/img/icon/color_star.svg" alt=""></a>
                                          <a href="#"><img src="<?= base_url() ?>assets/raga/img/icon/color_star.svg" alt=""></a>
                                          <a href="#"><img src="<?= base_url() ?>assets/raga/img/icon/color_star.svg" alt=""></a>
                                          <a href="#"><img src="<?= base_url() ?>assets/raga/img/icon/color_star.svg" alt=""></a>
                                          <a href="#"><img src="<?= base_url() ?>assets/raga/img/icon/star.svg" alt=""></a>
                                      </div>
                                      <p class="comment">
                                          Blessed made of meat doesn't lights doesn't was dominion and sea earth
                                          form
                                      </p>
                                  </div>
                              </div>
                          </div>
                      </div>
                  </div> -->
              </div>
                <div class="sidebar_top" style="margin-top: 20px">
                    <ul>
                        <li>
                            <a class="justify-content-between d-flex" >
                                <p>Nama Pengajar</p>
                                <span class="color"><?= $kelas['nama_pengajar'] ?></span>
                            </a>
                        </li>
                        <li>
                            <a class="justify-content-between d-flex" href="#">
                                <p>Biaya </p>
                                <span><?= $kelas['biaya'] ?></span>
                            </a>
                        </li>
                        <li>
                            <a class="justify-content-between d-flex" href="#">
                                <p>Jadwal Latihan </p>
                                <span><?= $kelas['jadwal_latihan'] ?></span>
                            </a>
                        </li>
                        <li>
                            <a class="justify-content-between d-flex" href="#">
                                <p>Jangka Waktu </p>
                                <span><?= $kelas['jangka_wkt'] ?></span>
                            </a>
                        </li>
                        <li>
                            <a class="justify-content-between d-flex" href="#">
                                <p>Kontak </p>
                                <span><?= $kelas['kontak_kelas'] ?></span>
                            </a>
                        </li>
                    </ul>
                    <a href="<?= $kelas['link'] ?>" target="_blank" style="text-decoration:none;color:white" class="btn_1 d-block">Daftar Sekarang!</a>
                </div>


            </div>
        </div>
    </div>
</section>
<!--================ End Course Details Area =================-->
