<div class="container" style="margin-top:50px">
    <div class="row align-items-center">
        <div class="col-lg-12">
            <h2>Buat Tempatmu!</h2>
            <div id="msgtxt"></div>
            <h3>Informasi Tempat</h3>
            <hr>
            <form class="form-horizontal" action="/action_page.php">
              <div class="form-group row">
                <input type="hidden" class="form-control" name="id_tempat" id="inputid">
                <label for="inputName" class="col-sm-2 col-form-label">Nama tempat</label>
                <div class="col-sm-10">
                  <input type="text" class="form-control" name="nama_tempat" id="inputName" placeholder="Nama tempat">
                  <?= form_error('nama_tempat','<small class="text-danger pl-3">','</small>'); ?>
                </div>
              </div>
              <div class="form-group row">
                <input type="hidden" class="form-control" name="createdBy" id="inputcreatedby" value="<?= $pengguna['id_pengguna'] ?>">
              </div>
              <div class="form-group row">
                <label for="inputCabang" class="col-sm-2 col-form-label">Cabang Olahraga</label>
                <div class="col-sm-10">
                  <select name="id_cabang" id="inputCB" class="form-control">
                      <?php foreach ($cabang as $c):?>
                            <option value="<?= $c['id_cabang'] ?>" selected><?= $c['nama_cabang'] ?></option>
                      <?php endforeach; ?>
                  </select>
                  <?= form_error('id_cabang','<small class="text-danger pl-3">','</small>'); ?>
                </div>
              </div>
              <div class="form-group row">
                <label for="inputLokasi" class="col-sm-2 col-form-label">Lokasi</label>
                <div class="col-sm-10">
                  <textarea class="form-control" name="lokasi" id="inputLokasi" placeholder="Link Gmaps.."></textarea>
                  <?= form_error('lokasi','<small class="text-danger pl-3">','</small>'); ?>
                </div>
              </div>
              <div class="form-group row">
                <label for="inputDeskripsi" class="col-sm-2 col-form-label">Deskripsi</label>
                <div class="col-sm-10">
                  <textarea class="form-control" name="deskripsi" id="inputDeskripsi" placeholder="Deskripsi.."></textarea>
                  <?= form_error('deskripsi','<small class="text-danger pl-3">','</small>'); ?>
                </div>
              </div>
              <div class="form-group row">
                <label for="inputBiaya" class="col-sm-2 col-form-label">Biaya Sewa</label>
                <div class="col-sm-10">
                  <input type="text" class="form-control" name="biaya_sewa" id="inputBiaya" placeholder="Rp.XXXXX / bulan" >
                  <?= form_error('biaya_sewa','<small class="text-danger pl-3">','</small>'); ?>
                </div>
              </div>
              <div class="form-group row">
                <label for="inputJam" class="col-sm-2 col-form-label">Jam Operasional</label>
                <div class="col-sm-10">
                  <textarea class="form-control" name="jam" id="inputjam" placeholder="Jam.."></textarea>
                  <?= form_error('jam','<small class="text-danger pl-3">','</small>'); ?>
                </div>
              </div>
              <div class="form-group row">
                <label for="inputkontak" class="col-sm-2 col-form-label">Kontak</label>
                <div class="col-sm-10">
                  <input type="text" class="form-control" name="kontak" id="inputkontak" placeholder="Kontak" >
                  <?= form_error('kontak','<small class="text-danger pl-3">','</small>'); ?>
                </div>
              </div>
                <button type="button" id="btnsave" class="btn btn-success" style="float: right;">Simpan</button>
        </div>

</div>
</div>
</div>
<div class="container">
    <div class="row align-items-center">
        <div class="col-lg-12">
            <h3>Tambah Gallery</h3>
            <hr>
        </div>
    </div>
</div>
<style>
  .dropzone{
    width: 300px;
    height: 300px;
    padding: 10px;
    display: flex;
    justify-content: center;
    align-items: center;
    border-radius: 10px;
    border: 2px dashed rgb(204, 204, 204);
    color: rgb(204, 204, 204);
    text-align: center;
    -webkit-box-align: center;
    -webkit-box-pack: center;
    z-index: 100;
    cursor: default;
  }
  .dropzone.dragover{
    border-color: #000;
    color: #000;
  }
  .dropzone_thumb{
    width: 100%;
    height: 100%;
    border-radius: 10px;
    background-image: url('<?= base_url() ?>assets/upload/default/img_tumbnail.png');
    background-size: contain;
    background-repeat: no-repeat;
    position: relative;
    overflow: hidden;
  }
  .dropzone_thumb::after{
    content: attr(data-label);
    position: absolute;
    bottom: 0;
    left: 0;
    width: 100%;
    padding: 5px 0;
    color: #ffffff;
    background-color: grey;
    font-size: 12px;
    text-align: center;
  }
</style>
<div class="col-12">
          <div class="container">
                <div class="form-group row" >
                  <div class="col-lg-6" style="justify-content:center;align-items:center;display:flex">
                    <div class="dropzone" >
                    <label for="browse" class="dropzone_msg">Drop files here</label>
                    <input type="file" name="browse" class="dropzone_input" style="display: none;">
                    <!-- <div class="dropzone_thumb" data-label="mytext.txt"></div> -->
                    </div>
                  </div>
                  <div class="col-lg-6">
                    <div class="col-lg-12">
                    <textarea class="form-control" name="keterangan" id="inputKeterangan" placeholder="Keterangan"></textarea>
                    </div>
                    <div class="col-lg-12">
                      <!-- <button id="getdatainfo" class="btn btn-warning">result</i></a> -->
                      <button id="addinforow" style="margin-top:20px" class="btn btn-success"><i class="fa fa-plus"></i></a>
                    </div>
                  </div>
                </div>
                <div class="form-group row" id="info1">
                  <div class="card-body table-responsive p-0">
                    <?= $this->session->flashdata('message'); ?>
                    <?= $this->session->flashdata('error'); ?>
                    <table class="table table-hover text-nowrap">
                      <thead>
                        <tr>
                          <th>Foto</th>
                          <th>Keterangan</th>
                          <th>Action</th>
                        </tr>
                      </thead>
                      <tbody id="listinfo">

                      </tbody>
                    </table>
                  </div>
                </div>
              </form>
            <!-- /.tab-pane -->
          </div>
          <!-- /.tab-content -->
</div>

<script type="text/javascript">
var base = <?php echo json_encode(base_url()); ?>;
const max = <?php echo json_encode($max_res_tempat); ?>;
var listFile = [];
var listKet = [];
var logoData = '';
var formData = new FormData();
var tempfiles = "";

  $('#inputpic').on('change',function(e){
    console.log(e.target.files[0]);
    var reader = new FileReader();
    logoData = e.target.files[0];
    reader.readAsDataURL(e.target.files[0]);
    reader.onload = function(event) {
      $('#imgbox').attr('style','background-image: url("'+event.target.result+'");background-size:cover')
      $('#imgplaceholder').html('<br/><br/><br/><br/><br/><br/><br/><br/>')
    }
  })

  $('#addinforow').on('click',function(e){
    e.preventDefault();
    var $div = $('tr[id^="listcount"]:last');
    if($div.length > 0 ){
        var num = parseInt( $div.prop("id").match(/\d+/g), 10 ) +1;
    }else{
        var num = 0;
    }

    var url = $('.dropzone_thumb').data('imgurl')
    var imgname = $('.dropzone_thumb').data('label')
    var keterangan = $('#inputKeterangan').val()
    if (num < max ){
      if( keterangan!= "" && url !=null){
        // data for form
        listFile.push(tempfiles)
        listKet.push(keterangan)
        var row = '  <tr id="listcount'+num+'">'+
                  '<td><img src='+url+' width="100px" height="100px"></img></td>'+
                  '<td>'+keterangan+'</td>'+
                  '<td><button id="minusinforow'+num+'" onclick="event.preventDefault(); minusinforow('+num+');" class="btn btn-warning"><i class="fa fa-trash"></i></a></td></tr>'
        $('#listinfo').append(row)
      }else{
        alert('Tolong masukan informasi yang lengkap')
      }
    }else{
      alert('anda sudah mencapai maksimal!')
    }

  })

  function minusinforow(id){
    $('#listcount'+id).remove()
    listFile.splice(id,1);
    listKet.splice(id,1)
  }

  $('#btnsave').on('click',function(e){
    e.preventDefault();
    var id_pengguna = $('#inputcreatedby').val()
    var nama = $('#inputName').val()
    var id_cabang = $('#inputCB').val()
    var lokasi = $('#inputLokasi').val()
    var biaya_sewa = $('#inputBiaya').val()
    var deskripsi = $('#inputDeskripsi').val()
    var jam = $('#inputjam').val()
    var kontak = $('#inputkontak').val()
    for(var i=0;i<listFile.length;i++){
      formData.append('file[]',listFile[i])
      formData.append('keterangan[]',listKet[i])
    }
    formData.append('createdBy',id_pengguna)
    formData.append('nama_tempat',nama)
    formData.append('deskripsi',deskripsi)
    formData.append('id_cabang',id_cabang)
    formData.append('lokasi',lokasi)
    formData.append('biaya_sewa',biaya_sewa)
    formData.append('jam',jam)
    formData.append('kontak',kontak)


    $.ajax({
      url : '<?= base_url(); ?>tempat/tambah/upload',
      type : 'POST',
      dataType : 'json',
      processData: false,
      contentType: false,
      data : formData,
      success : function(res){
        console.log(res)
        if(res.form_error.error){
           // if there is error
          form_validation(res.form_error.msg)
        }else{
          if(res.form_error.insert){
            alert('Sukses menambahkan tempat olahraga baru!');
            window.location = '<?= base_url() ?>dashboard';
          }else{
            alertCall('alert','Something wrong when input data! Please Try again','#msgtxt')
          }
        }
      }
    })
  })

  function setImg(dropzone,files){
    if(files.type.startsWith("image/")){
    var thumbnail = $('.dropzone_thumb').data('label')
    dropzone.attr('class','dropzone')
    if(!thumbnail){
      $('.dropzone_msg').hide()
      var thumb = $('<div/>').addClass('dropzone_thumb');
      dropzone.append(thumb)
    }
    // data for form
    tempfiles = files
    $('.dropzone_thumb').attr('data-label',files.name) // set img name
      var reader = new FileReader();
      reader.readAsDataURL(files);
      reader.onload = function(event) {
            $('.dropzone_thumb').attr('style','background-image: url("'+event.target.result+'")')
            $('.dropzone_thumb').data('imgurl',event.target.result) // hold img url
      };
    }else{
      alert('Please input image only!')
    }
  }

  jQuery(function($){
    $('.dropzone').on('click', function(){
        $('.dropzone_input')[0].click();
    });
  });

    $('.dropzone_input').on('change',function(e){
      $('.dropzone').attr('class','dropzone')
      setImg($('.dropzone'),e.target.files[0])
    })

    $('.dropzone').on('drop',function(e){
      e.preventDefault();
      $(this).attr('class','dropzone')
      setImg($(this),e.originalEvent.dataTransfer.files[0])
    })
    $('.dropzone').on('dragover',function(){
      $(this).attr('class','dropzone dragover')
      return false
    })
    $('.dropzone').on('dragleave',function(){
      $(this).attr('class','dropzone')
      return false
    })

    function form_validation(msg){
      var errors = msg;
                for(var i=0;i<errors.length;i++){
                  if(errors[i]!=""){errors[i] += '<br/>'}
                }
      alertCall('alert',errors.toString().replace(","," "),'#msgtxt')
    }

    function alertCall($type,$msg,$target){
      if($type == 'alert'){
        alt= '<div class="alert alert-danger alert-dismissible">'+
                      '<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>'+
                      '<h5><i class="icon fa fa-ban"></i> Alert!</h5>'+$msg+
                    '</div>'
      }else if($type == 'success'){
        alt= '<div class="alert alert-success alert-dismissible">'+
                      '<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>'+
                      '<h5><i class="icon fa fa-check"></i> Success!</h5>'+$msg+
                    '</div>'
      }
      $($target).html(alt)
    }

</script>
