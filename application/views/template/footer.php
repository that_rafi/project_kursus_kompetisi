<!-- footer part start-->
<footer class="footer-area">
    <div class="container">
        <div class="row justify-content-between">
            <div class="col-sm-6 col-md-6 col-xl-6">
                <div class="single-footer-widget footer_1">
                  <center><a href="index.html"> <img src="<?= base_url()  ?>assets/raga/img/logokecil.png" alt=""> </a></center>

              </div>
          </div>
          <div class="col-sm-6 col-md-6 col-xl-6">
              <div class="single-footer-widget footer_2">
              <h4>CONTACT US</h4>

                    <p>Address: Lorem ipsum dolor sit amet,
                        consectetur adipiscing elit.  <br>
                        Phone: 0855-8888-9999<br>
                        Email: info@raga.com
                     </p>
                </div>
            </div>
        </div>

    </div>
    <div class="container-fluid">
                <div class="copyright_part_text text-center">
                    <div class="row">
                        <div class="col-lg-12">
                            <p class="footer-text m-0"><!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
Copyright &copy;<script>document.write(new Date().getFullYear());</script> Raga. All Rights Reserved.
<!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. --></p>
                        </div>
                    </div>
                </div>
    </div>
</footer>
