<!doctype html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Raga</title>
    <link rel="icon" href="<?= base_url() ?>assets/raga/img/favicon2.png">
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="<?= base_url() ?>assets/raga/css/bootstrap.min.css">
    <!-- animate CSS -->
    <link rel="stylesheet" href="<?= base_url() ?>assets/raga/css/animate.css">
    <!-- owl carousel CSS -->
    <link rel="stylesheet" href="<?= base_url() ?>assets/raga/css/owl.carousel.min.css">
    <!-- themify CSS -->
    <link rel="stylesheet" href="<?= base_url() ?>assets/raga/css/themify-icons.css">
    <!-- flaticon CSS -->
    <link rel="stylesheet" href="<?= base_url() ?>assets/raga/css/flaticon.css">
    <!-- font awesome CSS -->
    <link rel="stylesheet" href="<?= base_url() ?>assets/raga/css/magnific-popup.css">
    <!-- swiper CSS -->
    <link rel="stylesheet" href="<?= base_url() ?>assets/raga/css/slick.css">
    <!-- style CSS -->
    <link rel="stylesheet" href="<?= base_url() ?>assets/raga/css/style.css">
    <script src="https://code.jquery.com/jquery-1.11.1.min.js"></script>
    <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
    <script src="https://use.fontawesome.com/5a8a7bb461.js"></script>

</head>

<body>

    <!--::header part start::-->
    <div class="top-bar">
        <div class="container">
          <?php if(!isset($login_button) || $login_button == ""){ ?>
                <p style="float:right;text-decoration:none;"><a href="<?php echo base_url(); ?>dashboard"><font color="black"><?= $pengguna['nama'] ?></font></a></p>
                <?php }else{ ?>
                <p style="float:right;text-decoration:none;"><?= $login_button; ?></p>
                <?php }?>
        </div>
      </div>
    <header class="main_menu home_menu animated fadeInDown sticky-top">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-lg-12">

                    <nav class="navbar navbar-expand-lg navbar-light">

                        <a class="navbar-brand" href="<?= base_url() ?>"> <img src="<?= base_url() ?>assets/raga/img/logokecil.png" alt="logo"> </a>
                        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent"
                        aria-expanded="false" aria-label="Toggle navigation">
                            <span class="navbar-toggler-icon"></span>
                        </button>


                        <div class="collapse navbar-collapse main-menu-item justify-content-end"
                            id="navbarSupportedContent">
                            <ul class="navbar-nav align-items-center">

                                <li class="nav-item">
                                    <a class="nav-link" href="<?= base_url(); ?>" data-target=".navbar-collapse.show">Beranda</a>
                                </li>
                                <li class="nav-item dropdown">
                                    <a class="nav-link dropdown-toggle" id="navbarDropdown" data-toggle="dropdown" aria-haspopup="true"
                                      aria-expanded="false">Tentang</a>
                                    <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                                        <a class="dropdown-item" href="<?= base_url(); ?>about" >Raga</a>
                                        <a class="dropdown-item" href="<?= base_url(); ?>about">Donate</a>
                                        <a class="dropdown-item" href="<?= base_url(); ?>about">Kontak Kami</a>
                                    </div>
                                </li>

                                <li class="nav-item dropdown">
                                    <a class="nav-link dropdown-toggle" id="navbarDropdown" data-toggle="dropdown" aria-haspopup="true"
                                      aria-expanded="false" >Kelas</a>
                                    <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                                        <a class="dropdown-item" href="<?= base_url(); ?>kelas" >Daftar Kelas</a>
                                        <a class="dropdown-item" href="<?= base_url(); ?>kelas/tambah" >Buat Kelas</a>
                                    </div>
                                </li>

                                <li class="nav-item">
                                    <a class="nav-link" href="<?= base_url(); ?>artikel" data-target=".navbar-collapse.show">Artikel</a>
                                </li>
                                <li class="nav-item dropdown">
                                    <a class="nav-link dropdown-toggle" id="navbarDropdownOR" data-toggle="dropdown" aria-haspopup="true"
                                      aria-expanded="false" >Tempat Olahraga</a>
                                    <div class="dropdown-menu" aria-labelledby="navbarDropdownOR">
                                        <a class="dropdown-item" href="<?= base_url(); ?>tempat" >Daftar Tempat</a>
                                        <a class="dropdown-item" href="<?= base_url(); ?>tempat/tambah" >Buat Tempat</a>
                                    </div>
                                </li>
                                <?php if(!isset($login_button) || $login_button == ""){ ?>
                                  <li class="nav-item dropdown">
                                      <a class="nav-link dropdown-toggle" id="navbarDropdown" data-toggle="dropdown" aria-haspopup="true"
                                      aria-expanded="false" data-target=""><?= substr($pengguna['nama'], 0, 7) . '...'; ?></a>
                                      <div class="dropdown-menu" style="margin-top:10px;margin-left:25px" aria-labelledby="navbarDropdown">
                                          <a class="dropdown-item" href="<?= base_url(); ?>dashboard">Dashboard</a>
                                          <a class="dropdown-item" href="<?= base_url(); ?>profile">Ubah Profil</a>
                                          <a class="dropdown-item" href="<?php echo base_url(); ?>logout">Keluar</a>
                                      </div>
                                  </li>
                                <?php }?>
                            </ul>
                        </div>
                    </nav>
                </div>
            </div>
        </div>
    </header>

    <script type="text/javascript">
            $('navbar-collapse a').on('click', function(){
                $('.navbar-collapse').collapse('hide');
                });
        </script>
