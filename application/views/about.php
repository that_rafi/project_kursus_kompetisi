<section class="breadcrumb breadcrumb_bg">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="breadcrumb_iner text-center">
                    <div class="breadcrumb_iner_item">
                        <h2>Tentang</h2>
                        <p>Beranda<span>/</span>About Us</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- breadcrumb start-->

<!-- feature_part start-->
<section class="feature_part single_feature_padding">
    <div class="container">
        <div class="row">
            <div class="col-sm-6 col-xl-3 align-self-center">
                <div class="single_feature_text ">
                    <h2>Mengapa kami?</h2>
                    <p>Olahraga adalah senjata rahasia melawan kemiskinan, dengan
                        olahraga banyak orang mendapat penghidupan yang lebih baik.
                         </p>
                    <a href="#info" class="btn_1">Read More</a>
                </div>
            </div>
            <div class="col-sm-6 col-xl-3">
                <div class="single_feature">
                    <div class="single_feature_part">
                        <img src="<?= base_url() ?>assets/raga/img/kemudahan.png" width="50%">
                        <h4>Kemudahan</h4>
                        <p> Raga memberi kemudahan  mulai dari mendaftar kursus olahraga
                            hingga menyewa tempat untuk berolahraga bisa melalui Raga.</p>
                    </div>
                </div>
            </div>
            <div class="col-sm-6 col-xl-3">
                <div class="single_feature">
                    <div class="single_feature_part">
                        <img src="<?= base_url() ?>assets/raga/img/berkompeten.png" width="100px">
                        <h4>Berkompeten</h4>
                        <p>Raga bekerja sama dengan mitra-mitra olahraga
                            yang berkompeten dan teruji dalam bidang olahraga.</p>
                    </div>
                </div>
            </div>
            <div class="col-sm-6 col-xl-3">
                <div class="single_feature">
                    <div class="single_feature_part single_feature_part_2">
                        <img src="<?= base_url() ?>assets/raga/img/terpercaya.png" width="100px">
                        <h4>Terpercaya</h4>
                        <p>Raga menyajikan informasi dengan tepat dan akurat.
                            Informasi yang Raga sajikanbisa dipertanggungjawabkan.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- upcoming_event part start-->

<!-- learning part start-->
<section class="learning_part" id="info">
    <div class="container">
        <div class="row align-items-sm-center align-items-lg-stretch">
            <div class="col-md-7 col-lg-7">
                <div class="learning_img">
                    <img src="<?= base_url() ?>assets/raga/img/raga.png" alt="">
                </div>
            </div>
            <div class="col-md-5 col-lg-5" >
                <div class="learning_member_text" >
                    <h2>To Inform, To Influence, To Inspire</h2>
                    <p>Olahraga adalah senjata rahasia melawan kemiskinan, dengan olahraga banyak orang mendapat penghidupan yang lebih baik.
                        Kami percaya dengan kamudahan berolahraga akan menciptakan
                        kabaikan bagi banyak orang.
                        Raga hadir untuk mempermudah segala urusan yang berkaitan dengan olahraga.
                        Bekerja sama dengan mitra-mitra terpercaya, raga berusaha memberikan solusi atas persoalan yang ada.
                    </p>
                    <p>
                        Tujuan kami sederhana yaitu “kemudahan dalam berolahraga”, kami menyajikan informasi tempat-tempat
                        kursus olahraga dan kami mendampingi dalam pengelolaannya dengan merumuskan standar pengajaran
                         sesuai dengan masing-masing cabang olahraga. Hingga pada akhirnya kami
                        memberikan output berupa nilai dan juga penghargaan bagi murid yang mengikuti kelas olahraga.
                    </p>
                </div>

            </div>
            <p style="font-size:18px">
                Kami percaya setiap orang memiliki potensi dan layak diberi ruang untuk berkembang.
                Dalam olahraga kami yakin dan percaya mulai dari Raga untuk olahraga Indonesia.
            </p>
        </div>
    </div>
</section>
