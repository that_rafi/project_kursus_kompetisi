<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <div class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1 class="m-0 text-dark"><?= $pengajar['nama_pengajar'] ?>'s Detail</h1>
        </div><!-- /.col -->
      </div><!-- /.row -->
    </div><!-- /.container-fluid -->
  </div>
  <!-- /.content-header -->

  <!-- Main content -->
  <section class="content">
  <div class="container-fluid">
    <!-- Small boxes (Stat box) -->
    <div class="row">
      <div class="col-12">
        <div class="card">
              <div class="card-header">
                <!-- <div class="container" style="width:7%;float:right">
                <button type="button" id="btndel" data-toggle="modal" data-target="#modal-danger-delete" class="btn btn-outline-danger"><i class="fas fa-trash"></i></button>
                </div> -->
              </div><!-- /.card-header -->
              <div class="card-body">
                <?= $this->session->flashdata('message'); ?>
                <?= $this->session->flashdata('error'); ?>
                <div class="tab-content">
                  <!-- /.tab-pane -->
                  <div class="tab-pane active" id="pengajar">
                    <form class="form-horizontal" action="<?= base_url() ?>admin/pengajar/detail/pjr<?= $pengajar['id_pengajar']; ?>" method="post" >
                      <div class="form-group row">
                        <input type="hidden" class="form-control" name="$id_pengajar" id="inputid" value="<?= $pengajar['id_pengajar']; ?>">
                        <label for="inputNama" class="col-sm-2 col-form-label">Nama</label>
                        <div class="col-sm-10">
                          <input type="text" class="form-control" name="nama_pengajar" id="inputnama_pengajar" value="<?= $pengajar['nama_pengajar']; ?>" placeholder="Nama....">
                          <?= form_error('nama_pengajar','<small class="text-danger pl-3">','</small>'); ?>
                        </div>
                      </div>
                      <div class="form-group row">
                        <label for="inputDeskripsi" class="col-sm-2 col-form-label">Deskripsi</label>
                        <div class="col-sm-10">
                          <textarea class="form-control" name="deskripsi_pengajar" id="inputStatus" placeholder="Deskripsi...."><?= $pengajar['deskripsi_pengajar']; ?></textarea>
                        </div>
                      </div>
                      <div class="form-group row">
                        <label for="inputPrestasi" class="col-sm-2 col-form-label">Prestasi</label>
                        <div class="col-sm-10">
                          <textarea class="form-control" name="prestasi_pengajar" id="inputPrestasi" placeholder="Prestasi...."><?= $pengajar['prestasi_pengajar']; ?></textarea>
                        </div>
                      </div>
                      <div class="form-group row">
                        <div class="offset-sm-2 col-sm-10">
                          <input type="submit" value="Simpan" class="btn btn-info"></button>
                        </div>
                      </div>
                    </form>
                  </div>
                  <!-- /.tab-pane -->
                </div>
                <!-- /.tab-content -->
              </div><!-- /.card-body -->
            </div>
        <!-- /.card -->
      </div>
    <!-- /.row (main row) -->
  </div><!-- /.container-fluid -->
</section>
  <!-- /.content -->
</div>
<!-- modal delete -->
<div class="modal fade" id="modal-danger-delete" style="display: none;" aria-hidden="true">
        <div class="modal-dialog">
          <div class="modal-content bg-danger">
            <div class="modal-header">
              <h4 class="modal-title">Confirmation</h4>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">×</span>
              </button>
            </div>
            <div class="modal-body">
              <p>Are you sure you want to delete this data?…</p>
            </div>
            <div class="modal-footer justify-content-between">
              <button type="button" class="btn btn-outline-light" data-dismiss="modal">No</button>
              <a href="<?= base_url() ?>admin/pengajar/delete/pjr<?= $pengajar['id_pengajar'] ?>" class="btn btn-outline-light" id="yesbtn">Yes</a>
            </div>
          </div>
          <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
      </div>
