<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <div class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1 class="m-0 text-dark">Setting Raga Website</h1>
        </div><!-- /.col -->
      </div><!-- /.row -->
    </div><!-- /.container-fluid -->
  </div>
  <!-- /.content-header -->

  <!-- Main content -->
  <section class="content">
  <div class="container-fluid">
    <!-- Small boxes (Stat box) -->
    <div class="row">
      <div class="col-12">
        <div class="card">
              <div class="card-header">
                <?= $this->session->flashdata('message'); ?>
                <?= $this->session->flashdata('error'); ?>
              </div><!-- /.card-header -->
              <div class="card-body">
                <div class="tab-content">
                  <!-- /.tab-pane -->
                  <div class="tab-pane active" id="settings">
                    <form class="form-horizontal" action="<?= base_url() ?>admin/setting" method="post" >
                      <div class="form-group row">
                        <input type="hidden" class="form-control" name="$id_setting" id="inputid" value="<?= $setting['id_setting'];; ?>">
                        <label for="inputKontak" class="col-sm-2 col-form-label">Kontak</label>
                        <div class="col-sm-10">
                          <input type="text" class="form-control" name="kontak" id="inputKontak" value="<?= $setting['kontak']; ?>" placeholder="Kontak....">
                          <?= form_error('kontak','<small class="text-danger pl-3">','</small>'); ?>
                        </div>
                      </div>
                      <div class="form-group row">
                        <label for="inputEmail" class="col-sm-2 col-form-label">Email</label>
                        <div class="col-sm-10">
                          <input type="email" class="form-control" name="email" value="<?= $setting['email']; ?>" id="inputEmail" placeholder="Email">
                          <?= form_error('email','<small class="text-danger pl-3">','</small>'); ?>
                        </div>
                      </div>
                      <div class="form-group row">
                        <label for="inputWeb" class="col-sm-2 col-form-label">Website</label>
                        <div class="col-sm-10">
                          <input type="text" class="form-control" name="web" value="<?= $setting['web']; ?>" >
                          <?= form_error('web','<small class="text-danger pl-3">','</small>'); ?>
                        </div>
                      </div>
                      <div class="form-group row">
                        <label for="inputmax_kelas" class="col-sm-2 col-form-label">Max Kelas</label>
                        <div class="col-sm-10">
                          <input type="text" class="form-control" name="max_kelas" value="<?= $setting['max_kelas']; ?>" >
                          <?= form_error('max_kelas','<small class="text-danger pl-3">','</small>'); ?>
                        </div>
                      </div>
                      <div class="form-group row">
                        <label for="inputmax_tempat" class="col-sm-2 col-form-label">Max Tempat Olahraga</label>
                        <div class="col-sm-10">
                          <input type="text" class="form-control" name="max_tempat" value="<?= $setting['max_tempat']; ?>" >
                          <?= form_error('max_tempat','<small class="text-danger pl-3">','</small>'); ?>
                        </div>
                      </div>
                      <div class="form-group row">
                        <label for="inputmax_res_kursus" class="col-sm-2 col-form-label">Max Resource Kursus</label>
                        <div class="col-sm-10">
                          <input type="text" class="form-control" name="max_res_kursus" value="<?= $setting['max_res_kursus']; ?>" >
                          <?= form_error('max_res_kursus','<small class="text-danger pl-3">','</small>'); ?>
                        </div>
                      </div>
                      <div class="form-group row">
                        <label for="inputmax_res_kelas" class="col-sm-2 col-form-label">Max Resource Kelas</label>
                        <div class="col-sm-10">
                          <input type="text" class="form-control" name="max_res_kelas" value="<?= $setting['max_res_kelas']; ?>" >
                          <?= form_error('max_res_kelas','<small class="text-danger pl-3">','</small>'); ?>
                        </div>
                      </div>
                      <div class="form-group row">
                        <label for="inputmax_res_tempat" class="col-sm-2 col-form-label">Max Resource Tempat</label>
                        <div class="col-sm-10">
                          <input type="text" class="form-control" name="max_res_tempat" value="<?= $setting['max_res_tempat']; ?>" >
                          <?= form_error('max_res_tempat','<small class="text-danger pl-3">','</small>'); ?>
                        </div>
                      </div>
                      <div class="form-group row">
                        <label for="inputmax_res_artikel" class="col-sm-2 col-form-label">Max Resource Artikel</label>
                        <div class="col-sm-10">
                          <input type="text" class="form-control" name="max_res_artikel" value="<?= $setting['max_res_artikel']; ?>" >
                          <?= form_error('max_res_artikel','<small class="text-danger pl-3">','</small>'); ?>
                        </div>
                      </div>
                      <div class="form-group row">
                        <label for="inputStatus" class="col-sm-2 col-form-label">Setting Status</label>
                        <div class="col-sm-10">
                          <textarea class="form-control" name="setting_status" id="inputStatus" placeholder="Status...."><?= $setting['setting_status']; ?></textarea>
                        </div>
                      </div>
                      <div class="form-group row">
                        <div class="offset-sm-2 col-sm-10">
                          <input type="submit" value="Simpan" class="btn btn-info"></button>
                        </div>
                      </div>
                    </form>
                  </div>
                  <!-- /.tab-pane -->
                </div>
                <!-- /.tab-content -->
              </div><!-- /.card-body -->
            </div>
        <!-- /.card -->
      </div>
    <!-- /.row (main row) -->
  </div><!-- /.container-fluid -->
</section>
  <!-- /.content -->
</div>
<!-- modal delete -->
