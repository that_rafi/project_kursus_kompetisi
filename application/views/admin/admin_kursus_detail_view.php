<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <div class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1 class="m-0 text-dark"><?= $kursus['nama_kursus'] ?>'s Detail</h1>
        </div><!-- /.col -->
      </div><!-- /.row -->
    </div><!-- /.container-fluid -->
  </div>
  <!-- /.content-header -->

  <!-- Main content -->
  <section class="content">
  <div class="container-fluid">
    <!-- Small boxes (Stat box) -->
    <div class="row">
      <div class="col-12">
        <div class="card">
              <div class="card-header">
                <div class="container" style="width:7%;float:right">
                <button type="button" id="btndel" data-toggle="modal" data-target="#modal-danger-delete" class="btn btn-outline-danger"><i class="fas fa-trash"></i></button>
                </div>
              </div><!-- /.card-header -->
              <div class="card-body">
                <div class="tab-content">
                  <!-- /.tab-pane -->
                  <?= $this->session->flashdata('message'); ?>
                  <?= $this->session->flashdata('error'); ?>
                  <div class="tab-pane active" id="settings">
                    <form class="form-horizontal" action="<?= base_url() ?>admin/kursus/detail/krs<?= $kursus['id_kursus'] ?>" method="post" >
                      <div class="form-group row">
                        <input type="hidden" class="form-control" name="logo" id="inputlogo" value="<?= $kursus['logo'] ?>">
                        <label for="inputCB" class="col-sm-2 col-form-label">Logo</label>
                        <div class="col-sm-10">
                          <img src="<?= base_url() ?>assets/upload/logo_kursus/<?= $kursus['logo'] ?>" width="150px" height="150px"/>
                        </div>
                      </div>
                      <div class="form-group row">
                        <input type="hidden" class="form-control" name="id_kursus" id="inputid" value="<?= $kursus['id_kursus'] ?>">
                        <label for="inputName" class="col-sm-2 col-form-label">Nama Kursus</label>
                        <div class="col-sm-10">
                          <input type="text" class="form-control" name="nama_kursus" id="inputName" value="<?= $kursus['nama_kursus']; ?>" placeholder="Nama Kursus">
                          <?= form_error('nama_kursus','<small class="text-danger pl-3">','</small>'); ?>
                        </div>
                      </div>
                      <div class="form-group row">
                        <input type="hidden" class="form-control" name="createdBy" id="inputcreatedby" value="<?= $kursus['createdBy'] ?>">
                        <label for="inputCB" class="col-sm-2 col-form-label">Created By</label>
                        <div class="col-sm-10">
                          <input type="text" class="form-control" name="nama" value="<?= $kursus['nama']; ?>" id="inputNama" placeholder="Nama Pengguna" readonly>
                        </div>
                      </div>
                      <div class="form-group row">
                        <input type="hidden" class="form-control" name="cabang_olahraga" id="inputidcabang" value="<?= $kursus['cabang_olahraga'] ?>">
                        <label for="inputCabang" class="col-sm-2 col-form-label">Cabang Olahraga</label>
                        <div class="col-sm-10">
                          <select name="cabang_olahraga" id="inputCB" class="custom-select">
                              <?php foreach ($cabang as $c):
                                  if($c['id_cabang'] == $kursus['id_cabang'] ){ ?>
                                    <option value="<?= $c['id_cabang'] ?>" selected><?= $c['nama_cabang'] ?></option>
                                  <?php }else{ ?>
                                    <option value="<?= $c['id_cabang'] ?>" ><?= $c['nama_cabang'] ?></option>
                              <?php }endforeach; ?>
                          </select>
                          <?= form_error('cabang_olahraga','<small class="text-danger pl-3">','</small>'); ?>
                        </div>
                      </div>
                      <div class="form-group row">
                        <label for="inputLokasi" class="col-sm-2 col-form-label">Lokasi</label>
                        <div class="col-sm-10">
                          <textarea class="form-control" name="lokasi" id="inputLokasi" placeholder="Lokasi.."><?= $kursus['lokasi']; ?></textarea>
                          <?= form_error('lokasi','<small class="text-danger pl-3">','</small>'); ?>
                        </div>
                      </div>
                      <div class="form-group row">
                        <label for="inputKontak" class="col-sm-2 col-form-label">No. Hp / Kontak</label>
                        <div class="col-sm-10">
                          <input type="tel" class="form-control" name="kontak_kursus" id="inputKontak" placeholder="Kontak.." value="<?= $kursus['kontak_kursus']; ?>"></input>
                          <?= form_error('kontak_kursus','<small class="text-danger pl-3">','</small>'); ?>
                        </div>
                      </div>
                      <div class="form-group row">
                        <label for="inputDeskripsi" class="col-sm-2 col-form-label">Deskripsi</label>
                        <div class="col-sm-10">
                          <textarea class="form-control" name="deskripsi" id="inputDeskripsi" placeholder="Deskripsi.."><?= $kursus['deskripsi']; ?></textarea>
                          <?= form_error('deskripsi','<small class="text-danger pl-3">','</small>'); ?>
                        </div>
                      </div>
                      <div class="form-group row">
                        <label for="inputstatus" class="col-sm-2 col-form-label">Status</label>
                        <div class="col-sm-10">
                          <select name="status" id="inputstatus" class="custom-select">
                              <?php for($i=0;$i<count($status);$i++) {
                                  if($status[$i] == $kursus['status'] ){ ?>
                                    <option value="<?= $kursus['status'] ?>" selected><?= $kursus['status'] ?></option>
                                  <?php }else{ ?>
                                    <option value="<?= $status[$i] ?>" ><?= $status[$i] ?></option>
                              <?php } } ?>
                          </select>
                            <?= form_error('status','<small class="text-danger pl-3">','</small>'); ?>
                        </div>
                      </div>
                      <div class="form-group row">
                        <label for="inputDate" class="col-sm-2 col-form-label">Tanggal Upload</label>
                        <div class="col-sm-10">
                          <input type="text" class="form-control" name="createdOn" value="<?= $kursus['createdOn'];  ?>" readonly id="inputdate" placeholder="createdOn">
                            <?= form_error('createdOn','<small class="text-danger pl-3">','</small>'); ?>
                        </div>
                      </div>
                      <div class="form-group row">
                        <div class="offset-sm-2 col-sm-10">
                          <input type="submit" value="Simpan" class="btn btn-info"></button>
                        </div>
                      </div>
                    </form>
                  </div>
                  <!-- /.tab-pane -->
                </div>
                <!-- /.tab-content -->
              </div><!-- /.card-body -->
            </div>
        <!-- /.card -->
      </div>
      <div class="col-12">
        <div class="card">
              <div class="card-header p-2">
                <div class="container">
                <h4> Galeri </h4>
                </div>
              </div><!-- /.card-header -->
              <div class="card-body">
                <div class="tab-content">
                  <div class="row mb-3">
                    <?php if($countres > 0 ){ ?>
                        <!-- <div class="col-sm-6">
                          <img class="img-fluid" src="<?= base_url() ?>assets/upload/default/img_tumbnail.png" alt="Photo">
                        </div> -->
                        <!-- /.col -->
                        <div class="col-sm-12">
                          <div class="row">
                            <?php foreach ($resource as $res): ?>
                              <div class="col-sm-2">
                                 <a target="_blank" href="<?= base_url() ?>assets/upload/resource_kursus/<?= $res['filename'] ?>">
                                <img class="img-fluid mb-3" src="<?= base_url() ?>assets/upload/resource_kursus/<?= $res['filename'] ?>"  alt="<?= $res['filename']  ?>">
                              </a>
                              </div>
                            <?php endforeach; ?>
                            <!-- /.col -->
                          </div>
                          <!-- /.row -->
                        </div>
                        <!-- /.col -->
                      </div>
                    <?php } else { ?>
                        <h5>Tidak ada galeri</h5>
                    <?php } ?>
                  <!-- /.tab-pane -->
                </div>
                <!-- /.tab-content -->
              </div><!-- /.card-body -->
            </div>
      </div>
    <!-- /.row (main row) -->
  </div><!-- /.container-fluid -->
</section>
  <!-- /.content -->
</div>
<!-- modal delete -->
<div class="modal fade" id="modal-danger-delete" style="display: none;" aria-hidden="true">
        <div class="modal-dialog">
          <div class="modal-content bg-danger">
            <div class="modal-header">
              <h4 class="modal-title">Confirmation</h4>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">×</span>
              </button>
            </div>
            <div class="modal-body">
              <p>Are you sure you want to delete this data?…</p>
            </div>
            <div class="modal-footer justify-content-between">
              <button type="button" class="btn btn-outline-light" data-dismiss="modal">No</button>
              <a href="<?= base_url() ?>admin/kursus/delete/krs<?= $kursus['id_kursus'] ?>" class="btn btn-outline-light" id="yesbtn">Yes</a>
            </div>
          </div>
          <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
      </div>

<script type="text/javascript">
var base = <?php echo json_encode(base_url()); ?>;

</script>
