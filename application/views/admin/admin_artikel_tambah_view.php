
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <div class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1 class="m-0 text-dark">Tambah Artikel</h1>
        </div><!-- /.col -->
      </div><!-- /.row -->
    </div><!-- /.container-fluid -->
  </div>
  <!-- /.content-header -->

  <!-- Main content -->
  <section class="content">
  <div class="container-fluid">
    <!-- Small boxes (Stat box) -->
    <div class="row">
      <div class="col-12">
        <div class="card">
              <!-- <div class="card-header">
              </div>-->
              <div class="card-body">
                <div class="tab-content">
                  <!-- /.tab-pane -->
                  <?= $this->session->flashdata('message'); ?>
                  <?= $this->session->flashdata('error'); ?>
                  <div id="msgtxt"></div>
                  <div class="tab-pane active" id="settings">
                    <form class="form-horizontal" id="formartikelinfo" method="post" >
                      <div class="form-group row">
                        <label for="inputName" class="col-sm-2 col-form-label">Judul</label>
                        <div class="col-sm-10">
                          <input type="text" class="form-control" name="judul" id="inputJudul" required value="<?= set_value('judul'); ?>" placeholder="Judul">
                          <?= form_error('judul','<small class="text-danger pl-3">','</small>'); ?>
                        </div>
                      </div>
                      <div class="form-group row">
                        <label for="inputIsi" class="col-sm-2 col-form-label">Isi</label>
                        <div class="col-sm-10">
                          <textarea class="form-control" required name="isi" id="inputIsi"><?= set_value('isi'); ?></textarea>
                          <?= form_error('isi','<small class="text-danger pl-3">','</small>'); ?>
                        </div>
                      </div>
                      <div class="form-group row">
                        <label for="inputTanggal" class="col-sm-2 col-form-label">Tanggal</label>
                        <div class="col-sm-10">
                          <input type="date" required class="form-control" id="inputDate" name="createdOn" value="<?= set_value('createdOn'); ?>" >
                          <?= form_error('createdOn','<small class="text-danger pl-3">','</small>'); ?>
                        </div>
                      </div>
                      <div class="form-group row">
                        <div class="offset-sm-2 col-sm-10">
                          <button type="submit" id="btnsave" class="btn btn-info">Tambah</button>
                        </div>
                      </div>
                    </form>
                  </div>
                  <!-- /.tab-pane -->
                </div>
                <!-- /.tab-content -->
              </div><!-- /.card-body -->
            </div>
        <!-- /.card -->
      </div>
      <!-- informasi tambahan -->
      <style>
        .dropzone{
          width: 300px;
          height: 300px;
          padding: 10px;
          display: flex;
          justify-content: center;
          align-items: center;
          border-radius: 10px;
          border: 2px dashed rgb(204, 204, 204);
          color: rgb(204, 204, 204);
          text-align: center;
          -webkit-box-align: center;
          -webkit-box-pack: center;
          z-index: 100;
          cursor: default;
        }
        .dropzone.dragover{
          border-color: #000;
          color: #000;
        }
        .dropzone_thumb{
          width: 100%;
          height: 100%;
          border-radius: 10px;
          background-image: url('<?= base_url() ?>assets/upload/default/img_tumbnail.png');
          background-size: contain;
          background-repeat: no-repeat;
          position: relative;
          overflow: hidden;
        }
        .dropzone_thumb::after{
          content: attr(data-label);
          position: absolute;
          bottom: 0;
          left: 0;
          width: 100%;
          padding: 5px 0;
          color: #ffffff;
          background-color: grey;
          font-size: 12px;
          text-align: center;
        }
        .dropzone_input{
          display: none;
        }
      </style>
      <div class="col-12">
        <div class="card">
          <div class="card-header">
            <h1 class="card-title">Informasi Tambahan</h1>
            <div class="card-tools">
            </div>
          </div><!-- /.card-header -->
              <div class="card-body">
                <div class="tab-content">
                    <form class="form-horizontal" >
                      <div class="form-group row" >
                        <div class="col-lg-6" style="justify-content:center;align-items:center;display:flex">
                          <div class="dropzone" >
                          <label for="browse" class="dropzone_msg">Drop files here</label>
                          <input type="file" name="browse" class="dropzone_input">
                          <!-- <div class="dropzone_thumb" data-label="mytext.txt"></div> -->
                          </div>
                        </div>
                        <div class="col-lg-6">
                          <div class="col-lg-12">
                          <textarea class="form-control" name="keterangan" id="inputKeterangan" placeholder="Keterangan"></textarea>
                          </div>
                          <div class="col-lg-12">
                            <!-- <button id="getdatainfo" class="btn btn-warning">result</i></a> -->
                            <button id="addinforow" style="margin-top:20px" class="btn btn-success"><i class="fas fa-plus"></i></a>
                          </div>
                        </div>
                      </div>
                      <div class="form-group row" id="info1">
                        <div class="card-body table-responsive p-0">
                          <?= $this->session->flashdata('message'); ?>
                          <?= $this->session->flashdata('error'); ?>
                          <table class="table table-hover text-nowrap">
                            <thead>
                              <tr>
                                <th>Foto</th>
                                <th>Keterangan</th>
                                <th>Action</th>
                              </tr>
                            </thead>
                            <tbody id="listinfo">

                            </tbody>
                          </table>
                        </div>
                      </div>
                    </form>
                  <!-- /.tab-pane -->
                </div>
                <!-- /.tab-content -->
              </div><!-- /.card-body -->
            </div>
      </div>
    <!-- /.row (main row) -->
  </div><!-- /.container-fluid -->
</section>
  <!-- /.content -->
</div>


<script type="text/javascript">
var base = <?php echo json_encode(base_url()); ?>;
var listFile = [];
var listKet = [];
var formData = new FormData();
var tempfiles = "";
  $('#addinforow').on('click',function(e){
    e.preventDefault();
    var $div = $('tr[id^="listcount"]:last');
    if($div.length > 0 ){
        var num = parseInt( $div.prop("id").match(/\d+/g), 10 ) +1;
    }else{
        var num = 0;
    }

    var url = $('.dropzone_thumb').data('imgurl')
    var imgname = $('.dropzone_thumb').data('label')
    var keterangan = $('#inputKeterangan').val()
    if (num < 9 ){
      if( keterangan!= "" && url !=null){
        // data for form
        listFile.push(tempfiles)
        listKet.push(keterangan)
        var row = '  <tr id="listcount'+num+'">'+
                  '<td><img src='+url+' width="30px" height="30px"></img></td>'+
                  '<td>'+keterangan+'</td>'+
                  '<td><button id="minusinforow'+num+'" onclick="event.preventDefault(); minusinforow('+num+');" class="btn btn-warning"><i class="fas fa-minus"></i></a></td></tr>'
        $('#listinfo').append(row)
      }else{
        alert('Tolong masukan informasi yang lengkap')
      }
    }else{
      alert('anda sudah mencapai maksimal!')
    }

  })

  function minusinforow(id){
    $('#listcount'+id).remove()
    listFile.splice(id,1);
    listKet.splice(id,1)
  }

  $('#btnsave').on('click',function(e){
    e.preventDefault();
    var judul = $('#inputJudul').val()
    var isi = $('#inputIsi').val()
    var date = $('#inputDate').val()
    for(var i=0;i<listFile.length;i++){
      formData.append('file[]',listFile[i])
      formData.append('keterangan[]',listKet[i])
    }
    formData.append('judul',judul)
    formData.append('isi',isi)
    formData.append('createdOn',date)

    $.ajax({
      url : '<?= base_url(); ?>admin/artikel/upload',
      type : 'POST',
      dataType : 'json',
      processData: false,
      contentType: false,
      data : formData,
      success : function(res){
        console.log(res)
        if(res.form_error.error){
           // if there is error
          form_validation(res.form_error.msg)
        }else{
          if(res.form_error.insert){
            alert('Successfully Menambahkan Artikel');
            window.location = '<?= base_url() ?>admin/artikel';
          }else{
            alertCall('alert','Something wrong when input data! Please Try again','#msgtxt')
          }
        }
      }
    })
  })

  function setImg(dropzone,files){
    if(files.type.startsWith("image/")){
    var thumbnail = $('.dropzone_thumb').data('label')
    dropzone.attr('class','dropzone')
    if(!thumbnail){
      $('.dropzone_msg').hide()
      var thumb = $('<div/>').addClass('dropzone_thumb');
      dropzone.append(thumb)
    }
    // data for form
    tempfiles = files
    $('.dropzone_thumb').attr('data-label',files.name) // set img name
      var reader = new FileReader();
      reader.readAsDataURL(files);
      reader.onload = function(event) {
            $('.dropzone_thumb').attr('style','background-image: url("'+event.target.result+'")')
            $('.dropzone_thumb').data('imgurl',event.target.result) // hold img url
      };
    }else{
      alert('Please input image only!')
    }
  }

  jQuery(function($){
    $('.dropzone').on('click', function(){
        $('.dropzone_input')[0].click();
    });
  });

    $('.dropzone_input').on('change',function(e){
      $('.dropzone').attr('class','dropzone')
      setImg($('.dropzone'),e.target.files[0])
    })

    $('.dropzone').on('drop',function(e){
      e.preventDefault();
      $(this).attr('class','dropzone')
      setImg($(this),e.originalEvent.dataTransfer.files[0])
    })
    $('.dropzone').on('dragover',function(){
      $(this).attr('class','dropzone dragover')
      return false
    })
    $('.dropzone').on('dragleave',function(){
      $(this).attr('class','dropzone')
      return false
    })

    function form_validation(msg){
      var errors = msg;
                for(var i=0;i<errors.length;i++){
                  if(errors[i]!=""){errors[i] += '<br/>'}
                }
      alertCall('alert',errors.toString().replace(","," "),'#msgtxt')
    }

    function alertCall($type,$msg,$target){
      if($type == 'alert'){
        alt= '<div class="alert alert-danger alert-dismissible">'+
                      '<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>'+
                      '<h5><i class="icon fas fa-ban"></i> Alert!</h5>'+$msg+
                    '</div>'
      }else if($type == 'success'){
        alt= '<div class="alert alert-success alert-dismissible">'+
                      '<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>'+
                      '<h5><i class="icon fas fa-check"></i> Success!</h5>'+$msg+
                    '</div>'
      }
      $($target).html(alt)
    }

</script>
