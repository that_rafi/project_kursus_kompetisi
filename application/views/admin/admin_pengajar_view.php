pjr



     <!-- Content Wrapper. Contains page content -->
     <div class="content-wrapper">
       <!-- Content Header (Page header) -->
       <div class="content-header">
         <div class="container-fluid">
           <div class="row mb-2">
             <div class="col-sm-6">
               <h1 class="m-0 text-dark">Data Pengajar</h1>
             </div><!-- /.col -->
           </div><!-- /.row -->
         </div><!-- /.container-fluid -->
       </div>
       <!-- /.content-header -->

       <!-- Main content -->
       <section class="content">
       <div class="container-fluid">
         <!-- Small boxes (Stat box) -->
         <div class="row">
           <div class="col-12">
             <div id="actionmsg"></div>
             <div class="card">
               <div class="card-header">
                 <h3 class="card-title"></h3>
                 <div class="card-tools">
                   <div class="input-group input-group-sm" style="width: 150px;">
                     <input type="text" name="keyword" id="search-text" class="form-control float-right" placeholder="Search" >
                     <div class="input-group-append">
                       <button type="submit" onclick="searchdata()" class="btn btn-default"><i class="fas fa-search"></i></button>
                     </div>
                   </div>
                 </div>
               </div>

               <!-- /.card-header -->
               <div class="card-body table-responsive p-0">
                 <?= $this->session->flashdata('message'); ?>
                 <?= $this->session->flashdata('error'); ?>
                 <table class="table table-hover text-nowrap">
                   <thead>
                     <tr>
                       <th>No</th>
                       <th>Nama Pengajar</th>
                       <th>Deskripsi</th>
                       <th>Prestasi</th>
                       <th>Action</th>
                     </tr>
                   </thead>
                   <tbody id="data-pengajar">

                   </tbody>
                 </table>
               </div>
               <!-- /.card-body -->
               <div class="card-footer clearfix">
                 <div id="page"></div>
               </div>
             </div>
             <!-- /.card -->
           </div>
         <!-- /.row (main row) -->
       </div><!-- /.container-fluid -->
     </section>
       <!-- /.content -->
     </div>
     <!-- /.content-wrapper -->
     <script type="text/javascript">
     var pathname = window.location.pathname.split('/');
     var pageno = pathname[4];
     var base = <?php echo json_encode(base_url()); ?>;
    getData();
 function getData(query=""){
     $.ajax({
         type:'POST',
         data:'keyword='+query+'&startpage='+pageno,
         url:'<?= base_url();?>admin/pengajar/table',
         dataType: 'json',
         success: function(data){
             var row = '';
             // table
             for(var i=0;i<data.result.length;i++){
                 row += '<tr>'+
                   '<td>'+data.result[i].id_pengajar+'</td>'+
                   '<td>'+data.result[i].nama_pengajar+'</td>'+
                   '<td>'+data.result[i].deskripsi_pengajar.slice(0, 20) + (data.result[i].deskripsi_pengajar.length > 20 ? "..." : "")+'</td>'+
                   '<td>'+data.result[i].prestasi_pengajar.slice(0, 20) + (data.result[i].prestasi_pengajar.length > 20 ? "..." : "")+'</td>'+
                   '<td>'+'<a href="<?= base_url() ?>admin/pengajar/detail/pjr'+data.result[i].id_pengajar+'" class="btn btn-primary btn-block" >See Details</a>'+
                   '</td>'+
                 '</tr>'
             }

             $('#data-pengajar').html(row)
             if(query!=""){var html = '<div></div>'}else{var html = '<div><?= $this->pagination->create_links(); ?></div>'}
             $('#page').html(html)
         }
     })
 }


 $('#search-text').keyup(function(){
        var search = $(this).val();
    if(search != "")
    {
    getData(search)
    }
    else
    {
    getData();
    }
    });


     </script>
