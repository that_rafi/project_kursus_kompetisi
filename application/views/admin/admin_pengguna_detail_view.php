<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <div class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1 class="m-0 text-dark"><?= $nama ?>'s Detail</h1>
        </div><!-- /.col -->
      </div><!-- /.row -->
    </div><!-- /.container-fluid -->
  </div>
  <!-- /.content-header -->

  <!-- Main content -->
  <section class="content">
  <div class="container-fluid">
    <!-- Small boxes (Stat box) -->
    <div class="row">
      <div class="col-12">
        <div class="card">
              <div class="card-header">
                <div class="container" style="width:7%;float:right">
                <button type="button" id="btndel" data-toggle="modal" data-target="#modal-danger-delete" class="btn btn-outline-danger"><i class="fas fa-trash"></i></button>
                </div>
              </div><!-- /.card-header -->
              <div class="card-body">
                <div class="tab-content">
                  <!-- /.tab-pane -->
                  <div class="tab-pane active" id="settings">
                    <form class="form-horizontal" action="<?= base_url() ?>admin/pengguna/detail" method="post" >
                      <div class="form-group row">
                        <input type="hidden" class="form-control" name="id_pengguna" id="inputid" value="<?= $id_pengguna; ?>">
                        <label for="inputName" class="col-sm-2 col-form-label">Nama</label>
                        <div class="col-sm-10">
                          <input type="text" class="form-control" name="nama" id="inputName" value="<?= $nama; ?>" placeholder="Name">
                          <?= form_error('nama','<small class="text-danger pl-3">','</small>'); ?>
                        </div>
                      </div>
                      <div class="form-group row">
                        <label for="inputEmail" class="col-sm-2 col-form-label">Email</label>
                        <div class="col-sm-10">
                          <input type="email" class="form-control" name="email" value="<?= $email; ?>" id="inputEmail" placeholder="Email">
                          <?= form_error('email','<small class="text-danger pl-3">','</small>'); ?>
                        </div>
                      </div>
                      <div class="form-group row">
                        <label for="inputName2" class="col-sm-2 col-form-label">Tanggal Lahir</label>
                        <div class="col-sm-10">
                          <input type="date" class="form-control" name="tgl_lahir" value="<?= $tgl_lahir; ?>" >
                          <?= form_error('tgl_lahir','<small class="text-danger pl-3">','</small>'); ?>
                        </div>
                      </div>
                      <div class="form-group row">
                        <label for="inputExperience" class="col-sm-2 col-form-label">Alamat</label>
                        <div class="col-sm-10">
                          <textarea class="form-control" name="alamat" value="<?= $alamat; ?>" id="inputExperience" placeholder="Alamat"><?= $alamat; ?></textarea>
                        </div>
                      </div>
                      <div class="form-group row">
                        <label for="inputtbadan" class="col-sm-2 col-form-label">Tinggi Badan</label>
                        <div class="col-sm-10">
                          <input type="number" class="form-control" name="t_badan" value="<?= $t_badan; ?>" id="inputSkills" placeholder="Tinggi Badan">
                          <?= form_error('t_badan','<small class="text-danger pl-3">','</small>'); ?>
                        </div>
                      </div>
                      <div class="form-group row">
                        <label for="inputbbadan" class="col-sm-2 col-form-label">Berat Badan</label>
                        <div class="col-sm-10">
                          <input type="number" class="form-control" name="b_badan" value="<?= $b_badan; ?>" id="inputSkills" placeholder="Berat Badan">
                          <?= form_error('b_badan','<small class="text-danger pl-3">','</small>'); ?>
                        </div>
                      </div>
                      <div class="form-group row">
                        <label for="inputnotelp" class="col-sm-2 col-form-label">No Telp</label>
                        <div class="col-sm-10">
                          <input type="text" class="form-control" name="no_tlp" value="<?= $no_telp; ?>" id="inputSkills" placeholder="No Telp">
                            <?= form_error('no_tlp','<small class="text-danger pl-3">','</small>'); ?>
                        </div>
                      </div>
                      <div class="form-group row">
                        <label for="inputPrestasi" class="col-sm-2 col-form-label">Prestasi</label>
                        <div class="col-sm-10">
                          <textarea class="form-control" name="prestasi" id="inputPrestasi" placeholder="Prestasi"><?= $prestasi; ?></textarea>
                        </div>
                      </div>
                      <div class="form-group row">
                        <div class="offset-sm-2 col-sm-10">
                          <input type="submit" value="Simpan" class="btn btn-info"></button>
                        </div>
                      </div>
                    </form>
                  </div>
                  <!-- /.tab-pane -->
                </div>
                <!-- /.tab-content -->
              </div><!-- /.card-body -->
            </div>
        <!-- /.card -->
      </div>
    <!-- /.row (main row) -->
  </div><!-- /.container-fluid -->
</section>
  <!-- /.content -->
</div>
<!-- modal delete -->
<div class="modal fade" id="modal-danger-delete" style="display: none;" aria-hidden="true">
        <div class="modal-dialog">
          <div class="modal-content bg-danger">
            <div class="modal-header">
              <h4 class="modal-title">Confirmation</h4>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">×</span>
              </button>
            </div>
            <div class="modal-body">
              <p>Are you sure you want to delete this data?…</p>
            </div>
            <div class="modal-footer justify-content-between">
              <button type="button" class="btn btn-outline-light" data-dismiss="modal">No</button>
              <button type="button" class="btn btn-outline-light" onclick="redirectDelete(<?= $id_pengguna ?>)" id="yesbtn">Yes</button>
            </div>
          </div>
          <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
      </div>

<script type="text/javascript">
var base = <?php echo json_encode(base_url()); ?>;
  function redirectDelete(id){
    $.redirect(base+'admin/pengguna/detele', {
      'id_pengguna': id
    });
  }
</script>
