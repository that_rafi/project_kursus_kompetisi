<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <div class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1 class="m-0 text-dark"><?= $kelas['nama_kelas'] ?>'s Detail</h1>
        </div><!-- /.col -->
      </div><!-- /.row -->
    </div><!-- /.container-fluid -->
  </div>
  <!-- /.content-header -->

  <!-- Main content -->
  <section class="content">
  <div class="container-fluid">
    <!-- Small boxes (Stat box) -->
    <div class="row">
      <div class="col-12">
        <div class="card">
              <div class="card-header">
                <div class="container" style="width:7%;float:right">
                <button type="button" id="btndel" data-toggle="modal" data-target="#modal-danger-delete" class="btn btn-outline-danger"><i class="fas fa-trash"></i></button>
                </div>
              </div><!-- /.card-header -->
              <div class="card-body">
                <div class="tab-content">
                  <!-- /.tab-pane -->
                  <?= $this->session->flashdata('message'); ?>
                  <?= $this->session->flashdata('error'); ?>
                  <div class="tab-pane active" id="settings">
                    <form class="form-horizontal" action="<?= base_url() ?>admin/kelas/detail/kls<?= $kelas['id_kelas'] ?>" method="post" >
                      <div class="form-group row">
                        <input type="hidden" class="form-control" name="id_kelas" id="inputid" value="<?= $kelas['id_kelas'] ?>">
                        <label for="inputName" class="col-sm-2 col-form-label">Nama Kelas</label>
                        <div class="col-sm-10">
                          <input type="text" class="form-control" name="nama_kelas" id="inputName" value="<?= $kelas['nama_kelas']; ?>" placeholder="Nama kelas">
                          <?= form_error('nama_kelas','<small class="text-danger pl-3">','</small>'); ?>
                        </div>
                      </div>
                      <div class="form-group row">
                        <input type="hidden" class="form-control" name="id_kursus_p" id="inputidkursus" value="<?= $kelas['id_kursus_p'] ?>">
                        <label for="inputCB" class="col-sm-2 col-form-label">Kursus</label>
                        <div class="col-sm-10">
                          <input type="text" class="form-control" name="nama_kursus" value="<?= $kelas['nama_kursus']; ?>" id="inputNama" placeholder="Nama Kursus" readonly>
                        </div>
                      </div>
                      <div class="form-group row">
                        <label for="inputDeskripsi" class="col-sm-2 col-form-label">Deskripsi</label>
                        <div class="col-sm-10">
                          <textarea class="form-control" name="deskripsi_kelas" id="inputDeskripsi" placeholder="Deskripsi.."><?= $kelas['deskripsi_kelas']; ?></textarea>
                          <?= form_error('deskripsi_kelas','<small class="text-danger pl-3">','</small>'); ?>
                        </div>
                      </div>
                      <div class="form-group row">
                        <input type="hidden" class="form-control" name="id_pengajar" id="inputidpengajar" value="<?= $kelas['id_pengajar'] ?>">
                        <label for="inputCB" class="col-sm-2 col-form-label">Pengajar</label>
                        <div class="col-sm-10">
                          <input type="text" class="form-control" name="nama_pengajar" value="<?= $kelas['nama_pengajar']; ?>" id="inputNama" placeholder="Nama Pengajar" readonly>
                        </div>
                      </div>
                      <div class="form-group row">
                        <label for="inputBiaya" class="col-sm-2 col-form-label">Biaya</label>
                        <div class="col-sm-10">
                          <input type="text" class="form-control" name="biaya" value="<?= $kelas['biaya'];  ?>" id="inputbiaya" placeholder="biaya">
                            <?= form_error('biaya','<small class="text-danger pl-3">','</small>'); ?>
                        </div>
                      </div>
                      <div class="form-group row">
                        <label for="inputBiaya" class="col-sm-2 col-form-label">Jadwal Latihan</label>
                        <div class="col-sm-10">
                          <input type="text" class="form-control" name="jadwal_latihan" value="<?= $kelas['jadwal_latihan'];  ?>" id="inputjadwal" placeholder="Jadwal Latihan">
                            <?= form_error('jadwal_latihan','<small class="text-danger pl-3">','</small>'); ?>
                        </div>
                      </div>
                      <div class="form-group row">
                        <label for="inputBiaya" class="col-sm-2 col-form-label">Jangka Waktu</label>
                        <div class="col-sm-10">
                          <input type="text" class="form-control" name="jangka_wkt" value="<?= $kelas['jangka_wkt'];  ?>" id="inputjangka" placeholder="Jangka Waktu">
                            <?= form_error('jangka_wkt','<small class="text-danger pl-3">','</small>'); ?>
                        </div>
                      </div>
                      <div class="form-group row">
                        <label for="inputBiaya" class="col-sm-2 col-form-label">Link Gform</label>
                        <div class="col-sm-10">
                          <input type="text" class="form-control" name="link" value="<?= $kelas['link'];  ?>" id="inputlink" placeholder="Link">
                            <?= form_error('link','<small class="text-danger pl-3">','</small>'); ?>
                        </div>
                      </div>
                      <div class="form-group row">
                        <label for="inputKontak" class="col-sm-2 col-form-label">Kontak</label>
                        <div class="col-sm-10">
                          <input type="text" class="form-control" name="kontak_kelas" value="<?= $kelas['kontak_kelas'] ?>" id="inputkontak" placeholder="+6289XXXXXXX">
                            <?= form_error('kontak_kelas','<small class="text-danger pl-3">','</small>'); ?>
                        </div>
                      </div>
                      <div class="form-group row">
                        <label for="inputstatus" class="col-sm-2 col-form-label">Status</label>
                        <div class="col-sm-10">
                          <select name="status_kelas" id="inputstatus_kelas" class="custom-select">
                              <?php for($i=0;$i<count($status);$i++) {
                                  if($status[$i] == $kelas['status_kelas'] ){ ?>
                                    <option value="<?= $kelas['status_kelas'] ?>" selected><?= $kelas['status_kelas'] ?></option>
                                  <?php }else{ ?>
                                    <option value="<?= $status[$i] ?>" ><?= $status[$i] ?></option>
                              <?php } } ?>
                          </select>
                            <?= form_error('status_kelas','<small class="text-danger pl-3">','</small>'); ?>
                        </div>
                      </div>
                      <div class="form-group row">
                        <label for="inputDate" class="col-sm-2 col-form-label">Tanggal Upload</label>
                        <div class="col-sm-10">
                          <input type="text" class="form-control" name="createdOn" value="<?= $kelas['createdOn'];  ?>" readonly id="inputdate" placeholder="createdOn">
                            <?= form_error('createdOn','<small class="text-danger pl-3">','</small>'); ?>
                        </div>
                      </div>
                      <div class="form-group row">
                        <label for="inputUpdate" class="col-sm-2 col-form-label">Last Update</label>
                        <div class="col-sm-10">
                          <input type="text" class="form-control" name="updatedOn" value="<?= $kelas['updatedOn'];  ?>" readonly id="inputupdate" placeholder="updatedOn">
                            <?= form_error('updatedOn','<small class="text-danger pl-3">','</small>'); ?>
                        </div>
                      </div>
                      <div class="form-group row">
                        <div class="offset-sm-2 col-sm-10">
                          <input type="submit" value="Simpan" class="btn btn-info"></button>
                        </div>
                      </div>
                    </form>
                  </div>
                  <!-- /.tab-pane -->
                </div>
                <!-- /.tab-content -->
              </div><!-- /.card-body -->
            </div>
        <!-- /.card -->
      </div>
      <div class="col-12">
        <div class="card">
              <div class="card-header p-2">
                <div class="container">
                <h4> Galeri </h4>
                </div>
              </div><!-- /.card-header -->
              <div class="card-body">
                <div class="tab-content">
                  <div class="row mb-3">
                    <?php if($countres > 0 ){ ?>
                        <!-- <div class="col-sm-6">
                          <img class="img-fluid" src="<?= base_url() ?>assets/upload/default/img_tumbnail.png" alt="Photo">
                        </div> -->
                        <!-- /.col -->
                        <div class="col-sm-12">
                          <div class="row">
                            <?php foreach ($resource as $res): ?>
                              <div class="col-sm-2">
                                 <a target="_blank" href="<?= base_url() ?>assets/upload/resource_kelas/<?= $res['filename'] ?>">
                                <img class="img-fluid mb-3" src="<?= base_url() ?>assets/upload/resource_kelas/<?= $res['filename'] ?>"  alt="<?= $res['filename']  ?>">
                              </a>
                              </div>
                            <?php endforeach; ?>
                            <!-- /.col -->
                          </div>
                          <!-- /.row -->
                        </div>
                        <!-- /.col -->
                      </div>
                    <?php } else { ?>
                        <h5>Tidak ada galeri</h5>
                    <?php } ?>
                  <!-- /.tab-pane -->
                </div>
                <!-- /.tab-content -->
              </div><!-- /.card-body -->
            </div>
      </div>
    <!-- /.row (main row) -->
  </div><!-- /.container-fluid -->
</section>
  <!-- /.content -->
</div>
<!-- modal delete -->
<div class="modal fade" id="modal-danger-delete" style="display: none;" aria-hidden="true">
        <div class="modal-dialog">
          <div class="modal-content bg-danger">
            <div class="modal-header">
              <h4 class="modal-title">Confirmation</h4>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">×</span>
              </button>
            </div>
            <div class="modal-body">
              <p>Are you sure you want to delete this data?…</p>
            </div>
            <div class="modal-footer justify-content-between">
              <button type="button" class="btn btn-outline-light" data-dismiss="modal">No</button>
              <a href="<?= base_url() ?>admin/kelas/delete/kls<?= $kelas['id_kelas'] ?>" class="btn btn-outline-light" id="yesbtn">Yes</a>
            </div>
          </div>
          <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
      </div>

<script type="text/javascript">
var base = <?php echo json_encode(base_url()); ?>;

</script>
