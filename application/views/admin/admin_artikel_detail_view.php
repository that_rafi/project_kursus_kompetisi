
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <div class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1 class="m-0 text-dark"><?= $artikel['judul'] ?> Artikel</h1>
        </div><!-- /.col -->
      </div><!-- /.row -->
    </div><!-- /.container-fluid -->
  </div>
  <!-- /.content-header -->

  <!-- Main content -->
  <section class="content">
  <div class="container-fluid">
    <!-- Small boxes (Stat box) -->
    <div class="row">
      <div class="col-12">
        <div class="card">
              <!-- <div class="card-header">
              </div>-->
              <div class="card-body">
                <div class="tab-content">
                  <!-- /.tab-pane -->
                  <?= $this->session->flashdata('message'); ?>
                  <?= $this->session->flashdata('error'); ?>
                  <div class="tab-pane active" id="settings">
                    <form class="form-horizontal" action="<?= base_url() ?>admin/artikel/detail/art<?= $artikel['id_artikel'] ?>" method="post" >
                      <div class="form-group row">
                        <input type="hidden" class="form-control" name="id_artikel" id="inputid" value="<?= $artikel['id_artikel']; ?>">
                        <label for="inputName" class="col-sm-2 col-form-label">Judul</label>
                        <div class="col-sm-10">
                          <input type="text" class="form-control" name="judul" id="inputJudul" value="<?= $artikel['judul']; ?>" placeholder="Judul">
                          <?= form_error('judul','<small class="text-danger pl-3">','</small>'); ?>
                        </div>
                      </div>
                      <div class="form-group row">
                        <label for="inputIsi" class="col-sm-2 col-form-label">Isi</label>
                        <div class="col-sm-10">
                          <textarea class="form-control" style="white-space:none" name="isi" id="inputIsi" ><?= $artikel['isi']; ?></textarea>
                          <?= form_error('isi','<small class="text-danger pl-3">','</small>'); ?>
                        </div>
                      </div>
                      <div class="form-group row">
                        <label for="inputTanggal" class="col-sm-2 col-form-label">Tanggal</label>
                        <div class="col-sm-10">
                          <input type="date" class="form-control" name="createdOn" value="<?= $artikel['createdOn']; ?>" >
                          <?= form_error('createdOn','<small class="text-danger pl-3">','</small>'); ?>
                        </div>
                      </div>
                      <div class="form-group row">
                        <div class="offset-sm-2 col-sm-10">
                          <button type="submit" class="btn btn-info"><i class="fas fa-save"></i></button>
                          <button type="button" id="btndel" data-toggle="modal" data-target="#modal-danger-delete" class="btn btn-danger"><i class="fas fa-trash"></i></button>
                        </div>
                      </div>
                    </form>
                  </div>
                  <!-- /.tab-pane -->
                </div>
                <!-- /.tab-content -->
              </div><!-- /.card-body -->
            </div>
        <!-- /.card -->
      </div>
      <!-- informasi tambahan -->
      <style>
        .dropzone{
          width: 300px;
          height: 300px;
          padding: 10px;
          display: flex;
          justify-content: center;
          align-items: center;
          border-radius: 10px;
          border: 2px dashed rgb(204, 204, 204);
          color: rgb(204, 204, 204);
          text-align: center;
          -webkit-box-align: center;
          -webkit-box-pack: center;
          z-index: 100;
          cursor: default;
        }
        .dropzone.dragover{
          border-color: #000;
          color: #000;
        }
        .dropzone_thumb{
          width: 100%;
          height: 100%;
          border-radius: 10px;
          background-image: url('<?= base_url() ?>assets/upload/default/img_tumbnail.png');
          background-size: contain;
          background-repeat: no-repeat;
          position: relative;
          overflow: hidden;
        }
        .dropzone_thumb::after{
          content: attr(data-label);
          position: absolute;
          bottom: 0;
          left: 0;
          width: 100%;
          padding: 5px 0;
          color: #ffffff;
          background-color: grey;
          font-size: 12px;
          text-align: center;
        }
        .dropzone_input{
          display: none;
        }
      </style>
      <div class="col-12">
        <div class="card">
              <div class="card-header p-2">
                <div class="container">
                <h4> Informasi Tambahan </h4>
                </div>
              </div><!-- /.card-header -->
              <div class="card-body">
                <div class="tab-content">
                        <form class="form-horizontal">
                      <div class="form-group row">
                        <div class="col-lg-6" style="justify-content:center;align-items:center;display:flex">
                          <div class="dropzone" >
                          <label for="browse" class="dropzone_msg">Drop files here</label>
                          <input type="file" id="browse" name="browse" class="dropzone_input">
                          <!-- <div class="dropzone_thumb" data-label="mytext.txt"></div> -->
                          </div>
                        </div>
                        <div class="col-lg-6">
                          <div class="col-lg-12">
                          <textarea class="form-control" id="inputKeterangan" placeholder="Keterangan"></textarea>
                          </div>
                          <div class="col-lg-12">
                            <button id="addinforow" style="margin-top:20px" class="btn btn-success"><i class="fas fa-plus"></i></a>
                          </div>
                        </div>
                      </div>
                        </form>
                      <div class="form-group row" id="info1">
                        <div class="card-body table-responsive p-0">
                          <?= $this->session->flashdata('message'); ?>
                          <?= $this->session->flashdata('error'); ?>
                          <table class="table table-hover text-nowrap">
                            <thead>
                              <tr>
                                <th>Foto</th>
                                <th>Keterangan</th>
                                <th>Action</th>
                              </tr>
                            </thead>
                            <tbody id="listinfo">
                              <?php $count=0; foreach($resource as $res) : ?>
                                <tr id="listcount<?= $count ?>">
                                  <td><img src='<?= base_url() ?>assets/upload/training_mandiri/<?= $res['filename'] ?>' width="30px" height="30px"></img></td>
                                  <td><?= $res['keterangan'] ?></td>
                                  <td><button id="minusinforow<?= $res['id_resource'] ?>" onclick="event.preventDefault(); minusinforow(<?= $res['id_resource'] ?>,<?= $count ?>);" class="btn btn-warning"><i class="fas fa-minus"></i></a></td></td>
                                </tr>
                              <?php $count++; endforeach; ?>
                            </tbody>
                          </table>
                        </div>
                      </div>


                  <!-- /.tab-pane -->
                </div>
                <!-- /.tab-content -->
              </div><!-- /.card-body -->
            </div>
      </div>
    <!-- /.row (main row) -->
  </div><!-- /.container-fluid -->
</section>
  <!-- /.content -->
</div>
<!-- modal delete -->
<div class="modal fade" id="modal-danger-delete" style="display: none;" aria-hidden="true">
        <div class="modal-dialog">
          <div class="modal-content bg-danger">
            <div class="modal-header">
              <h4 class="modal-title">Confirmation</h4>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">×</span>
              </button>
            </div>
            <div class="modal-body">
              <p>Are you sure you want to delete this data?…</p>
            </div>
            <div class="modal-footer justify-content-between">
              <button type="button" class="btn btn-outline-light" data-dismiss="modal">No</button>
              <a href="<?= base_url() ?>admin/artikel/delete/art<?= $artikel['id_artikel'] ?>" class="btn btn-outline-light" id="yesbtn">Yes</a>
            </div>
          </div>
          <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
      </div>

<script type="text/javascript">
var base = <?php echo json_encode(base_url()); ?>;
const max = <?php echo json_encode($max_artikel); ?>;
var formData = new FormData();
var tempfiles = "";
  $('#addinforow').on('click',function(e){
    e.preventDefault();
    var $div = $('tr[id^="listcount"]:last');
    if($div.length > 0 ){
        var num = parseInt( $div.prop("id").match(/\d+/g), 10 ) +1;
    }else{
        var num = 0;
    }

    var url = $('.dropzone_thumb').data('imgurl')
    var imgname = $('.dropzone_thumb').data('label')
    var keterangan = $('#inputKeterangan').val()
    if (num < max ){
      if( keterangan!= "" && url !=null){
        // data for form
        upload(tempfiles,keterangan,function(res){
           if(res.result.upload){
             var row = '  <tr id="listcount'+num+'">'+
                       '<td><img src='+url+' width="30px" height="30px"></img></td>'+
                       '<td>'+keterangan+'</td>'+
                       '<td><button id="minusinforow'+res.result.newid+'" onclick="event.preventDefault(); minusinforow('+res.result.newid+','+num+');" class="btn btn-warning"><i class="fas fa-minus"></i></a></td></tr>'
             $('#listinfo').append(row)
           }
        })

      }else{
        alert('Tolong masukan informasi yang lengkap')
      }
    }else{
      alert('anda sudah mencapai maksimal!')
    }

  })

  function minusinforow(id,index){
    $.ajax({
      url : '<?= base_url(); ?>admin/artikel/deleteRes',
      type : 'POST',
      dataType : 'json',
      data : 'id_resource='+id,
      success : function(res){
        if(res.result){
          $('#listcount'+index).remove()
        }
      }
    })
  }

  function upload(file,keterangan,calback){
    formData.append('file',file)
    formData.append('keterangan',keterangan)
    formData.append('id_artikel',<?= $artikel['id_artikel'] ?>)
    $.ajax({
      url : '<?= base_url(); ?>admin/artikel/uploadRes',
      type : 'POST',
      dataType : 'json',
      processData: false,
      contentType: false,
      data : formData,
      success : function(res){
        calback(res)
      }
    })
  }

  function setImg(dropzone,files){
    if(files.type.startsWith("image/")){
    var thumbnail = $('.dropzone_thumb').data('label')
    dropzone.attr('class','dropzone')
    if(!thumbnail){
      $('.dropzone_msg').hide()
      var thumb = $('<div/>').addClass('dropzone_thumb');
      dropzone.append(thumb)
    }
    // data for form
    tempfiles = files
    $('.dropzone_thumb').attr('data-label',files.name) // set img name
      var reader = new FileReader();
      reader.readAsDataURL(files);
      reader.onload = function(event) {
            $('.dropzone_thumb').attr('style','background-image: url("'+event.target.result+'")')
            $('.dropzone_thumb').data('imgurl',event.target.result) // hold img url
      };
    }else{
      alert('Please input image only!')
    }
  }

  jQuery(function($){
    $('.dropzone').on('click', function(){
        $('.dropzone_input')[0].click();
    });
  });

    $('.dropzone_input').on('change',function(e){
      $('.dropzone').attr('class','dropzone')
      setImg($('.dropzone'),e.target.files[0])
    })

    $('.dropzone').on('drop',function(e){
      e.preventDefault();
      $(this).attr('class','dropzone')
      setImg($(this),e.originalEvent.dataTransfer.files[0])
    })
    $('.dropzone').on('dragover',function(){
      $(this).attr('class','dropzone dragover')
      return false
    })
    $('.dropzone').on('dragleave',function(){
      $(this).attr('class','dropzone')
      return false
    })

    function alertCall($type,$msg,$target){
      if($type == 'alert'){
        alt= '<div class="alert alert-danger alert-dismissible">'+
                      '<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>'+
                      '<h5><i class="icon fas fa-ban"></i> Alert!</h5>'+$msg+
                    '</div>'
      }else if($type == 'success'){
        alt= '<div class="alert alert-success alert-dismissible">'+
                      '<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>'+
                      '<h5><i class="icon fas fa-check"></i> Success!</h5>'+$msg+
                    '</div>'
      }
      $($target).html(alt)
    }
</script>
