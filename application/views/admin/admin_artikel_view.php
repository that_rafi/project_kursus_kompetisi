



     <!-- Content Wrapper. Contains page content -->
     <div class="content-wrapper">
       <!-- Content Header (Page header) -->
       <div class="content-header">
         <div class="container-fluid">
           <div class="row mb-2">
             <div class="col-sm-6">
               <h1 class="m-0 text-dark">Data Artikel</h1>
             </div><!-- /.col -->
           </div><!-- /.row -->
         </div><!-- /.container-fluid -->
       </div>
       <!-- /.content-header -->

       <!-- Main content -->
       <section class="content">
       <div class="container-fluid">
         <!-- Small boxes (Stat box) -->
         <div class="row">
           <div class="col-12">
             <div id="actionmsg"></div>
             <div class="card">
               <div class="card-header">
                 <h3 class="card-title"><a href="<?= base_url() ?>admin/artikel/detail/tambah" class="btn btn-success"><i class="fas fa-plus"></i></a></h3>
                 <div class="card-tools">
                   <div class="input-group input-group-sm" style="width: 150px;">
                     <input type="text" name="keyword" id="search-text" class="form-control float-right" placeholder="Search" >
                     <div class="input-group-append">
                       <button type="submit" onclick="searchdata()" class="btn btn-default"><i class="fas fa-search"></i></button>
                     </div>
                   </div>
                 </div>
               </div>

               <!-- /.card-header -->
               <div class="card-body table-responsive p-0">
                 <?= $this->session->flashdata('message'); ?>
                 <?= $this->session->flashdata('error'); ?>
                 <table class="table table-hover text-nowrap">
                   <thead>
                     <tr>
                       <th>No</th>
                       <th>Judul</th>
                       <th>Isi</th>
                       <th>Tanggal</th>
                       <th>Action</th>
                     </tr>
                   </thead>
                   <tbody id="data-artikel">

                   </tbody>
                 </table>
               </div>
               <!-- /.card-body -->
               <div class="card-footer clearfix">
                 <div id="page"></div>
               </div>
             </div>
             <!-- /.card -->
           </div>
         <!-- /.row (main row) -->
       </div><!-- /.container-fluid -->
     </section>
       <!-- /.content -->
     </div>
     <!-- /.content-wrapper -->
     <script type="text/javascript">
     var pathname = window.location.pathname.split('/');
     var pageno = pathname[4];
     var base = <?php echo json_encode(base_url()); ?>;
    getData();
 function getData(query=""){
     $.ajax({
         type:'POST',
         data:'keyword='+query+'&startpage='+pageno,
         url:'<?= base_url();?>admin/artikel/table',
         dataType: 'json',
         success: function(data){
           //console.log(data)
             var row = '';
             // table
             for(var i=0;i<data.result.length;i++){
                 row += '<tr>'+
                   '<td>'+data.result[i].id_artikel+'</td>'+
                   '<td>'+data.result[i].judul+'</td>'+
                   '<td>'+data.result[i].isi.slice(0, 20) + (data.result[i].isi.length > 20 ? "..." : "")+'</td>'+
                   '<td>'+data.result[i].createdOn+'</td>'+
                   '<td>'+'<a href="<?= base_url() ?>admin/artikel/detail/art'+data.result[i].id_artikel+'" class="btn btn-primary btn-block" >See Details</a>'+
                   '</td>'+
                 '</tr>'
             }

             $('#data-artikel').html(row)
             if(query!=""){var html = '<div></div>'}else{var html = '<div><?= $this->pagination->create_links(); ?></div>'}
             $('#page').html(html)
         }
     })
 }


 $('#search-text').keyup(function(){
        var search = $(this).val();
    if(search != "")
    {
    getData(search)
    }
    else
    {
    getData();
    }
    });


     </script>
