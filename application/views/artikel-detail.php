<!-- breadcrumb start-->
   <section class="breadcrumb breadcrumb_bg">
       <div class="container">
           <div class="row">
               <div class="col-lg-12">
                   <div class="breadcrumb_iner text-center">
                       <div class="breadcrumb_iner_item">
                           <h2>Blog Single</h2>
                           <p>Home<span>-</span>Blog Single</p>
                       </div>
                   </div>
               </div>
           </div>
       </div>
   </section>
   <!-- breadcrumb start-->
  <!--================Blog Area =================-->
  <section class="blog_area single-post-area section_padding">
     <div class="container">
        <div class="row">
           <div class="col-lg-8 posts-list">
              <div class="single-post">
                 <div class="feature-img">
                   <?php if($main_resource!=null) { ?>
                     <img class="img-fluid" style="max-height:600px" src="<?= base_url() ?>assets/upload/training_mandiri/<?= $main_resource['filename'] ?>" alt="">
                   <?php }else{ ?>
                    <img class="img-fluid" src="<?= base_url() ?>assets/raga/img/blog/single_blog_1.png" alt="">
                  <?php } ?>
                 </div>
                 <div class="blog_details">
                    <h2><?php echo $artikel['judul']; ?></h2>
                    <ul class="blog-info-link mt-3 mb-4">
                       <li><a><i class="far fa-calendar"></i> <?php echo $artikel['createdOn']; ?></a></li>
                       <!-- <li><a href="#"><i class="far fa-comments"></i> Author</a></li> -->
                    </ul>
                    <p class="excert">
                       <?php echo $artikel['isi']; ?>
                    </p>
                 </div>
                 <div class="blog_details">
                 <h2>Langkah-Langkah</h2>

                 <div class="panel panel-default" style="margin-top:-1px">
                      <div class="panel-body">
                         <?php foreach($resource as $r ): ?>
                           <h3><?php echo $r['keterangan']; ?></h3>
                          <img src="<?= base_url() ?>assets/upload/training_mandiri/<?= $r['filename'] ?>" alt="<?= $r['filename'] ?>" class="img-fluid" style="position:center;max-height:300px">
                        <?php endforeach; ?>
                      </div>
                 </div>

                 </div>
              </div>

           </div>
           <div class="col-lg-4">
              <div class="blog_right_sidebar">
                 <aside class="single_sidebar_widget search_widget">
                    <form action="<?= base_url() ?>artikel" method="get">
                       <div class="form-group">
                          <div class="input-group mb-3">
                             <input type="text" name="keyword" class="form-control" placeholder='Search Keyword'
                                onfocus="this.placeholder = ''" onblur="this.placeholder = 'Search Keyword'">
                          </div>
                       </div>
                       <button class="button rounded-0 primary-bg text-white w-100 btn_1" type="submit">Search</button>
                    </form>
                 </aside>
                 <!-- <aside class="single_sidebar_widget post_category_widget">
                    <h4 class="widget_title">Category</h4>
                    <ul class="list cat-list">
                       <li>
                          <a href="#" class="d-flex">
                             <p>Resaurant food</p>
                             <p>(37)</p>
                          </a>
                       </li>
                       <li>
                          <a href="#" class="d-flex">
                             <p>Travel news</p>
                             <p>(10)</p>
                          </a>
                       </li>
                       <li>
                          <a href="#" class="d-flex">
                             <p>Modern technology</p>
                             <p>(03)</p>
                          </a>
                       </li>
                       <li>
                          <a href="#" class="d-flex">
                             <p>Product</p>
                             <p>(11)</p>
                          </a>
                       </li>
                       <li>
                          <a href="#" class="d-flex">
                             <p>Inspiration</p>
                             <p>(21)</p>
                          </a>
                       </li>
                       <li>
                          <a href="#" class="d-flex">
                             <p>Health Care</p>
                             <p>(21)</p>
                          </a>
                       </li>
                    </ul>
                 </aside> -->
                 <aside class="single_sidebar_widget popular_post_widget">
                     <h3 class="widget_title">Recent Post</h3>
                       <?php if($artikel_all != null) { $count=0; foreach($artikel_all as $a) :?>
                     <div class="media post_item">
                         <?php if($thumbnail_all[$count] != null){ ?>
                             <img src="<?= base_url() ?>assets/upload/training_mandiri/<?= $thumbnail_all[$count]['filename'] ?>" height="80px" width="80px" alt="">
                         <?php }else{ ?>
                             <img src="<?= base_url() ?>assets/raga/img/post/post_1.png" alt="post">
                         <?php } ?>
                         <div class="media-body">
                             <a href="<?php echo base_url(); ?>artikel/detail/<?php echo $a['id_artikel']; ?>">
                                 <h3><?= $a['judul'] ?></h3>
                             </a>
                             <p><?= $a['createdOn'] ?></p>
                         </div>
                     </div>
                   <?php $count++; endforeach; } ?>

                 </aside>
                 <!-- <aside class="single_sidebar_widget tag_cloud_widget">
                    <h4 class="widget_title">Tag Clouds</h4>
                    <ul class="list">
                       <li>
                          <a href="#">project</a>
                       </li>
                       <li>
                          <a href="#">love</a>
                       </li>
                       <li>
                          <a href="#">technology</a>
                       </li>
                       <li>
                          <a href="#">travel</a>
                       </li>
                       <li>
                          <a href="#">restaurant</a>
                       </li>
                       <li>
                          <a href="#">life style</a>
                       </li>
                       <li>
                          <a href="#">design</a>
                       </li>
                       <li>
                          <a href="#">illustration</a>
                       </li>
                    </ul>
                 </aside> -->
              </div>
           </div>
        </div>
     </div>
  </section>
  <!--================Blog Area end =================-->
