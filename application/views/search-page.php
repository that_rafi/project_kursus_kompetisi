<section class="breadcrumb breadcrumb_bg">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="breadcrumb_iner text-center">
                    <div class="breadcrumb_iner_item">
                        <h2>Search Page</h2>
                        <p>Hasil Pencarian</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- breadcrumb start-->

<section class="cari" style="padding-top:50px; padding-bottom:30px;">
    <div class="container">
      <form action="<?= base_url() ?>search" method="get">
        <div class="row">
            <div class="col-lg-12">
                <div class="input-group-append" style="height:50px; color:#095816;">
                  <input class="form-control my-0 py-1 amber-border" type="text" name="keyword" placeholder="Search" aria-label="Search" style="height:50px;">
                  <span class="glyphicon form-control-feedback"></span>
                  <button type="submit"><i class="fa fa-search"
                    aria-hidden="true"></i></button>
                </div>
                <a data-toggle="collapse" href="#collapseExample" role="button" aria-expanded="false" aria-controls="collapseExample">
                  Add Filter
                </a>
                <?php if($sort != null || $tags != null) { ?>
                <div class="collapse show" id="collapseExample">
                <?php }else{ ?>
                  <div class="collapse" id="collapseExample">
                <?php } ?>
                  <div class="card card-body">
                    <div class="container" style="margin-top:10px">
                      <span><b>Order By :</b></span><br/>
                      <?php if($sort=="ASC"){ ?>
                        <input type="radio" name="sort" value="ASC" checked> A-Z </input>
                        <input type="radio" name="sort" value="DESC"> Z-A </input>
                      <?php }else if($sort=="DESC"){ ?>
                        <input type="radio" name="sort" value="ASC" > A-Z </input>
                        <input type="radio" name="sort" value="DESC" checked> Z-A </input>
                      <?php }else{ ?>
                        <input type="radio" name="sort" value="ASC" > A-Z </input>
                        <input type="radio" name="sort" value="DESC"> Z-A </input>
                      <?php } ?>
                    </div>
                    <div class="container">
                      <span><b>Tags :</b></span><br/>
                      <?php foreach ($cabang as $c): ?>
                        <?php if($tags == $c['nama_cabang']){ ?>
                        <input type="radio" name="tags" value="<?= $c['nama_cabang'] ?>" checked> <?= $c['nama_cabang'] ?> </input>
                      <?php }else{ ?>
                        <input type="radio" name="tags" value="<?= $c['nama_cabang'] ?>"> <?= $c['nama_cabang'] ?> </input>
                      <?php } ?>
                      <?php endforeach; ?>
                    </div>
                  </div>
                </div>
            </div>



        </div>
      </form>
    </div>
</section>
    <?php if($kelas != null) { ?>
      <div class="container" >
              <div class="container">
                  <h3>Hasil Pencarian Kelas :</h3>
              </div>
              <div class="row">
                  <div class="MultiCarousel" data-items="1,3,5,6" data-slide="1" id="MultiCarousel"  data-interval="1000">
                      <div class="MultiCarousel-inner">
                        <?php if($num_kelas > 0) { ?>
                          <?php $count=0; foreach($kelas as $dk) : ?>
                            <div class="item">
                                <div class="pad15">
                                    <p class="lead">
                                      <?php if($thumbnail[$count] != null){ ?>
                                      <img src="<?= base_url(); ?>assets/upload/resource_kelas/<?= $thumbnail[$count]['filename'] ?>" width="100px" height="100px">
                                      <?php }else{ ?>
                                      <img src="<?= base_url(); ?>assets/raga/img/camera.png" width="100px">
                                      <?php } ?>
                                    </p>
                                    <p><h4><?= $dk['nama_kelas'] ?></h4></p>
                                    <p> <a href="<?= base_url(); ?>kelas/detail/<?= $dk['id_kelas'] ?>">
                                      <button type="button" class="btn btn-success">Baca Selengkapnya</button>
                                    </a></p>
                                </div>
                            </div>
                          <?php $count++; endforeach; ?>
                        <?php }else{ ?>
                        <div class="item">
                            <div class="pad15">
                                <p class="lead"><img src="<?= base_url(); ?>assets/raga/img/camera.png" width="100px"></p>
                                <p>Tidak ada kelas</p>
                            </div>
                        </div>
                      <?php } ?>
                      </div>
                      <button class="btn btn-success leftLst"><</button>
                      <button class="btn btn-success rightLst">></button>
                  </div>
              </div>
           </div>

  <?php } if($tempat != null){ ?>
    <div class="container">
       <div class="container">
           <h3>Hasil Pencarian Tempat Olahraga :</h3>
       </div>
       <div class="row">
           <div class="MultiCarousel" data-items="1,3,5,6" data-slide="1" id="MultiCarousel"  data-interval="1000">
               <div class="MultiCarousel-inner">
                 <?php if($num_tempat > 0) { ?>
                   <?php $count=0; foreach($tempat as $t) : ?>
                     <div class="item">
                         <div class="pad15">
                             <p class="lead">
                               <?php if($thumbnail_tempat[$count] != null){ ?>
                               <img src="<?= base_url(); ?>assets/upload/resource_tempat/<?= $thumbnail_tempat[$count]['filename'] ?>" width="100px" height="100px">
                               <?php }else{ ?>
                               <img src="<?= base_url(); ?>assets/raga/img/camera.png" width="100px">
                               <?php } ?>
                             </p>
                             <p><h4><?= $t['nama_tempat'] ?></h4></p>
                             <p> <a href="<?= base_url(); ?>tempat/detail/<?= $t['id_tempat'] ?>">
                               <button type="button" class="btn btn-success">Baca Selengkapnya</button>
                             </a></p>
                         </div>
                     </div>
                   <?php $count++; endforeach; ?>
                 <?php }else{ ?>
                 <div class="item">
                     <div class="pad15">
                         <p class="lead"><img src="<?= base_url(); ?>assets/raga/img/camera.png" width="100px"></p>
                         <p>Tidak ada Tempat</p>
                     </div>
                 </div>
               <?php } ?>
               </div>
               <button class="btn btn-success leftLst"><</button>
               <button class="btn btn-success rightLst">></button>
           </div>
       </div>
    </div>

  <?php }else{ ?>

    <div class="container" style="min-height:500px;padding_top:10px">
      <h1>Tidak ada kelas dan tempat yang tersedia</h1>
    </div>

  <?php } ?>


<?php if($kelas!=null) {?>
    <div class="container"><?php echo $this->pagination->create_links(); ?></div>
  <?php } ?>
