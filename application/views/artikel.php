<section class="breadcrumb breadcrumb_bg">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="breadcrumb_iner text-center">
                    <div class="breadcrumb_iner_item">
                        <h2>Artikel</h2>
                        <p>Beranda<span>/</span>Artikel</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- breadcrumb start-->


<!--================Blog Area =================-->
<section class="blog_area section_padding">
    <div class="container">
        <div class="row">
            <div class="col-lg-8 mb-5 mb-lg-0">
                <div class="blog_left_sidebar">
                  <?php if($artikel != null) { $count=0; foreach($artikel as $a) :?>
                    <article class="blog_item">
                        <div class="blog_item_img">
                          <?php if($thumbnail[$count] != null){ ?>
                              <img class="card-img rounded-0" src="<?= base_url() ?>assets/upload/training_mandiri/<?= $thumbnail[$count]['filename'] ?>" height="570px" alt="">
                          <?php }else{ ?>
                              <img class="card-img rounded-0" src="<?= base_url() ?>assets/raga/img/blog/single_blog_1.png" alt="">
                          <?php } ?>
                            <a href="#" class="blog_item_date">
                                <p><?php echo $a['createdOn'];?></p>
                            </a>
                        </div>

                        <div class="blog_details">
                            <a class="d-inline-block" href="<?php echo base_url(); ?>artikel/detail/<?php echo $a['id_artikel']; ?>">
                                <h2><?php echo $a['judul']; ?></h2>
                            </a>
                            <p><?= substr($a['isi'], 0, 28) . '...'; ?></p>
                            <!-- <ul class="blog-info-link">
                                <li><a href="#"><i class="far fa-user"></i> Travel, Lifestyle</a></li>
                            </ul> -->
                        </div>
                    </article>
                  <?php $count++; endforeach; }else{ ?>
                      <div class="container">
                        <h1>Tidak Ada Artikel Tersedia</h1>
                      </div>
                  <?php } ?>

                  <?php if($artikel!=null){ ?>
                      <div class="container"><?php echo $this->pagination->create_links(); ?></div>
                  <?php } ?>



                    <!-- <nav class="blog-pagination justify-content-center d-flex">
                        <ul class="pagination">
                            <li class="page-item">
                                <a href="#" class="page-link" aria-label="Previous">
                                    <i class="ti-angle-left"></i>
                                </a>
                            </li>
                            <li class="page-item">
                                <a href="#" class="page-link">1</a>
                            </li>
                            <li class="page-item active">
                                <a href="#" class="page-link">2</a>
                            </li>
                            <li class="page-item">
                                <a href="#" class="page-link" aria-label="Next">
                                    <i class="ti-angle-right"></i>
                                </a>
                            </li>
                        </ul>
                    </nav> -->
                </div>
            </div>
            <div class="col-lg-4">
                <div class="blog_right_sidebar">
                    <aside class="single_sidebar_widget search_widget">
                        <form action="<?php echo base_url(); ?>artikel" method="get">
                           <div class="form-group">
                              <div class="input-group mb-3">
                                 <input type="text" class="form-control" placeholder='Search Keyword'
                                    onfocus="this.placeholder = ''" name="keyword" onblur="this.placeholder = 'Search Keyword'">
                              </div>
                           </div>
                           <button class="button rounded-0 primary-bg text-white w-100 btn_1" type="submit">Search</button>
                        </form>
                     </aside>

                    <!-- <aside class="single_sidebar_widget post_category_widget">
                        <h4 class="widget_title">Category</h4>
                        <ul class="list cat-list">
                            <li>
                                <a href="#" class="d-flex">
                                    <p>Resaurant food</p>
                                    <p>(37)</p>
                                </a>
                            </li>
                            <li>
                                <a href="#" class="d-flex">
                                    <p>Travel news</p>
                                    <p>(10)</p>
                                </a>
                            </li>
                            <li>
                                <a href="#" class="d-flex">
                                    <p>Modern technology</p>
                                    <p>(03)</p>
                                </a>
                            </li>
                            <li>
                                <a href="#" class="d-flex">
                                    <p>Product</p>
                                    <p>(11)</p>
                                </a>
                            </li>
                            <li>
                                <a href="#" class="d-flex">
                                    <p>Inspiration</p>
                                    <p>21</p>
                                </a>
                            </li>
                            <li>
                                <a href="#" class="d-flex">
                                    <p>Health Care (21)</p>
                                    <p>09</p>
                                </a>
                            </li>
                        </ul>
                    </aside> -->

                    <aside class="single_sidebar_widget popular_post_widget">
                        <h3 class="widget_title">Recent Post</h3>
                          <?php if($artikel_all != null) { $count=0; foreach($artikel_all as $a) :?>
                        <div class="media post_item">
                            <?php if($thumbnail_all[$count] != null){ ?>
                                <img src="<?= base_url() ?>assets/upload/training_mandiri/<?= $thumbnail_all[$count]['filename'] ?>" height="80px" width="80px" alt="">
                            <?php }else{ ?>
                                <img src="<?= base_url() ?>assets/raga/img/post/post_1.png" alt="post">
                            <?php } ?>
                            <div class="media-body">
                                <a href="<?php echo base_url(); ?>artikel/detail/<?php echo $a['id_artikel']; ?>">
                                    <h3><?= $a['judul'] ?></h3>
                                </a>
                                <p><?= $a['createdOn'] ?></p>
                            </div>
                        </div>
                      <?php $count++; endforeach; } ?>

                    </aside>
                    <!-- <aside class="single_sidebar_widget tag_cloud_widget">
                        <h4 class="widget_title">Tag Clouds</h4>
                        <ul class="list">
                            <li>
                                <a href="#">project</a>
                            </li>
                            <li>
                                <a href="#">love</a>
                            </li>
                            <li>
                                <a href="#">technology</a>
                            </li>
                            <li>
                                <a href="#">travel</a>
                            </li>
                            <li>
                                <a href="#">restaurant</a>
                            </li>
                            <li>
                                <a href="#">life style</a>
                            </li>
                            <li>
                                <a href="#">design</a>
                            </li>
                            <li>
                                <a href="#">illustration</a>
                            </li>
                        </ul>
                    </aside> -->


                    <!-- <aside class="single_sidebar_widget instagram_feeds">
                        <h4 class="widget_title">Instagram Feeds</h4>
                        <ul class="instagram_row flex-wrap">
                            <li>
                                <a href="#">
                                    <img class="img-fluid" src="<?= base_url() ?>assets/raga/img/post/post_5.png" alt="">
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                    <img class="img-fluid" src="<?= base_url() ?>assets/raga/img/post/post_6.png" alt="">
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                    <img class="img-fluid" src="<?= base_url() ?>assets/raga/img/post/post_7.png" alt="">
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                    <img class="img-fluid" src="<?= base_url() ?>assets/raga/img/post/post_8.png" alt="">
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                    <img class="img-fluid" src="<?= base_url() ?>assets/raga/img/post/post_9.png" alt="">
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                    <img class="img-fluid" src="<?= base_url() ?>assets/raga/img/post/post_10.png" alt="">
                                </a>
                            </li>
                        </ul>
                    </aside> -->


                    <!-- <aside class="single_sidebar_widget newsletter_widget">
                        <h4 class="widget_title">Newsletter</h4>

                        <form action="#">
                            <div class="form-group">
                                <input type="email" class="form-control" onfocus="this.placeholder = ''"
                                    onblur="this.placeholder = 'Enter email'" placeholder='Enter email' required>
                            </div>
                            <button class="button rounded-0 primary-bg text-white w-100 btn_1"
                                type="submit">Subscribe</button>
                        </form>
                    </aside> -->
                </div>
            </div>
        </div>
    </div>
</section>
<!--================Blog Area =================-->
