<div class="container" style="margin-top:50px">


            <form class="form-horizontal" method="post" action="<?= base_url() ?>profile" enctype="multipart/form-data">

              <div class="row align-items-center">
                  <div class="col-lg-12">
                      <h2>Profil Kamu</h2>
                      <?= $this->session->flashdata('message'); ?>
                      <?= $this->session->flashdata('error'); ?>
                      <h3>Informasi Akun</h3>
                      <hr>
                      <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                              <!--{GLOBAL_MESSAGES_aae3749ba9c2e308ffa9c240ac185959}-->
                              <!--/{GLOBAL_MESSAGES_aae3749ba9c2e308ffa9c240ac185959}-->

                              <script type="text/javascript" src="/js/sinaprinting/jquery.uploadfile.min.js"></script>

                                <div class="uploader_form">
                                  <input type="hidden" name="id" value="291">
                                  <input type="hidden" name="productid" value="210">
                                  <input type="hidden" name="sides" value="1">
                                  <input type="hidden" name="templatesize" value="77">
                                  <input type="hidden" name="product" value="18">
                                  <input type="hidden" id="oneSideOrTwoSides" name="oneSideOrTwoSides" value="1">
                                  <div class="fieldset">
                                    <ul style="list-style: none;">
                                      <li style="padding: 10px;">
                                        <div class="button-set">
                                          <?php if($pengguna['foto']==null || $pengguna['foto']=="" || $pengguna['foto']=="default.png"){ ?>
                                          <div class="ajax-upload-dragdrop upload-drag-and-drop-box" id="imgbox" >
                                            <div id="imgplaceholder">
                                            <img src="https://res.cloudinary.com/dtutqsucw/image/upload/v1438955603/file-upload-01.png" alt="upload-file" />
                                            </br>
                                            <p class="file-type-desc">( <font color="red">*</font>file extension allowed: <strong>.jpg )</p>
                                            </div>
                                            <br/><br/>
                                            <input type="file" id="inputpic" name="file" class="browse-button" style="width: 280px;position: relative; overflow: hidden; cursor: default;" ></input>
                                            </div>
                                             <?php }else{ ?>
                                               <?php if(strpos($pengguna['foto'], "https://") !== false)  { ?>
                                               <div class="ajax-upload-dragdrop upload-drag-and-drop-box" id="imgbox"  style="background-image:url('<?= $pengguna['foto']; ?>');background-size: cover;">
                                               <?php }else {?>
                                                 <div class="ajax-upload-dragdrop upload-drag-and-drop-box" id="imgbox" style="background-image:url('./assets/upload/pengguna/<?= $pengguna['foto']; ?>');background-size: cover;">
                                               <?php } ?>
                                                 </br></br></br></br></br></br></br></br></br></br>
                                                 <input type="file" id="inputpic" name="file" class="browse-button" style="width: 280px;position: relative; overflow: hidden; cursor: default;" ></input>
                                                 </div>
                                             <?php } ?>


                                                 <!-- <div class="ajax-file-upload-statusbar" style="width:100%;">
                                                   <div id="ajax-file-upload-radiobutton1" class="ajax-file-upload-radiobutton" style="display: none;">
                                                     <input type="radio" name="frontOrBack1" value="front" checked=""> Page 1 &nbsp;
                                                     <input type="radio" name="frontOrBack1" value="back"> Page 2</div>
                                                     <img class="ajax-file-upload-preview" style="width: 100%; height: auto; display: none;">
                                                     <div class="ajax-file-upload-filename">1) RWACImage-1.pdf</div><div class="ajax-file-upload-progress" style="display: inline-block;">
                                                     <div class="ajax-file-upload-bar ajax-file-upload-1438968315092" style="width: 100%; text-align: center;">100%</div></div>
                                                     <div class="ajax-file-upload-red ajax-file-upload-abort ajax-file-upload-1438968315092" style="display: none;">Abort</div>
                                                     <div class="ajax-file-upload-red ajax-file-upload-cancel ajax-file-upload-1438968315092" style="display: none;">Cancel</div>
                                                     <div class="ajax-file-upload-green" style="display: none;">Done</div><div class="ajax-file-upload-green" style="display: none;">Download</div>
                                                     <div class="ajax-file-upload-red" style="display: inline-block;">Delete</div></div>
                                                                             <div class="" id="advice-required-entry-chooseFile" style="display: none;">You need to choose at least one file</div>
                                                     <form method="POST" action="https://eprint4me.ca/uploader/view/ajaxUpload/" enctype="multipart/form-data" style="margin: 0px; padding: 0px;">
                                                      <input type="file" id="ajax-upload-id-1438952882103" name="fileToUploadbyAjax[]" accept="*" multiple="" style="position: absolute; cursor: pointer; opacity: 0;">
                                                     </form>
                                                </div> -->

                                          </div>
                                          <!-- <div id="fileuploader" style="display: none;">
                                            Choose file
                                          </div>
                                          <div class="" id="advice-required-entry-chooseFile" style="display: none;">
                                            You need to choose at least one file
                                          </div> -->
                                        </div>
                                      </li>
                                    </ul>
                                  <!-- <ul style="list-style: none;">
                                    <li style="margin-left: 15px; margin-top: -25px;">
                                      <div id="eventmessage" style="display: inline-block;">
                                      </div>
                                    </li>
                                  </ul> -->
                              </div>


                              <!-- <div class="file-upload-buttons">
                                <ul style="list-style: none;">
                                  <li style="padding: 10px;">
                                    <div class="add-to-cart">
                                      <input type="hidden" name="item_id" value="">

                                      <input type="hidden" name="uploadedfiles" id="uploadedfiles" value="">
                                      <input type="hidden" name="product_category_id" id="product_category_id" value="291">
                                      <input type="hidden" name="uploadedfiles_additional" id="uploadedfiles_additional" value="">
                                      <button class="form-back-button" type="button" id="goBackButton" name="btnGoBack" onclick="window.location.href='https://eprint4me.ca/menu/marketing-products/postcards.html'">
                                        <span>
                                          <span>
                                            <p>
                                            <img src="https://res.cloudinary.com/dtutqsucw/image/upload/v1438960670/back-button-icn.png"/ class="animated rotateIn">
                                              Go back
                                            </p>
                                          </span>
                                        </span>
                                      </button>
                                      &nbsp;&nbsp;
                                      <button class="form-upload-button" type="submit" id="uploadFileButton" name="btnSubmit">
                                        <span>
                                          <span id="uploadButtonName" >
                                            <p>
                                              Upload
                                               <img src="https://res.cloudinary.com/dtutqsucw/image/upload/v1438960670/file-upload.png"/ class="animated slideInUp">
                                            </p>
                                          </span>
                                        </span>
                                      </button>
                                    </div>
                                  </li>
                                </ul>
                              </div> -->
                          </div>
                        </div>
                        <div class="col-lg-12">

                          <div class="form-group">
                            <label class="control-label col-sm-2" for="nama">Nama Lengkap:</label>
                            <input type="hidden" name="foto" value="<?= $pengguna['foto'] ?>">
                            <input type="hidden" name="id_pengguna" value="<?= $pengguna['id_pengguna'] ?>">
                            <div class="col-sm-10">
                              <input type="text" class="form-control" name="nama" id="nama" value="<?= $pengguna['nama'] ?>" placeholder="Masukkan Nama">
                              <?= form_error('nama','<small class="text-danger pl-3">','</small>'); ?>
                            </div>
                          </div>
                          <div class="form-group">
                            <label class="control-label col-sm-2" for="email">Email:</label>
                            <div class="col-sm-10">
                              <input type="email" class="form-control" name="email" readonly value="<?= $pengguna['email'] ?>" id="email" placeholder="Masukkan Email">
                              <?= form_error('email','<small class="text-danger pl-3">','</small>'); ?>
                            </div>
                          </div>
                          <div class="form-group">
                              <label class="control-label col-sm-2" for="tgl_lahir">Tanggal Lahir:</label>
                              <div class="col-sm-10">
                                <input type="date" name="tgl_lahir" value="<?= $pengguna['tgl_lahir'] ?>" class="form-control" id="date">
                                <?= form_error('tgl_lahir','<small class="text-danger pl-3">','</small>'); ?>
                              </div>
                            </div>
                            <div class="form-group">
                              <label class="control-label col-sm-2" for="alamat">Alamat:</label>
                              <div class="col-sm-10">
                                  <textarea class="form-control" name="alamat" rows="5" id="alamat"><?= $pengguna['alamat'] ?></textarea>
                              </div>
                            </div>
                            <div class="row">
                              <div class="col-lg-6 col-md-6">
                                <div class="form-group">
                                    <label class="control-label col-sm-4" for="tb">Tinggi Badan:</label>
                                    <div class="col-sm-2">
                                        <input type="text" name="t_badan" value="<?= $pengguna['t_badan'] ?>" class="form-control" id="tb"> *cm
                                          <?= form_error('t_badan','<small class="text-danger pl-3">','</small>'); ?>
                                    </div>
                                  </div>
                              </div>

                              <div class="col-lg-6 col-md-6">
                                <div class="form-group">
                                    <label class="control-label col-sm-4" for="bb">Berat Badan:</label>
                                    <div class="col-sm-2">
                                        <input type="text" name="b_badan" value="<?= $pengguna['b_badan'] ?>" class="form-control" id="bb"> *kg
                                          <?= form_error('b_badan','<small class="text-danger pl-3">','</small>'); ?>
                                    </div>
                                  </div>
                              </div>
                            </div>


                            <div class="form-group">
                              <label class="control-label col-sm-2" for="notelp">No. Telepon:</label>
                              <div class="col-sm-10">
                                <input type="text" name="no_telp" value="<?= $pengguna['no_telp'] ?>" class="form-control" id="notelp" placeholder="Masukkan No. Telp">
                                  <?= form_error('no_telp','<small class="text-danger pl-3">','</small>'); ?>
                              </div>
                            </div>
                            <div class="form-group">
                              <label class="control-label col-sm-2" for="prestasi">Prestasi:</label>
                              <div class="col-sm-10">
                                <textarea name="prestasi" name="prestasi" class="form-control" id="prestasi" placeholder="Prestasi 1"><?= $pengguna['prestasi'] ?></textarea>
                              </div>
                            </div>


                            <button type="submit" class="btn btn-success btn-block">Simpan</button>
                            <br><br>
                        </form>
                        </div>
                      </div>

                    </div>

<script type="text/javascript">
$('#inputpic').on('change',function(e){
  console.log(e.target.files[0]);
  var reader = new FileReader();
  reader.readAsDataURL(e.target.files[0]);
  reader.onload = function(event) {
    $('#imgbox').attr('style','background-image: url("'+event.target.result+'");background-size:cover')
    $('#imgplaceholder').html('<br/><br/><br/><br/><br/><br/><br/><br/>')
  }
})
</script>
