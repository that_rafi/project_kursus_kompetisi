<div class="container" style="margin-top:50px">
    <div class="row align-items-center">
        <div class="col-lg-12">
            <h2>Buat Tempatmu!</h2>
            <?= $this->session->flashdata('message'); ?>
            <?= $this->session->flashdata('error'); ?>
            <div id="msgtxt"></div>
            <h3>Informasi Tempat</h3>
            <hr>
            <form class="form-horizontal" action="<?= base_url() ?>tempat/manage/tpt<?= $tempat['id_tempat'] ?>" method="post" enctype="multipart/form-data">
              <div class="form-group row">
                <input type="hidden" class="form-control" name="id_tempat" id="inputid" value="<?= $tempat['id_tempat'] ?>">
                <label for="inputName" class="col-sm-2 col-form-label">Nama tempat</label>
                <div class="col-sm-10">
                  <input type="text" class="form-control" name="nama_tempat" id="inputName" value="<?= $tempat['nama_tempat']; ?>" placeholder="Nama tempat">
                  <?= form_error('nama_tempat','<small class="text-danger pl-3">','</small>'); ?>
                </div>
              </div>
              <div class="form-group row">
                <input type="hidden" class="form-control" name="createdBy" id="inputcreatedby" value="<?= $tempat['createdBy'] ?>">
              </div>
              <div class="form-group row">
                <input type="hidden" class="form-control" name="id_cabang" id="inputidcabang" value="<?= $tempat['id_cabang'] ?>">
                <label for="inputCabang" class="col-sm-2 col-form-label">Cabang Olahraga</label>
                <div class="col-sm-10">
                  <select name="id_cabang" id="inputCB" class="form-control">
                      <?php foreach ($cabang as $c):
                          if($c['id_cabang'] == $tempat['id_cabang'] ){ ?>
                            <option value="<?= $c['id_cabang'] ?>" selected><?= $c['nama_cabang'] ?></option>
                          <?php }else{ ?>
                            <option value="<?= $c['id_cabang'] ?>" ><?= $c['nama_cabang'] ?></option>
                      <?php }endforeach; ?>
                  </select>
                  <?= form_error('id_cabang','<small class="text-danger pl-3">','</small>'); ?>
                </div>
              </div>
              <div class="form-group row">
                <label for="inputLokasi" class="col-sm-2 col-form-label">Lokasi</label>
                <div class="col-sm-10">
                  <textarea class="form-control" name="lokasi" id="inputLokasi" placeholder="Link Gmaps.."><?= $tempat['lokasi']; ?></textarea>
                  <?= form_error('lokasi','<small class="text-danger pl-3">','</small>'); ?>
                </div>
              </div>
              <div class="form-group row">
                <label for="inputDeskripsi" class="col-sm-2 col-form-label">Deskripsi</label>
                <div class="col-sm-10">
                  <textarea class="form-control" name="deskripsi" id="inputDeskripsi" placeholder="Deskripsi.."><?= $tempat['deskripsi']; ?></textarea>
                  <?= form_error('deskripsi','<small class="text-danger pl-3">','</small>'); ?>
                </div>
              </div>
              <div class="form-group row">
                <label for="inputBiaya" class="col-sm-2 col-form-label">Biaya Sewa</label>
                <div class="col-sm-10">
                  <input type="text" class="form-control" name="biaya_sewa" value="<?= $tempat['biaya_sewa']; ?>" id="inputBiaya" placeholder="Rp.XXXXX / bulan" >
                  <?= form_error('biaya_sewa','<small class="text-danger pl-3">','</small>'); ?>
                </div>
              </div>
              <div class="form-group row">
                <label for="inputJam" class="col-sm-2 col-form-label">Jam Oprasional</label>
                <div class="col-sm-10">
                  <textarea class="form-control" name="jam" id="inputjam" placeholder="Jam.."><?= $tempat['jam']; ?></textarea>
                  <?= form_error('jam','<small class="text-danger pl-3">','</small>'); ?>
                </div>
              </div>
              <div class="form-group row">
                <label for="inputkontak" class="col-sm-2 col-form-label">Kontak</label>
                <div class="col-sm-10">
                  <input type="text" class="form-control" name="kontak" value="<?= $tempat['kontak']; ?>" id="inputkontak" placeholder="Kontak" >
                  <?= form_error('kontak','<small class="text-danger pl-3">','</small>'); ?>
                </div>
              </div>
              <div class="form-group row">
                <input type="hidden" name="status" value="<?= $tempat['status'] ?>"/>
              </div>
              <button type="submit" id="btnsave" class="btn btn-success btn-block">Simpan</button>
        </div>
      </form>
</div>
</div>
</div>
<div class="container">
    <div class="row align-items-center">
        <div class="col-lg-12">
            <h3>Tambah Gallery</h3>
            <hr>
        </div>
    </div>
</div>
<style>
  .dropzone{
    width: 300px;
    height: 300px;
    padding: 10px;
    display: flex;
    justify-content: center;
    align-items: center;
    border-radius: 10px;
    border: 2px dashed rgb(204, 204, 204);
    color: rgb(204, 204, 204);
    text-align: center;
    -webkit-box-align: center;
    -webkit-box-pack: center;
    z-index: 100;
    cursor: default;
  }
  .dropzone.dragover{
    border-color: #000;
    color: #000;
  }
  .dropzone_thumb{
    width: 100%;
    height: 100%;
    border-radius: 10px;
    background-image: url('<?= base_url() ?>assets/upload/default/img_tumbnail.png');
    background-size: contain;
    background-repeat: no-repeat;
    position: relative;
    overflow: hidden;
  }
  .dropzone_thumb::after{
    content: attr(data-label);
    position: absolute;
    bottom: 0;
    left: 0;
    width: 100%;
    padding: 5px 0;
    color: #ffffff;
    background-color: grey;
    font-size: 12px;
    text-align: center;
  }
</style>
<div class="col-12">
          <div class="container">
                <div class="form-group row">
                  <div class="col-lg-6" style="justify-content:center;align-items:center;display:flex">
                    <div class="dropzone" >
                    <label for="browse" class="dropzone_msg">Drop files here</label>
                    <input type="file" id="browse" name="browse" style="display:none;" class="dropzone_input">
                    <!-- <div class="dropzone_thumb" data-label="mytext.txt"></div> -->
                    </div>
                  </div>
                  <div class="col-lg-6">
                    <div class="col-lg-12">
                    <textarea class="form-control" id="inputKeterangan" placeholder="Keterangan"></textarea>
                    </div>
                    <div class="col-lg-12">
                      <button id="addinforow" style="margin-top:20px" class="btn btn-success"><i class="fa fa-plus"></i></a>
                    </div>
                  </div>
                </div>
                  </form>
                <div class="form-group row" id="info1">
                  <div class="card-body table-responsive p-0">
                    <table class="table table-hover text-nowrap">
                      <thead>
                        <tr>
                          <th>Foto</th>
                          <th>Keterangan</th>
                          <th>Action</th>
                        </tr>
                      </thead>
                      <tbody id="listinfo">
                        <?php $count=0; foreach($resource as $res) : ?>
                          <tr id="listcount<?= $count ?>">
                            <td><img src='<?= base_url() ?>assets/upload/resource_tempat/<?= $res['filename'] ?>' width="30px" height="30px"></img></td>
                            <td><?= $res['keterangan'] ?></td>
                            <td><button id="minusinforow<?= $res['id_resource'] ?>" onclick="event.preventDefault(); minusinforow(<?= $res['id_resource'] ?>,<?= $count ?>);" class="btn btn-warning"><i class="fa fa-minus"></i></a></td></td>
                          </tr>
                        <?php $count++; endforeach; ?>
                      </tbody>
                    </table>
                  </div>
                </div>


            <!-- /.tab-pane -->
          </div>
</div>

<script type="text/javascript">
var base = <?php echo json_encode(base_url()); ?>;
const max = <?php echo json_encode($max_res_tempat); ?>;
var formData = new FormData();
var tempfiles = "";

  $('#inputpic').on('change',function(e){
    console.log(e.target.files[0]);
    var reader = new FileReader();
    logoData = e.target.files[0];
    reader.readAsDataURL(e.target.files[0]);
    reader.onload = function(event) {
      $('#imgbox').attr('style','background-image: url("'+event.target.result+'");background-size:cover')
      $('#imgplaceholder').html('<br/><br/><br/><br/><br/><br/><br/><br/>')
    }
  })

  $('#addinforow').on('click',function(e){
    e.preventDefault();
    var $div = $('tr[id^="listcount"]:last');
    if($div.length > 0 ){
        var num = parseInt( $div.prop("id").match(/\d+/g), 10 ) +1;
    }else{
        var num = 0;
    }

    var url = $('.dropzone_thumb').data('imgurl')
    var imgname = $('.dropzone_thumb').data('label')
    var keterangan = $('#inputKeterangan').val()
    if (num < max ){
      if( keterangan!= "" && url !=null){
        // data for form
        upload(tempfiles,keterangan,function(res){
           if(res.result.upload){
             var row = '  <tr id="listcount'+num+'">'+
                       '<td><img src='+url+' width="30px" height="30px"></img></td>'+
                       '<td>'+keterangan+'</td>'+
                       '<td><button id="minusinforow'+res.result.newid+'" onclick="event.preventDefault(); minusinforow('+res.result.newid+','+num+');" class="btn btn-warning"><i class="fa fa-minus"></i></a></td></tr>'
             $('#listinfo').append(row)
           }
        })

      }else{
        alert('Tolong masukan informasi yang lengkap')
      }
    }else{
      alert('anda sudah mencapai maksimal!')
    }

  })

  function minusinforow(id,index){
    $.ajax({
      url : '<?= base_url(); ?>tempat/manage/tpt<?= $tempat['id_tempat'] ?>/deleteRes',
      type : 'POST',
      dataType : 'json',
      data : 'id_resource='+id,
      success : function(res){
        console.log(res);
        if(res.result){
          $('#listcount'+index).remove()
        }
      }
    })
  }

  function upload(file,keterangan,calback){
    formData.append('file',file)
    formData.append('keterangan',keterangan)
    formData.append('id_tempat',<?= $tempat['id_tempat'] ?>)
    $.ajax({
      url : '<?= base_url(); ?>tempat/manage/tpt<?= $tempat['id_tempat'] ?>/uploadRes',
      type : 'POST',
      dataType : 'json',
      processData: false,
      contentType: false,
      data : formData,
      success : function(res){
        calback(res)
      }
    })
  }

  function setImg(dropzone,files){
    if(files.type.startsWith("image/")){
    var thumbnail = $('.dropzone_thumb').data('label')
    dropzone.attr('class','dropzone')
    if(!thumbnail){
      $('.dropzone_msg').hide()
      var thumb = $('<div/>').addClass('dropzone_thumb');
      dropzone.append(thumb)
    }
    // data for form
    tempfiles = files
    $('.dropzone_thumb').attr('data-label',files.name) // set img name
      var reader = new FileReader();
      reader.readAsDataURL(files);
      reader.onload = function(event) {
            $('.dropzone_thumb').attr('style','background-image: url("'+event.target.result+'")')
            $('.dropzone_thumb').data('imgurl',event.target.result) // hold img url
      };
    }else{
      alert('Please input image only!')
    }
  }

  jQuery(function($){
    $('.dropzone').on('click', function(){
        $('.dropzone_input')[0].click();
    });
  });

    $('.dropzone_input').on('change',function(e){
      $('.dropzone').attr('class','dropzone')
      setImg($('.dropzone'),e.target.files[0])
    })

    $('.dropzone').on('drop',function(e){
      e.preventDefault();
      $(this).attr('class','dropzone')
      setImg($(this),e.originalEvent.dataTransfer.files[0])
    })
    $('.dropzone').on('dragover',function(){
      $(this).attr('class','dropzone dragover')
      return false
    })
    $('.dropzone').on('dragleave',function(){
      $(this).attr('class','dropzone')
      return false
    })

    function alertCall($type,$msg,$target){
      if($type == 'alert'){
        alt= '<div class="alert alert-danger alert-dismissible">'+
                      '<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>'+
                      '<h5><i class="icon fas fa-ban"></i> Alert!</h5>'+$msg+
                    '</div>'
      }else if($type == 'success'){
        alt= '<div class="alert alert-success alert-dismissible">'+
                      '<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>'+
                      '<h5><i class="icon fas fa-check"></i> Success!</h5>'+$msg+
                    '</div>'
      }
      $($target).html(alt)
    }
</script>
